import sys

import numpy as np
from scipy import linalg
import scipy.sparse as sparse
import scipy.sparse.linalg as sparse_linalg

import _utility


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def DA_Stoch_EnKF_Cycle(Model_Object           ,      \
                        Xf                     ,      \
                        Y                      ,      \
                        Nens                   ,      \
                        update_B=True          ,      \
                        update_B_fac = 0.5     ,      \
                        inflation_factor=1.09  ,      \
                        system_solver='splu'          \
                        ):
        """
        Ensemble Kalman filter (Stochastic version) cycle. Observations are perturbed using Gaussian noise with
        covariance matrix = R.

        :param Model_Object: ...
        :type Xf: 1D numpy array.

        :return: numpy array of resampled values.
        """
        EnKF_Statistics = {'effective_sample_size':[]}

        state_size = Model_Object._state_size
        nobsVars = Y.size
        nens = Nens

        if np.isnan(Xf).any():
            raise ValueError("Passed Forecast state cannot contain NaNs!!!")
        if np.isnan(Y).any():
            raise ValueError("Passed observation vector cannot contain NaNs!!!")
        if not(system_solver.lower() in ['lu' , 'splu'] ):
            raise ValueError("The desired solution strategy of the linear system is not yet implemented!!!")

        if update_B:
            #----------- Use flow-dependent B up update the modeled B ----------
            # construct a flow-dependent background error covariance matrix. REQUIRES a new flag. This is very costly..
            infl_fac = inflation_factor # inflation coefficient.
            X_mean = np.mean(Xf,1)
            prturb_X = np.zeros((state_size,nens))
            for iens in range(nens):
                #
                print '\rFlow-dependent B0. ens:%d           \r'%(iens+1) ,
                sys.stdout.flush()
                #print'\r                                                       \r',
                print('\r'+'\r'.rjust(50)),
                #
                prturb_X[:,iens] = ( infl_fac * ( Xf[:,iens] - X_mean ) /np.sqrt(nens-1) )
            Pb = np.dot(prturb_X , prturb_X.T )

            #
            # linear weight with modeled B
            # Pb should NOT be constructed in full in practical applications
            fac = update_B_fac # empirical balance factor for the flow-dependent background error covariance matrix
            Pb = (Pb * fac) + ((1-fac) * Model_Object._B0)
            #-------------------------------------------------------------------
        else:
            # use the modeled background error covariance matrix
            Pb = Model_Object._B0
            #

        #print 'Pb before decorr', Pb
        # localize the background error covariance matrix if required...
        # the result is either ndarray or csr_matrix (the latter is if sparse package is to be used).
        if Model_Object._decorrelate:
            #
            print '\rDecorrelating Pb                     ',
            sys.stdout.flush()
            #print'\r                                                       \r',
            print('\r'+'\r'.rjust(50)),
            #
            Pb = Model_Object.decorr_dot_matrix(Pb)
            #print 'Pb after decorr', Pb

        #
        print '\rSolving the linear system                ',
        sys.stdout.flush()
        #print'\r                                                       \r',
        print('\r'+'\r'.rjust(50)),
        #

        # initialize the analysis ensemble matrix
        Xa = np.zeros((state_size,nens))



        if Model_Object._Use_Sparse:
            Pb_HT   = ((Model_Object._H).dot(Pb.T)).T
            H_Pb_HT = Model_Object._H.dot(Pb_HT) # result is an np.array

            # sparse LU factorization
            A_mat = H_Pb_HT + Model_Object._R
            #print repr(A_mat)
            #print 'sections of A_mat', A_mat[[0,0,0,1,1,1,2,10,20],[10,11,20,21,31,41,22,12,2]]
            #print A_mat.shape
            if system_solver.lower()=='splu':
                #sparse LU solver
                invB_OBJ = sparse_linalg.splu( A_mat.tocsc() )
            elif system_solver.lower()=='lu':
                #dense LU solver
                sp_lu,sp_piv = linalg.lu_factor( A_mat.toarray() )
            else:
                raise ValueError("Solver is not supported yet! ")

        else:
            Pb_HT   = np.dot(Pb , Model_Object._H.T)
            H_Pb_HT = np.dot(Model_Object._H , Pb_HT)
            A_mat = H_Pb_HT + Model_Object._R
            #print 'sections of A_mat', A_mat[[0,0,0,1,1,1,2,10,20],[10,11,20,21,31,41,22,12,2]]
            lu,piv = linalg.lu_factor( A_mat )


        #
        # Assimilating each ensemble member:
        #
        for iens in range(nens):

            print '\rAssimilating ensemble number: %d       '%(iens+1) ,
            sys.stdout.flush()
            #print'\r                                                       \r',
            print('\r'+'\r'.rjust(50)),
            #
            # inverse... (delayed op)
            xf = Xf[:,iens]
            #print 'xf= ', xf[0:10],'>+<',xf[-10:-1]

            # perturb observation vector
            randnVec = _utility.randn_Vec(nobsVars)
            if Model_Object._Use_Sparse:
                prtrb_Y = Y + (Model_Object._sqrtR).dot(randnVec)
            else:
                prtrb_Y = Y + ( np.dot(Model_Object._sqrtR, randnVec ) )
            D_HXa = prtrb_Y - Model_Object.obs_oper_prod_Vec(xf)
            #

            #
            # solve the linear system and get the ensemble update:
            if Model_Object._Use_Sparse:
                if system_solver.lower()=='splu':
                    #sparse LU solver
                    sol_Vec = invB_OBJ.solve(D_HXa)
                elif system_solver.lower()=='lu':
                    #dense LU solver
                    sol_Vec = linalg.lu_solve((sp_lu,sp_piv) , D_HXa)
                else:
                    raise ValueError("Solver is not supported yet! ")
                #print 'sol_Vec ', sol_Vec[0:10],'>+<',sol_Vec[-10:-1]
                ens_update = Pb_HT.dot(sol_Vec)
            else:
                sol_Vec = linalg.lu_solve((lu,piv) , D_HXa)
                #print 'sol_Vec ', sol_Vec[0:10],'>+<',sol_Vec[-10:-1]
                ens_update = np.dot(Pb_HT , sol_Vec)

            # generate the analysis ensemble
            #print 'ens_update ', ens_update[0:10],'>+<',ens_update[-10:-1]
            Xa[:,iens] = xf + ens_update



        #print Xa
        return Xa , EnKF_Statistics

#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~





#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def DA_Sqrt_EnKF_Cycle(Model_Object, Xf , Y , Nens , update_B=True):
        """
        Square-Root Ensemble Kalman filter (Deterministic version ) cycle.
        TODO: update the filter as did in Stoch_EnKF and revalidate...
        """
        sqrtEnKF_Statistics = {'effective_sample_size':[]}

        nvars = Model_Object._state_size
        #nobsVars = Model_Object._no_of_observed_vars
        nens = Nens

        Xa  = np.zeros((nvars,nens))

        H = Model_Object._H
        R = Model_Object._R

        # inflation
        alpha = 1.05

        Xmean = np.mean(Xf,1)
        Aprime = np.zeros((nvars,nens))
        for iens in range(nens):
            print '\rAprime ensemble number: %d               '%(iens+1) ,
            sys.stdout.flush()
            #print'\r                                                       \r',
            print('\r'+'\r'.rjust(50)),
            Aprime[:,iens] = alpha * (Xf[:,iens]-Xmean)

        S = np.dot(H,Aprime)
        C = np.dot(S,S.T) + (nens-1)*R
        C_eig , Z = linalg.eig(C)

        inv_Ceig = np.diag(1/C_eig)
        y1 = np.dot(Z.T,(Y - np.dot(H,Xmean) ))
        y2 = np.dot(inv_Ceig,y1)
        y3 = np.dot(Z , y2)
        y4 = np.dot(S.T , y3)

        # update ensemble mean
        Xa_mean = Xmean + np.dot(Aprime,y4)

        # Evaluate matrix X2 and it SVD
        inv_sqrt_Ceig = np.diag(np.sqrt(np.diag(inv_Ceig)))
        X2 = np.dot( np.dot(inv_sqrt_Ceig , Z.T) , S)


        Ul,Sig2,Vh = linalg.svd(X2)
        Vr = Vh.T

        # the square root matrix update
        Sig2_full = np.zeros((Ul.shape[0] , Vr.shape[1]))
        for i in range(Sig2.size):
            Sig2_full[i,i] = Sig2[i]


        temp_mat = np.dot(Sig2_full.T,Sig2_full)
        msize= temp_mat.shape[0]

        #Sqrtmat = linalg.sqrtm(np.eye(msize)-temp_mat)
        Sqrtmat = linalg.cholesky(np.eye(msize)-temp_mat , lower=True)

        #update ensemble perturbations
        Aa_prime = np.dot(np.dot(np.dot(Aprime,Vr),Sqrtmat),Vr.T)

        for iens in range(nens):
            print '\rAnalysis ensemble number: %d             '%(iens+1) ,
            sys.stdout.flush()
            #print'\r                                                       \r',
            print('\r'+'\r'.rjust(50)),
            Xa[:,iens] = Aa_prime[:,iens] + Xa_mean


        return Xa , sqrtEnKF_Statistics
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~





##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#def StratResample(W):
#        """
#        Function implementing stratified resampling. Reference: Kitagawa, G., 1996.
#
#        .. todo:: Test this! Add sorting to make it more efficient.
#
#        :param W: positive array of probabilities.
#        :type W: 1D numpy array.
#
#        :return: numpy array of resampled values.
#        """
#        W = np.asarray(W)
#
#        m=len(W)
#        W_sum=W.sum()
#        W=(W/W_sum)*m
#
#        r=np.random.uniform(size=(m,1))
#        k=0
#        c=0
#        idx_out=np.zeros(len(W) , dtype=np.int)
#        for iprt in range(m):
#            c=c+W[iprt]
#            if (c>=1):
#                a=int(np.floor(c))
#                c=c-a
#                for kk in range(k,k+a):
#                    idx_out[kk]=iprt
#                k=k+a
#            if (k<m and c>=r[k]):
#                c=c-1
#                k=k+1
#                idx_out[k-1]=iprt
#        return idx_out


def PF_Resample(Weights , nens=None , Strategy='stratified' ):
    """
    Resampling step for the particle filter.
    Inputs:
    Weights:  np.array of (particles') weights (normalized or not)
    Nens:     number of samples to generate indexes for
    Strategy: Resampling strategy;
    Implemented schemes:
    1- Systematic Resampling,
    2- Stratified Resampling,
    3- ...
    Outputs:
    Indexes:  np.array of size Nens containing integer indexes generated based on the given weights.
    """

    Resampling_strategies = ['systematic','stratified']
    if not(Strategy.lower() in Resampling_strategies):
        raise ValueError("Strategies implemented so far are: "+repr(Resampling_strategies))
    else:
        Weights = np.squeeze(np.asarray(Weights))
        weightVec_len = Weights.size
        weightVec_sum = Weights.sum()
        #
        if weightVec_sum==0:
            raise ValueError("Weights sum to zeros!!")
        #
        if nens is None:
            nens = weightVec_len

        if weightVec_sum!=1:
            # Normalize weights if necessary
            Weights = Weights/weightVec_sum


    if Strategy.lower() == 'systematic':
        #
        Weights_CumSum = Weights.cumsum()
        Indexes = np.zeros(nens)

        T = _utility.linspace(0,1-1/nens,nens,endpoint=True)+((np.random.rand(1)[0])/nens)
        i = 0
        j = 0
        while (i<nens and j<weightVec_len):
            while Weights_CumSum[j]<T[i]:
                j+=1
            Indexes[i] = j
            i+=1


    elif Strategy.lower() == 'stratified':
        #
        Weights_CumSum = Weights.cumsum()
        Indexes = np.zeros(nens,dtype=np.int)

        T = np.linspace(0,1-1/nens,nens,endpoint=True)+(np.random.rand(nens)/nens)
        i = 0
        j = 0
        while (i<nens ):
            while Weights_CumSum[j]<T[i] and j<weightVec_len-1:
                #print '>>>>>>>',i , '    ', j
                #print 'T[',j,']=',Weights_CumSum[j]
                #print 'Weights_CumSum[',j,']=',Weights_CumSum[j]
                j+=1

            #print 'Indexes[',i,']=' ,Indexes[i]
            Indexes[i] = j
            i+=1

    else:
        raise NotImplementedError


    #print 'Indexes :',Indexes
    return Indexes



def likelihood( Model_Object , y , y_obs ):
    """
    Simple Gaussian likelihood function
    .. todo:: make it efficient.
    :param y: particle observations.
    :type y: 1D numpy array.
    :param y_obs: real observations.
    :type y_obs: 1D numpy array.
    :param invR: inverse of the observation error covariance matrix.
    :type invR: 2D numpy array.

    :return: likelihood value.
    """
    # TODO : polish this code...
    # the input y should be a matrix containing all states we need to evaluate likelihood for.
    # numpy has a precision problem with np.exp because of underflow issues. One thing we can do is to use sympy with unlimited precision
    # but this will bring unnecessary complication. One thing we can do is to scale all log-likelihoods by the maximum magnitude then normalize
    # the resulting likelihoods. This I will do here. The result will be weighted likelihoods.
    # if the input is a single vector the result will be unnormalized likelihood which might be rounded to zero if the misfit is large!
    # the scaling factor (exponent with maximum magnitude will be returned as well. In case of single observation, this will the simply the associated exponente (magnitude)
    #
    # R determinant if not evaluated.
    #try:
    #    detR = Model_Object._detR
    #except :
    #    if Model_Object._Use_Sparse:
    #        detR = np.det( Model_Object._detR )
    #    else:
    #        detR =  np.det( Model_Object._detR.toarray() )
    #norm_part = 1./((2*np.pi)**(y_obs.size*0.5)*sqrt(detR))


    if y.size==y_obs.size: # same number of elements!
        norm_part = 1.#1/((2*np.pi)**(y_obs.size*0.5)*sqrt(detR))
        mfit = y_obs - y
        #print 'mfit',mfit
        #bleah - this needs to be fixed too
        #expon=np.exp( -0.5*( (mfit.T).dot(invR).dot(mfit)) )
        scld_mfit = Model_Object.observation_Error_Cov_Inv_prod_Y(mfit)
        #print 'scld_mfit',scld_mfit
        #print 'np.dot(mfit,scld_mfit)',np.dot(mfit,scld_mfit)
        expon = np.exp( -0.5*( np.dot(mfit,scld_mfit) ) )
        #print 'expon',expon
        nrmlized_Likelihood = norm_part*expon
        #print 'nrmlized_Likelihood',nrmlized_Likelihood



    else:
        try:
            # each column in y should be an observation
            norm_part = 1.
            num_features , num_obs = y.shape
            if num_features!=y_obs.size:
                raise ValueError("number of rows in y must be of the same size as y_obs")

            neglog_likelihood = np.zeros(num_obs)
            likelihood = np.zeros(num_obs)

            for obs_ind in xrange(num_obs):
                mfit = y_obs - y[:,obs_ind]
                scld_mfit = Model_Object.observation_Error_Cov_Inv_prod_Y(mfit)
                neglog_likelihood[obs_ind] = 0.5*( np.dot(mfit,scld_mfit) )

            min_neglog_likelihood = np.min(np.abs(neglog_likelihood) )# there should not be negative originally...
            #for obs_ind in xrange(num_obs):
            #    #likelihood[obs_ind] = sympy.exp( -(neglog_likelihood[obs_ind]-min_neglog_likelihood)  ).evalf()
            #    likelihood[obs_ind] = np.exp( -(neglog_likelihood[obs_ind]-min_neglog_likelihood)  )
            likelihood = np.exp( -(neglog_likelihood-min_neglog_likelihood)  ) # the most favorable particle will recieve a likelihood set to one.
            #print likelihood

            nrmlized_Likelihood = norm_part*likelihood

        except:
            raise ValueError("y must be an array with all observations aligned as columns")

    return nrmlized_Likelihood




def importance_distribution( Model_Object , xk, xk1, y_obs ):
    """
    Simple Gaussian likelihood function
        .. todo:: this is not implemented!

    :param Model_Object: the model object.
    :type Model_Object: class.
    :param xk: forecast at time k.
    :type xk: 1D numpy array.
    :param xk1: analysis at time k-1.
    :type xk1: 1D numpy array.
    :param y_obs: real observations.
    :type y_obs: 1D numpy array.
    :return: the importance distribution value at sample points.
    """

    return 1



def DA_BootstrapPF_Cycle(da_self, Model_Object , Xf , Y  , Nens , resampling_strategy='stratified'):
        """
        Bootstrap particle filter [Gordon 1994, I think]
        .. todo:: This, and the similar ones, should be moved to a utility module... EC: I would put them in the DAFilter class, but not right now. Once we move we need to replace da_self with self.
        """
        _state_size = Model_Object._state_size
        _particle_no = Nens
        #_observation_size = Model_Object._no_of_observed_vars

        PF_Statistics = {'effective_sample_size':[]}
        weights_statistic = np.zeros(_particle_no)

        # loop over all particles, calculate the posterior
        #H = Model_Object._H

        Lik = np.zeros(_particle_no)
        #
        theo_Obs = np.zeros((Y.size, Xf.shape[1]))
        for iprt in range(_particle_no):
            xf = Xf[:,iprt]
            #particle observations ; real observations ; #inverse of R - this needs to be replaced
            theo_Obs[:,iprt]  = Model_Object.obs_oper_prod_Vec(xf)
            #Lik[iprt] = likelihood( Model_Object , theo_Obs, Y )
        Lik = likelihood( Model_Object , theo_Obs, Y )
        #print Lik

        #if Scl_fac!=0:
        #    Lik=Scld_Lik # will be polished after deciding what strategy should be followd in general

        Lsum   = Lik.sum()
        if Lsum==0: # This should not happen as long as all particles are passed to the likelihood function once!
            print Lik
            print 'All particles received negligible weights rounded down to zero!. Filter diverged; Ensemble is not updated!'
            Xa = Xf.copy()
            return Xa
        else:
            w_norm = Lik/Lsum
        #print Lsum

        # Monitors the effective particle number
        weights_statistic[0:_particle_no]=w_norm
        ess=1./np.sum(weights_statistic**2)
        da_self._DAMonitor['ess'].append(ess)

        # one monitor will be chosen...
        PF_Statistics = ['effective_sample_size'].append(ess)


        #prt_idx = StratResample(w_norm)
        prt_idx = PF_Resample(Weights=w_norm , nens=_particle_no , Strategy=resampling_strategy )


        #Analysis ensemble by weighted resampling
        Xa      = np.zeros((_state_size,_particle_no))
        for iprt in xrange(_particle_no):
            res_ind = prt_idx[iprt]
            #print res_ind
            #print Xf[:,res_ind]
            Xa[:,iprt] = np.squeeze(Xf[:,res_ind])

        return Xa , PF_Statistics
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def DA_PF_Cycle(da_self, Model_Object , Xf , X_k1 , Y  , Nens , resampling_strategy='stratified'):
        """
        SIR particle filter
        .. todo:: needs to be tested, currently it is orphaned; major point is to treat R adequately.[Ahmed: DONE!]
        .. todo:: This, and the similar ones, should be moved to a utility module... EC: I would put them in the DAFilter class, but not right now. Once we move we need to replace da_self with self.
        """
        _state_size = Model_Object._state_size
        _particle_no = Nens
        #_observation_size = Model_Object._no_of_observed_vars

        PF_Statistics = {'effective_sample_size':[]}

        weights_statistic = np.zeros(_particle_no)

        # loop over all particles, calculate the posterior
        #H = Model_Object._H

        Lik = np.zeros(_particle_no)
        Imp = np.zeros(_particle_no)
        W   = np.zeros(_particle_no)
        #
        for iprt in range(_particle_no):
            xf = Xf[:,iprt]
            xfk1= X_k1[:,iprt]
            #particle observations ; real observations ; #inverse of R - this needs to be replaced
            theo_Obs  = Model_Object.obs_oper_prod_Vec(xf)
            Lik[iprt] = likelihood( Model_Object , theo_Obs, Y )
            Imp[iprt] = importance_distribution( Model_Object , xf, xfk1, Y)
            W[iprt]   = Lik[iprt]/Imp[iprt]

        Psum   = W.sum()
        if Psum==0: # all particles diverge!
            print 'All particles received negligible weights rounded down to zero. Filter diverged; Ensemble is not updated!'
            Xa = Xf.copy()
            return Xa
        else:
            W_norm = W/Psum
        #print Lsum

        # Monitors the effective particle number
        weights_statistic[0:_particle_no]=W_norm
        ess=1./np.sum(weights_statistic**2)
        da_self._DAMonitor['ess'].append(ess)

        # one monitor will be chosen...
        PF_Statistics = ['effective_sample_size'].append(ess)


        prt_idx = PF_Resample(Weights=W_norm , nens=_particle_no , Strategy=resampling_strategy )

        #Analysis ensemble by weighted resampling
        Xa      = np.zeros((_state_size,_particle_no))
        for iprt in xrange(_particle_no):
            res_ind = prt_idx[iprt]
            #print res_ind
            #print Xf[:,res_ind]
            Xa[:,iprt] = np.squeeze(Xf[:,res_ind])

        return Xa , PF_Statistics
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def DA_Hamiltonian_MC_Cycle(Model_Object, Xf, Observation, HMC_Options, screen_output=False):
        """
        Hybrid/Hamiltonian Monte-Carlo sampling algorithm.
        This is a crude implementation that includes NO tuning of the main parameters {Hamiltonian step size, and number of steps}.
        These parameters can be tuned by watching acceptance rate, or :
        NUTS can be considered for automatic tuning of these parameters.
        Xf: forecast ensemble
        """
        # get the sampling strategy:
        try:
            Sampling_Strategy = HMC_Options['sampling_strategy']
        except:
            Sampling_Strategy ='vanilla_hmc'

        # Monitor sampling statistics
        # 'Func_Evals': number of potential function evaluations
        # 'Grad_Evals': number of evaluations of the gradient of the potential function
        # Probability of acceptance at each iteration. Acceptance rate can be evaluated from it...
        HMC_Statistics={'Func_Evals':0 , 'Grad_Evals':0, 'P_Acceptance':[] , 'Kernel_NegLog_Vals':[], 'Accepted_Proposals':[] }
        HMC_Options.update({'HMC_Statistics':HMC_Statistics})
        #

        HMC_Options.update({'state_size':Model_Object._state_size})
        #
        #Xf = np.asarray(Xf)
        #Y  = np.asarray(Y)
        #
        state_size = Model_Object._state_size
        update_B_factor = HMC_Options['update_B_factor']
        use_sparse = HMC_Options['use_sparse']
        infl_fac = HMC_Options['inflation_factor'] # inflation coefficient.
        system_solver = HMC_Options['linear_system_solver'].lower()
        nens = HMC_Options['ensemble_size']

        Forecast_State = np.mean(Xf,1)

        if np.isnan(Xf).any():
            raise ValueError("Passed Forecast state cannot contain NaNs!!!")
        if np.isnan(Observation).any():
            raise ValueError("Passed observation vector cannot contain NaNs!!!")


        if ( update_B_factor !=0 ):
            #----------- Use flow-dependent B up update the modeled B ----------
            # construct a flow-dependent background error covariance matrix. REQUIRES a new flag. This is very costly..
            X_mean = np.mean(Xf,1)
            prturb_X = np.zeros((state_size,nens))
            for iens in range(nens):
                #
                print '\rFlow-dependent B0. ens:%d           \r'%(iens+1) ,
                sys.stdout.flush()
                #print'\r                                                       \r',
                print('\r'+'\r'.rjust(50)),
                #
                prturb_X[:,iens] = ( infl_fac * ( Xf[:,iens] - X_mean ) /np.sqrt(nens-1) )
            Pb = np.dot(prturb_X , prturb_X.T )

            #
            # linear weight with modeled B
            # Pb should NOT be constructed in full in practical applications
            fac = update_B_factor # empirical balance factor for the flow-dependent background error covariance matrix
            Pb = (Pb * fac) + ((1-fac) * Model_Object._B0)

            #print 'Pb before decorr', Pb
            # localize the background error covariance matrix if required...
            # the result is either ndarray or csr_matrix (the latter is if sparse package is to be used).
            if Model_Object._decorrelate:
                #
                print '\rDecorrelating Pb                     ',
                sys.stdout.flush()
                #print'\r                                                       \r',
                print('\r'+'\r'.rjust(50)),
                #
                Pb = Model_Object.decorr_dot_matrix(Pb)
                #print 'Pb after decorr', Pb

            #-------------------------------------------------------------------
        else:
            # use the modeled background error covariance matrix
            Pb    = Model_Object._B0
            #


        # construct the diagonal of the Mass matrix:
        if HMC_Options['mass_matrix_strategy']=='prior_variances':
            if Model_Object._Use_Sparse:
                mass_mat_variances = HMC_Options['mass_matrix_scale_factor'] * Pb.diagonal()
            else:
                mass_mat_variances = HMC_Options['mass_matrix_scale_factor'] * np.diag(Pb)
            HMC_Options.update({'mass_mat_variances':mass_mat_variances})
        elif HMC_Options['mass_matrix_strategy']=='prior_precisions':
            if Model_Object._Use_Sparse:
                mass_mat_variances = HMC_Options['mass_matrix_scale_factor'] * (1./Pb.diagonal())
            else:
                mass_mat_variances = HMC_Options['mass_matrix_scale_factor'] * (1./np.diag(Pb))
            HMC_Options.update({'mass_mat_variances':mass_mat_variances})
        elif HMC_Options['mass_matrix_strategy']=='modeled_precisions':
            if Model_Object._Use_Sparse:
                mass_mat_variances = HMC_Options['mass_matrix_scale_factor'] * (1./Model_Object._B0.diagonal())
            else:
                mass_mat_variances = HMC_Options['mass_matrix_scale_factor'] * (1./np.diag(Model_Object._B0))
            HMC_Options.update({'mass_mat_variances':mass_mat_variances})
        elif HMC_Options['mass_matrix_strategy']in['identity','scaled_identity']:
            mass_mat_variances = HMC_Options['mass_matrix_scale_factor'] * np.ones(Model_Object._state_size)
            HMC_Options.update({'mass_mat_variances':mass_mat_variances})
        else:
            raise ValueError("mass_matrix_strategy[ "+HMC_Options['mass_matrix_strategy']+"] is not implemented!")



        print '\rSolving Linear system --> Inv_B          ',
        sys.stdout.flush()
        #print'\r                                                       \r',
        print('\r'+'\r'.rjust(50)),
        if use_sparse:
            # scipy.sparse.linalg should be used
            if system_solver=='splu':
                #sparse LU solver
                #print Pb
                invB_OBJ = sparse_linalg.splu( Pb.tocsc() )
                HMC_Options.update({'invB_OBJ':invB_OBJ , 'p_LU':None, 'p_IV':None})
                #print invB_OBJ

                ## .............
                #Xa = HMC_Sampling_Process(Model_Object, Forecast_State, Observation, HMC_Options,  \
                #                          Initial_State = Forecast_State  \
                #                         )

            elif system_solver=='lu':
                #dense LU solver
                p_lu , p_iv = linalg.lu_factor( Pb.toarray() )
                HMC_Options.update({'p_LU':p_lu, 'p_IV':p_iv , 'invB_OBJ':None})
                # .............

            else:
                raise ValueError("Solver is not supported yet! ")

        else:
            # full np.linalg should be used
            #dense LU solver
            system_solver = 'lu'
            p_lu , p_iv = linalg.lu_factor( Pb )
            HMC_Options.update({'p_LU':p_lu, 'p_IV':p_iv , 'invB_OBJ':None})
            #print p_lu , p_iv
            # .............


        # .............
        if Sampling_Strategy.lower() in['hmc','vanilla_hmc','original_hmc']:
            Xa = HMC_Sampling_Process(Model_Object, Forecast_State, Observation, HMC_Options,  \
                                      Forecast_State  , screen_output\
                                     )
        elif Sampling_Strategy.lower() in['nuts','nuts_hmc','hmc_nuts','automatic_hmc']:
            # HMC_NUTS_Sampling_Process returns: Posterior_Ensemble, Burned_Ensemble , Stationary_Ensemble
            # What we need finally will be only the posterior. The HMC_Statistics are updated in the HMC_Options.HMC_Statistics dictionary.
            Xa,_,_ = HMC_NUTS_Sampling_Process(Model_Object, Forecast_State, Observation, HMC_Options,  \
                                      Initial_State=Forecast_State  , Screen_out=screen_output\
                                     )

        else:
            raise ValueError("Sampling_Strategy: "+Sampling_Strategy+" is not supported!. Vanilla_HMC, and NUTS are supported so far!")


        #print Xa
        #print 'DEBUG-THEN-REMOVE:'
        #print 'Analysis: ', np.mean(Xa,1)
        #J_val = HMC_Potential_Energy(Model_Object, np.mean(Xa,1), Forecast_State, Observation, HMC_Options )
        #print 'with J value: ',J_val
        #sys.exit("Manual CheckPoint!")


        # separate Statistics monitor/dictionary for convenience
        HMC_Statistics = HMC_Options['HMC_Statistics']
        return Xa , HMC_Statistics





def HMC_Sampling_Process(Model_Object, Forecast_State, observation, HMC_Options,  Initial_State = None ,  Screen_out=False):
#Model_Object,    \
#                                          Forecast_State,  \
#                                          Observation,     \
#                                          HMC_Options,     \
#                                          Mass_Mat_Variances \
#                                          InvB0_OBJ     = invB_OBJ                  \
                                          #Initial_State = Forecast_State   \
    """
    sequentially generate samples from the target/posterior PDF
    """


    #print 'initial_state' ,initial_state
    #print 'use_Sparse',use_Sparse
    #print 'InvB0_OBJ',InvB0_OBJ
    #print 'P_LU',P_LU
    #print 'P_PIV',P_PIV
    #print 'System_Solver',System_Solver
    #print 'S_PLU',S_PLU
    #print 'S_PIV',S_PIV

    ensemble_size = HMC_Options['ensemble_size']



    if Initial_State is None:
        Initial_State = Forecast_State.copy()


    xf  = Forecast_State.copy()
    X_n = Initial_State.copy()

    BurnIn_Steps = HMC_Options['Hamiltonian_BurnIn_Steps']
    Mixing_Steps = HMC_Options['Hamiltonian_Mixing_Steps']

    StateSize = np.size(X_n)

    # Initialize Kinetic Energy distribution paramters
    #M , sqrtM , invM = HMC_Initialize_Kinetic_Parameters(Model_Object , HMC_Options )
    HMC_Initialize_Kinetic_Parameters(Model_Object , HMC_Options )
    #print 'M',M
    #print 'sqrtM ',sqrtM
    #print 'invM', invM



    # burn-in to achieve convergence. Convergence diagnostics may be considered!
    for burn_iter in xrange(BurnIn_Steps):
        #
        if Screen_out:
            X_tmp = X_n.copy()

        P_n = HMC_Generate_Momentum_Vec( StateSize, HMC_Options )
        #print 'P_n',P_n
        #print 'X_n 1', X_n
        #
        X_Star , P_Star = HMC_Hamiltonian_FWD(Model_Object, X_n, P_n, xf, observation, HMC_Options )
        #print 'X_Star',X_Star
        #print 'P_Star',P_Star

        H_P_X      = HMC_Total_Energy(Model_Object, X_n   , xf , P_n   , observation, HMC_Options )
        H_P_X_Star = HMC_Total_Energy(Model_Object, X_Star, xf , P_Star, observation, HMC_Options )

        #print 'X_n 2', X_n
        #print 'X_Star', X_Star
        #print 'H_P_X' , H_P_X
        #print 'H_P_X_Star' , H_P_X_Star

        #Acceptance probability and acceptance criteria
        Energy_Loss = H_P_X_Star - H_P_X
        prob = np.exp( - Energy_Loss )
        a_n = np.min([ 1 , prob ])
        if ( np.isnan(a_n) or np.isinf(a_n) ):
            a_n =0
        #
        HMC_Options['HMC_Statistics']['P_Acceptance'].append(a_n)
        #
        u_n = np.random.rand(1)[0]
        if a_n > u_n:
            X_n = X_Star.copy()
            #print a_n
            #Monitor accepted states:
            HMC_Options['HMC_Statistics']['Accepted_Proposals'].append(True)
        else:
            # Otherwise the proposal is rejected and the last vector is kept
            # Monitor negative-log of the target kernel:
            HMC_Options['HMC_Statistics']['Accepted_Proposals'].append(False)


        if Screen_out:
            #
            J_val = HMC_Potential_Energy(Model_Object, X_Star, xf, observation, HMC_Options )
            out_str = '\r>> Chain Burning-in; Step['+str(burn_iter+1).rjust(4)+']; P(Accept)='+str(a_n).rjust(18)+' |X_S-X|='+str(linalg.norm(X_Star-X_tmp)).rjust(18)+' J(X_S)='+str(J_val).rjust(18)+'     ',
            print out_str[0],
            sys.stdout.flush()
            print('\r'+'\r'.rjust(100)),
            #

        # Monitor negative-log of the target kernel:
        if not Screen_out:
            J_val = HMC_Potential_Energy(Model_Object, X_Star, xf, observation, HMC_Options )
        HMC_Options['HMC_Statistics']['Kernel_NegLog_Vals'].append(J_val)



    # initialize the ensemble (Posterior)
    Posterior_Ensemble = np.zeros((StateSize , ensemble_size))
    #
    for i_ens in xrange(ensemble_size):
        #
        for mixing_iter in xrange(Mixing_Steps):
            #
            if Screen_out:
                X_tmp = X_n.copy()
            #
            P_n = HMC_Generate_Momentum_Vec(StateSize , HMC_Options)
            #
            X_Star , P_Star = HMC_Hamiltonian_FWD(Model_Object, X_n, P_n, xf, observation, HMC_Options )

            H_P_X = HMC_Total_Energy(Model_Object, X_n   , xf , P_n   , observation, HMC_Options )
            H_P_X_Star = HMC_Total_Energy(Model_Object, X_Star, xf , P_Star, observation, HMC_Options )


            #Acceptance probability and acceptance criteria
            Energy_Loss = H_P_X_Star - H_P_X
            prob = np.exp( - Energy_Loss )
            a_n = np.min([ 1 , prob ])
            if ( np.isnan(a_n) or np.isinf(a_n) ):
                a_n =0
            #
            HMC_Options['HMC_Statistics']['P_Acceptance'].append(a_n)
            #
            u_n = np.random.rand(1)[0]
            #
            if a_n > u_n:
                X_n = X_Star.copy()
                #Monitor accepted states:
                HMC_Options['HMC_Statistics']['Accepted_Proposals'].append(True)
            else:
                # Otherwise the proposal is rejected and the last vector is kept
                #Monitor accepted states:
                HMC_Options['HMC_Statistics']['Accepted_Proposals'].append(False)

            #
            if Screen_out:
                J_val = HMC_Potential_Energy(Model_Object, X_Star, xf, observation, HMC_Options )
                out_str = '\r>> Ensemble member['+str(i_ens+1).rjust(4)+']. Mixing Step['+str(mixing_iter+1).rjust(4)+']; P(Accept)='+str(a_n).rjust(18)+' |X_S-X|='+str(linalg.norm(X_Star-X_tmp)).rjust(18)+' J(X_S)='+str(J_val).rjust(18)+'     ',
                print out_str[0],
                sys.stdout.flush()
                print('\r'+'\r'.rjust(100)),
                #

            # Monitor negative-log of the target kernel:
            if not Screen_out:
                J_val = HMC_Potential_Energy(Model_Object, X_Star, xf, observation, HMC_Options )
            HMC_Options['HMC_Statistics']['Kernel_NegLog_Vals'].append(J_val)

        #
        Posterior_Ensemble[:,i_ens] = X_n.copy()

    return Posterior_Ensemble






def HMC_Total_Energy(Model_Object, U, Uf, P , Y,  HMC_Options):
    """
    Evaluate the Hamiltonian H = Potential + Kinetic Energy
    U    : state vector
    Uf   : forecast state
    Mass : inverse of the Mass Matrix
    Y    : observation vector

    """
    Potential_Energy = HMC_Potential_Energy(Model_Object, U, Uf, Y, HMC_Options )

    inv_M = HMC_Options['inv_M']
    use_sparse = HMC_Options['use_sparse']

    if use_sparse:
        scld_P = inv_M.dot(P)
        Kinetic_Energy = 0.5* P.dot(scld_P)
    else:
        scld_P = np.dot(inv_M,P)
        Kinetic_Energy = 0.5* np.dot(P,scld_P)

    #
    Hamiltonian = Potential_Energy + Kinetic_Energy

    return Hamiltonian






def HMC_Potential_Energy(model_Object,  state, forecast_state, observation, HMC_Options ):
    """
    J value
    """
    system_Solver = HMC_Options['linear_system_solver']
    invB_OBJ = HMC_Options['invB_OBJ']
    p_lu  = HMC_Options['p_LU']
    p_iv  = HMC_Options['p_IV']
    use_sparse = HMC_Options['use_sparse']

    if use_sparse:
        if system_Solver.lower() =='splu' and not(invB_OBJ is None):
            #forecast term of J
            devs = state  - forecast_state
            scld_devs = invB_OBJ.solve( devs)
            T1 = 0.5 * devs.dot(scld_devs)

        elif system_Solver.lower() =='lu' and not ( (p_lu is None) or (p_iv is None) ):
            #forecast term of J
            devs = state  - forecast_state
            scld_devs = linalg.lu_solve((p_lu,p_iv) , devs)
            T1 = 0.5 * devs.dot(scld_devs)

        else:
            raise ValueError(system_Solver+" is not implemented! and/or related arguments are not passed appropriately!")

        #observation term of J
        theo_observation = model_Object.obs_oper_prod_Vec(state)
        innovs = theo_observation - observation
        scld_innovs = model_Object.observation_Error_Cov_Inv_prod_Y(innovs)
        T2 = 0.5 * innovs.dot(scld_innovs)

        # Evaluating exponent
        Potential_Energy = T1+T2

    else:
        if p_lu is None or p_iv is None:
            raise ValueError("A factorized system solver must be passed: P_LU, P_IV must be passed!")
        else:
            #forecast term of J
            devs = state  - forecast_state
            scld_devs = linalg.lu_solve((p_lu,p_iv) , devs)
            T1 = 0.5 * np.dot(devs , scld_devs)

            #observation term of J
            theo_observation = model_Object.obs_oper_prod_Vec(state)
            innovs = theo_observation - observation
            scld_innovs = model_Object.observation_Error_Cov_Inv_prod_Y(innovs)
            T2 = 0.5 * np.dot(innovs , scld_innovs)

            # Evaluating exponent
            Potential_Energy = T1+T2


    HMC_Options['HMC_Statistics']['Func_Evals']+=1

    return Potential_Energy



def HMC_Potential_Energy_Gradient(model_Object,  state, forecast_state, observation, HMC_Options ):
    """
    dJ/dx
    """
    system_Solver = HMC_Options['linear_system_solver']
    invB_OBJ = HMC_Options['invB_OBJ']
    p_lu  = HMC_Options['p_LU']
    p_iv  = HMC_Options['p_IV']
    #print p_lu
    #print p_iv
    use_sparse = HMC_Options['use_sparse']

    if use_sparse:

        if system_Solver.lower() =='splu' and not(invB_OBJ is None):
            #forecast term of J
            devs = state  - forecast_state
            scld_devs = invB_OBJ.solve( devs)
            T1 = scld_devs

        elif system_Solver.lower() =='lu' and not ( (p_lu is None) or (p_iv is None) ):
            #forecast term of J
            devs = state  - forecast_state
            scld_devs = linalg.lu_solve((p_lu,p_iv) , devs)
            T1 = scld_devs

        else:
            raise ValueError(system_Solver+" is not implemented! and/or related arguments are not passed appropriately!")

        #observation term of J
        theo_observation = model_Object.obs_oper_prod_Vec(state)
        innovs           = theo_observation - observation
        obs_Jacobian     = model_Object.obs_doper_prod_dU(state)
        scld_innovs      = model_Object.observation_Error_Cov_Inv_prod_Y(innovs)
        T2               = (obs_Jacobian.T).dot(scld_innovs)

        # Evaluating exponent
        #print 'T1',T1
        #print 'T2',T2
        Potential_Energy_Gradient = T1+T2



    else:
        if p_lu is None or p_iv is None:
            raise ValueError("A factorized system solver must be passed: P_LU, P_IV must be passed!")
        else:
            #forecast term of J
            #print 'p_LU', p_LU
            #print 'p_IV', p_IV

            devs = state  - forecast_state           # change order -> change sign
            #print 'devs',devs
            scld_devs = linalg.lu_solve((p_lu,p_iv) , devs)
            T1 = scld_devs

            #observation term of J
            theo_observation = model_Object.obs_oper_prod_Vec(state)
            innovs           = theo_observation - observation  # change order -> change sign
            obs_Jacobian     = model_Object.obs_doper_prod_dU(state)
            scld_innovs      = model_Object.observation_Error_Cov_Inv_prod_Y(innovs)
            T2               = np.dot(obs_Jacobian.T , scld_innovs)

            # Evaluating exponent
            Potential_Energy_Gradient = T1+T2



    # #----------------------------Keep for debugging----------------------------<
    # # Complex-step FD are more accurate but will cause pain through other modules!
    # # Also, complex-step may be problematic with time integrators in other packages such as HyPar...
    # # Validate the gradient:
    # Grad_FD = np.zeros(state.size)
    # eps = 1e-5
    # for i in range(state.size):
    #     X_1   = state.copy()
    #     X_2   = state.copy()
    #     X_1[i] = state[i] - eps
    #     X_2[i] = state[i] + eps
    #     F_1 = HMC_Potential_Energy(model_Object,  X_1, forecast_state, observation, HMC_Options )
    #     F_2 = HMC_Potential_Energy(model_Object,  X_2, forecast_state, observation, HMC_Options )
    #
    #     Grad_FD[i] = (F_2-F_1)/(2*eps)
    #     print 'Gradient [',i,']=',Potential_Energy_Gradient[i],' | FD-Grad [',i,']=',Grad_FD[i],' | Rel-Err = ',(Potential_Energy_Gradient[i]-Grad_FD[i])/Grad_FD[i]
    #     #
    #     #----------------------------Keep for debugging----------------------------<


    #
    HMC_Options['HMC_Statistics']['Grad_Evals']+=1
    #
    return Potential_Energy_Gradient
    #-----------------------


def HMC_Potential_Energy_Func_and_Gradient(model_Object,  state, forecast_state, observation, HMC_Options ):
    """
    Return both the potential function value and gradient
    """
    Potential_Energy          = HMC_Potential_Energy(model_Object,  state, forecast_state, observation, HMC_Options )
    Potential_Energy_Gradient = HMC_Potential_Energy_Gradient(model_Object,  state, forecast_state, observation, HMC_Options )

    return Potential_Energy , Potential_Energy_Gradient





def HMC_Initialize_Kinetic_Parameters(Model_Object , HMC_Options ):
    """
    define the mass matrix, it's inverse and it's square root
    """
    #system_Solver = HMC_Options['linear_system_solver']
    #invB_OBJ = HMC_Options['invB_OBJ']
    #p_lu  = HMC_Options['p_LU']
    #p_iv  = HMC_Options['p_LU']
    state_size = HMC_Options['state_size']
    use_sparse = HMC_Options['use_sparse']
    method = HMC_Options['mass_matrix_strategy']
    #scale_fac = HMC_Options['mass_matrix_scale_factor']
    M_variances = HMC_Options['mass_mat_variances']

    #print variances
    if not method.lower() in ['prior_variances', 'prior_precisions','modeled_precisions','identity']:
        raise ValueError("This method:[ ",method," ] to intialize the Mass matrix is not implemented")

    if use_sparse:
        ind = np.arange(0,state_size)
        M      = sparse.csr_matrix(  ( M_variances , (ind,ind)) , shape=(state_size,state_size) )
        sqrt_M = sparse.csr_matrix(  ( np.sqrt(M_variances) , (ind,ind)) , shape=(state_size,state_size) )
        inv_M  = sparse.csr_matrix(  ( (1./M_variances) , (ind,ind)) , shape=(state_size,state_size) )

    else:
        M      = np.diag( M_variances )
        sqrt_M = np.diag( np.sqrt(M_variances) )
        inv_M  = np.diag( 1./M_variances )

    HMC_Options.update({'M':M, 'inv_M':inv_M, 'sqrt_M':sqrt_M })





def HMC_Generate_Momentum_Vec( state_size , HMC_Options ):
    """
    randomly generate a synthetic momentum vector
    """
    sqrt_M = HMC_Options['sqrt_M']
    use_sparse = HMC_Options['use_sparse']

    rand_Vec = _utility.randn_Vec(state_size)
    if use_sparse:
        Potential = sqrt_M.dot(rand_Vec)
    else:
        Potential = np.dot(sqrt_M , rand_Vec)

    return Potential









def HMC_Hamiltonian_FWD(Model_Object, X , P, Xf , Y ,  HMC_Options , Perturb_step_size=True):
    """
    Propagate the Hamiltonian system forward in (pseudo) time
    (X,P): state in phase space to integrate forward
    Xf : forecast state
    Y  : observation vector
    """

    inv_M = HMC_Options['inv_M']
    Integrator = HMC_Options['Hamiltonian_Integrator']
    step_size_ref = HMC_Options['Hamiltonian_step_size']
    num_steps = HMC_Options['Hamiltonian_number_of_steps']
    use_sparse = HMC_Options['use_sparse']



    if not Integrator in['verlet','2stage','3stage','4stage']:
        raise ValueError("Hamiltonian integrator ["+Integrator+" ] is not supported!")


    # to keep the pattern, have local copy...
    X_n = X.copy()
    P_n = P.copy()

    if Perturb_step_size:
        # perturb step-size:
        u = ( (np.random.rand(1)[0])-0.5)*(0.4);
        step_size = (1+u) * step_size_ref
    else:
        step_size = step_size_ref

    if Integrator == 'verlet':
        h = step_size/2.0;
        if use_sparse:
            for step in xrange( num_steps ):
                X_n += h * inv_M.dot(P_n)
                DJ = HMC_Potential_Energy_Gradient(Model_Object,  X_n, Xf, Y, HMC_Options )
                P_n -= (2*h) * DJ ;
                X_n += h * inv_M.dot(P_n)
        else:
            for step in xrange( num_steps ):
                X_n += h * np.dot(inv_M,P_n)
                #print X
                DJ = HMC_Potential_Energy_Gradient(Model_Object,  X_n, Xf, Y, HMC_Options )
                #print 'DJ',DJ
                P_n -= (2*h) * DJ ;
                X_n += h * np.dot(inv_M,P_n)
        #get the proposal  that returned to be tested for acceptance or rejection.
        P_Star = P_n.copy()
        X_Star = X_n.copy() # P_Star = P_n # P_Star = P_n[:]: #no need for explicit copy: #no need for explicit copy

        #--------------

    elif Integrator == '2stage':
        h  = step_size
        a1 = (3 - np.sqrt(3)) / 6.
        a2 = 1 - 2*a1
        b1 = 0.5
        #
        if use_sparse:
            for step in xrange( num_steps ):
                #
                X_1 = X_n + (a1*h) * inv_M.dot(P_n)
                DJ  = HMC_Potential_Energy_Gradient(Model_Object,  X_1, Xf, Y, HMC_Options )
                P_1 = P_n - (b1*h) * DJ
                X_2 = X_1 + (a2*h) * inv_M.dot(P_1)
                DJ  = HMC_Potential_Energy_Gradient(Model_Object,  X_2, Xf, Y, HMC_Options )
                P_n = P_1 - (b1*h) * DJ ;
                X_n = X_2 + (a1*h) * inv_M.dot(P_n)

        else:
            for step in xrange( num_steps ):
                #
                X_1 = X_n + (a1*h) * np.dot(inv_M , P_n)
                DJ  = HMC_Potential_Energy_Gradient(Model_Object,  X_1, Xf, Y, HMC_Options )
                P_1 = P_n - (b1*h) * DJ
                X_2 = X_1 + (a2*h) * np.dot(inv_M , P_1)
                DJ  = HMC_Potential_Energy_Gradient(Model_Object,  X_2, Xf, Y, HMC_Options )
                P_n = P_1 - (b1*h) * DJ ;
                X_n = X_2 + (a1*h) * np.dot(inv_M , P_n)


        P_Star = P_n.copy()
        X_Star = X_n.copy() # P_Star = P_n # P_Star = P_n[:]: #no need for explicit copy: #no need for explicit copy




    elif Integrator == '3stage':
        #
        h  = step_size
        a1 = 0.11888010966548
        b1 = 0.29619504261126
        a2 = 0.5 - a1
        b2 = 1 - 2*b1
        #
        #print 'h= ',h
        if use_sparse:
            for step in xrange( num_steps ):
                #
                X_1 = X_n + (a1*h) * inv_M.dot(P_n)
                DJ  = HMC_Potential_Energy_Gradient(Model_Object,  X_1, Xf, Y, HMC_Options )
                P_1 = P_n - (b1*h) * DJ
                X_2 = X_1 + (a2*h) * inv_M.dot(P_1)
                DJ  = HMC_Potential_Energy_Gradient(Model_Object,  X_2, Xf, Y, HMC_Options )
                P_2 = P_1 - (b2*h) * DJ
                X_3 = X_2 + (a2*h) * inv_M.dot(P_2)
                DJ  = HMC_Potential_Energy_Gradient(Model_Object,  X_3, Xf, Y, HMC_Options )

                P_n = P_2 - (b1*h) * DJ ;
                X_n = X_3 + (a1*h) * inv_M.dot(P_n)

        else:
            for step in xrange( num_steps ):
                #
                X_1 = X_n + (a1*h) * np.dot(inv_M , P_n)
                DJ  = HMC_Potential_Energy_Gradient(Model_Object,  X_1, Xf, Y, HMC_Options )
                P_1 = P_n - (b1*h) * DJ
                X_2 = X_1 + (a2*h) * np.dot(inv_M , P_1)
                DJ  = HMC_Potential_Energy_Gradient(Model_Object,  X_2, Xf, Y, HMC_Options )
                P_2 = P_1 - (b2*h) * DJ
                X_3 = X_2 + (a2*h) * np.dot(inv_M , P_2)
                DJ  = HMC_Potential_Energy_Gradient(Model_Object,  X_3, Xf, Y, HMC_Options )

                P_n = P_2 - (b1*h) * DJ ;
                X_n = X_3 + (a1*h) * np.dot(inv_M , P_n)


        P_Star = P_n.copy()
        X_Star = X_n.copy() # P_Star = P_n # P_Star = P_n[:]: #no need for explicit copy: #no need for explicit copy


    elif Integrator == '4stage':
        #
        h = step_size
        a1 = 0.071353913450279725904
        a2 = 0.268548791161230105820
        b1 = 0.191667800000000000000
        b2 = 0.5 - b1
        a3 = 1 - 2*a1 - 2*a2;
        #
        if use_sparse:
            for step in xrange( num_steps ):
                #
                X_1 = X_n + (a1*h) * inv_M.dot(P_n)
                DJ  = HMC_Potential_Energy_Gradient(Model_Object,  X_1, Xf, Y, HMC_Options )
                P_1 = P_n - (b1*h) * DJ
                X_2 = X_1 + (a2*h) * inv_M.dot(P_1)
                DJ  = HMC_Potential_Energy_Gradient(Model_Object,  X_2, Xf, Y, HMC_Options )
                P_2 = P_1 - (b2*h) * DJ
                X_3 = X_2 + (a3*h) * inv_M.dot(P_2)
                DJ  = HMC_Potential_Energy_Gradient(Model_Object,  X_3, Xf, Y, HMC_Options )
                P_3 = P_2 - (b2*h) * DJ
                X_4 = X_3 + (a2*h) * inv_M.dot(P_3)
                DJ  = HMC_Potential_Energy_Gradient(Model_Object,  X_4, Xf, Y, HMC_Options )

                P_n = P_3 - (b1*h) * DJ ;
                X_n = X_4 + (a1*h) * inv_M.dot(P_n)

        else:
            for step in xrange( num_steps ):
                #
                X_1 = X_n + (a1*h) * np.dot(inv_M , P_n)
                DJ  = HMC_Potential_Energy_Gradient(Model_Object,  X_1, Xf, Y, HMC_Options )
                P_1 = P_n - (b1*h) * DJ
                X_2 = X_1 + (a2*h) * np.dot(inv_M , P_1)
                DJ  = HMC_Potential_Energy_Gradient(Model_Object,  X_2, Xf, Y, HMC_Options )
                P_2 = P_1 - (b2*h) * DJ
                X_3 = X_2 + (a2*h) * np.dot(inv_M , P_2)
                DJ  = HMC_Potential_Energy_Gradient(Model_Object,  X_3, Xf, Y, HMC_Options )

                P_3 = P_2 - (b2*h) * DJ
                X_4 = X_3 + (a2*h) * np.dot(inv_M , P_3)
                DJ  = HMC_Potential_Energy_Gradient(Model_Object,  X_4, Xf, Y, HMC_Options )

                P_n = P_3 - (b1*h) * DJ ;
                X_n = X_4 + (a1*h) * np.dot(inv_M , P_n)


        P_Star = P_n.copy()
        X_Star = X_n.copy() # P_Star = P_n # P_Star = P_n[:]: #no need for explicit copy: #no need for explicit copy


    else:
        raise ValueError("Hamiltonian integrator ["+Integrator+" ] is not supported!") # should not be reached...


    #print '>>>',X_Star , P_Star,'<<<<'
    return X_Star , P_Star



#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def HMC_NUTS_Sampling_Process(Model_Object,                \
                              Forecast_State,              \
                              Observation,                 \
                              HMC_Options,                 \
                              Initial_State = None,        \
                              Target_Acceptance_Prob=0.75, \
                              Screen_out=False             \
                             ):
    """
    sequentially generate samples from the target/posterior PDF
    NUTS is the NO-U-Turn sampler developed by Matt Hoffman (2011) as an automatically tunable verison of HMC. Here I am trying the NUTS with dual averaging to estimate the step size and the trajectory lenth automatically, where the mass matrix and the symplectic integrators are generalized. Theory is in progress...

    Target_Acceptance_Prob : should be between 0 and 1, and is a target HMC acceptance probability. Defaults to 0.6 if unspecified.
    """
    # backup hamiltonian number of steps for later use with Vanilla HMC if needed for comparison or so
    Hamiltonin_num_steps_bkup = HMC_Options['Hamiltonian_number_of_steps']
    HMC_Options['Hamiltonian_number_of_steps'] = 1

    #
    # Number of sampled point (from stationary distribution)
    Ensemble_Size = HMC_Options['ensemble_size']

    # DA forecast state. Taken as the mean of the prior usually.
    #xf  = Forecast_State.copy()

    if Initial_State is None: # set the initial state to the forecast if none is provided
        Initial_State = Forecast_State.copy()

    # dimension of the position in the phase-space. D/2 where D is the dimension of the phase space.
    State_Size = Model_Object._state_size

    # Number of steps in the dual averaging phase
    BurnIn_Steps = HMC_Options['Hamiltonian_BurnIn_Steps']
    Mixing_Steps = HMC_Options['Hamiltonian_Mixing_Steps']

    #Add NUTS-specific options to HMC_Options:
    HMC_Options.update( {'NUTS_Options':{'initial_step_size':HMC_Options['Hamiltonian_step_size']} } )
    HMC_Options.update( {'NUTS_Options':{'initial_step_size':1} } )


    # Initialize Kinetic Energy distribution paramters: Mass matrix, its inverse and its square root.
    HMC_Initialize_Kinetic_Parameters(Model_Object , HMC_Options )
    #print 'M',M
    #print 'sqrtM ',sqrtM
    #print 'invM', invM

    # Initialize the posterior ensemble array. (State_Size x Ensemble_Size)
    # Each ensemble member is stored as a colummn.
    Burned_Ensemble    = np.zeros((State_Size , BurnIn_Steps))  # saved only for testing. Should be removed later to save memory...
    Stationary_Ensemble = np.zeros((State_Size , Mixing_Steps*Ensemble_Size)) # Mixing steps can be dropped to save memory...
    Posterior_Ensemble = np.zeros((State_Size , Ensemble_Size)) # Mixing steps are dropped to save memory...

    # Start NUTS
    # Initialize Potential variable parameters:
    HMC_Initialize_Kinetic_Parameters(Model_Object , HMC_Options )

     #get the total energy and gradient at the initial state: (May/Will be skipped...)
    #( Initial_Potential_Energy, Initial_Potential_Energy_Gradient) = HMC_Potential_Energy_Func_and_Gradient(Model_Object,  Initial_State, Forecast_State, Observation, HMC_Options )

    # Find a reasonable step-size for the Hamiltonian integrator with a simple huristic:
    if Screen_out:
        out_str = '\r>> Inside NUTS; Finding Reasonable step size...'+'  '.rjust(60),
        print out_str[0],
        sys.stdout.flush()
        print('\r'+'\r'.rjust(100)),
        #

    reference_step_size = HMC_NUTS_reasonable_epsilon(Model_Object,  Initial_State, Forecast_State, Observation, HMC_Options , Screen_out)
    HMC_Options['NUTS_Options']['tuned_step_size'] = reference_step_size
    HMC_Options['Hamiltonian_step_size'] = reference_step_size
    #
    if Screen_out:
        out_str = '\r>> Inside NUTS; Reasonable step size found to be:'+str(reference_step_size).rjust(6)+'  '.rjust(45),
        print out_str[0],
        sys.stdout.flush()
        print('\r'+'\r'.rjust(100)),
        #


    # Parameters to the dual averaging algorithm.
    gamma = 0.05
    t0 = 10
    kappa = 0.75
    mu = np.log(10*reference_step_size)
    # Initialize dual averaging algorithm.
    step_size_bar = reference_step_size
    Hbar = 0

    # Burn-in stage:
    #Burned_Ensemble = ?
    #Potential_Energy          = Initial_Potential_Energy
    #Potential_Energy_Gradient = Initial_Potential_Energy_Gradient
    #
    #print '>>>>>>>>BurnIn_Steps:',BurnIn_Steps
    Burned_Ensemble[:,0] = Initial_State
    for burn_ind in xrange(1,BurnIn_Steps):
        #print '>>>>>>>>burn_ind:',burn_ind
        #if burn_ind==99:
        #    sys.exit("Manual CheckPoint")

        X_n = Burned_Ensemble[:,burn_ind-1]
        # resample a momenta
        P_n = HMC_Generate_Momentum_Vec( State_Size, HMC_Options )
        Total_Energy = HMC_Total_Energy(Model_Object, X_n , Forecast_State , P_n , Observation,  HMC_Options)
        #print 'X_n', X_n
        #print 'P_n', P_n
        #print 'Total_Energy beg of loop:', Total_Energy
        logu = np.log(np.random.rand()) - Total_Energy
        #logu = -Total_Energy - np.random.exponential()
        #print 'logu',logu

        # initialize tree:
        state_left    = X_n.copy()
        state_right   = X_n.copy()
        momenta_left  = P_n.copy()
        momenta_right = P_n.copy()

        # initial tree height:
        tree_height = 0

        # the number of valid points (so far only the initial point is considered valid)
        num_valid_points = 1

        # copy the previous state as proposal:
        Burned_Ensemble[:,burn_ind] = X_n


        # Main loop. Keep going until the criterion hits a zero
        s = 1  # s is the stop criterion (a binary flag...)
        while (s == 1):
            # choose a direction
            direction = 2*(np.random.rand()<0.5)-1  # > {-1 = left; 1=right}

            #double the size of the tree:
            if direction == -1:
                # go left:
                state_left, momenta_left, _, _, X_Star,  P_Star, n_Star, s_Star, alpha, n_alpha   = HMC_NUTS_Build_Tree(Model_Object, state_left , momenta_left, logu, direction, tree_height, Total_Energy, Forecast_State, Observation, HMC_Options)

            elif direction==1:
                # go right:
                _, _, state_right, momenta_right, X_Star, P_Star, n_Star, s_Star, alpha, n_alpha = HMC_NUTS_Build_Tree(Model_Object, state_right , momenta_right, logu, direction, tree_height, Total_Energy, Forecast_State, Observation, HMC_Options)
                #
            else:
                raise ValueError("This shouldn't be visited. direction should be -+1. direction:" +direction+"returned!" )

            #print 's_Star', s_Star
            #print 'returned X_Star', X_Star
            #print 'returned P_Star', P_Star
            # Use Metropolis-Hastings to decide whether or not to move to
            # a point from the half-tree we just generated.
            lst_Accept_Prob = 0
            lst_Energy_Loss = np.infty
            if ( (s_Star == 1) and (np.random.rand() < n_Star/num_valid_points) ):
                Burned_Ensemble[:,burn_ind] = X_Star.copy()
                #
                Proposal_Total_Energy = HMC_Total_Energy(Model_Object, X_Star , Forecast_State, P_Star , Observation,  HMC_Options)
                Energy_Loss = Proposal_Total_Energy - Total_Energy
                prob = np.exp( - Energy_Loss )
                Accept_Prob = np.min([ 1 , prob ]) # acceptance probability. update statistics:
                #
                lst_Accept_Prob = Accept_Prob
                lst_Energy_Loss = Energy_Loss
                #
                #print 'Taken',Accept_Prob
                #print 'BurnIn step:',burn_ind,'   Accepted with  Accept_Prob',Accept_Prob
                # This check should not be reached!
                if ( np.isnan(Accept_Prob) or np.isinf(Accept_Prob) ):
                    print ("WARNINIG: Convergence achieved but Acceptance probability is: "+str(lst_Accept_Prob)+" Energy Loss= "+str(lst_Energy_Loss)+", s_Star="+s_Star)
                    Accept_Prob =0
                    # copy the previous state as proposal:
                    #Burned_Ensemble[:,burn_ind] = Burned_Ensemble[:,burn_ind-1]
                #logp = logpprime;
                #grad = gradprime;
            else:
                # copy the previous state as proposal:
                Proposal_Total_Energy = HMC_Total_Energy(Model_Object, X_Star , Forecast_State, P_Star , Observation,  HMC_Options)
                Energy_Loss = Proposal_Total_Energy - Total_Energy
                prob = np.exp( - Energy_Loss )
                Accept_Prob = min( 1 , prob ) # acceptance probability. update statistics:
                #print 'Not Taken', Accept_Prob
                #Accept_Prob = 0
            #    Burned_Ensemble[:,burn_ind] = Burned_Ensemble[:,burn_ind-1]


            #print 'BurnIn step:',burn_ind,' <OUT>    Accept_Prob',Accept_Prob
            HMC_Options['HMC_Statistics']['P_Acceptance'].append(lst_Accept_Prob)

            #Monitor accepted states:
            if lst_Accept_Prob:
                HMC_Options['HMC_Statistics']['Accepted_Proposals'].append(True)
            else:
                HMC_Options['HMC_Statistics']['Accepted_Proposals'].append(False)

            # Update number of valid points we've seen.
            num_valid_points+= n_Star

            # Decide if it's time to stop.
            s = s_Star and HMC_NUTS_stop_criterion(Model_Object,  state_left, state_right , momenta_left, momenta_right )
            # Increment depth.
            tree_height+=1
            #print 's=',s

        # Adapt the step size (only in the burn-in phase)
        eta = 1./( burn_ind + t0 )
        Hbar = (1-eta)*Hbar + eta*(Target_Acceptance_Prob - alpha/n_alpha)
        step_size = np.exp(mu - np.sqrt(burn_ind)/gamma * Hbar)
        HMC_Options['Hamiltonian_step_size'] = step_size
        eta = (burn_ind)**(-kappa)
        step_size_bar = np.exp( (1 - eta) * np.log(step_size_bar) + eta * np.log(step_size) )
        #print 'step_size_bar=',step_size_bar

        # during sampling step size-size will be fixed to step_size_bar
        if Screen_out:
            #
            J_val = HMC_Potential_Energy(Model_Object, Burned_Ensemble[:,burn_ind], Forecast_State, Observation, HMC_Options )
            out_str = '\r>> Inside NUTS; BurningIn Stage. Step:'+str(burn_ind)+' stop criterion=['+str(s).rjust(2)+']; P(Accept)='+str(lst_Accept_Prob).rjust(8)+' |X_S-X|='+str(linalg.norm(Stationary_Ensemble[:,burn_ind]-Stationary_Ensemble[:,burn_ind-1])).rjust(18)+' J(X_S)='+str(J_val).rjust(18)+'     ',
            print out_str[0],
            sys.stdout.flush()
            print('\r'+'\r'.rjust(100)),
            #

        # Monitor negative-log of the target kernel:
        if not Screen_out:
            J_val = HMC_Potential_Energy(Model_Object, Burned_Ensemble[:,burn_ind], Forecast_State, Observation, HMC_Options )
        HMC_Options['HMC_Statistics']['Kernel_NegLog_Vals'].append(J_val)


    #Burn-in phase complete. Sampling process starts...
    # Sampling stage:
    #Stationary_Ensemble=?  repeat as in the burn-in steps... May be in the same loop instead or rewritting it!



    #

    # fix the step size to the adapted one:
    HMC_Options['Hamiltonian_step_size'] = step_size_bar
    #print 'sampling reference step size: ', step_size_bar
    #
    Stationary_Ensemble[:,0] = Burned_Ensemble[:,-1]
    #
    #print '>>>>>>>>BurnIn_Steps:',BurnIn_Steps
    for ens_ind in xrange(1,Ensemble_Size*Mixing_Steps):
        #if ens_ind==599:
        #    sys.exit("Manual CheckPoint!")

        #print '<<<<<<<ens_ind: ',ens_ind
        # resample a momenta
        X_n = Stationary_Ensemble[:,ens_ind-1]
        P_n = HMC_Generate_Momentum_Vec( State_Size, HMC_Options )
        Total_Energy = HMC_Total_Energy(Model_Object, X_n , Forecast_State, P_n , Observation,  HMC_Options)
        #print 'Total_Energy beg of loop:', Total_Energy
        logu = np.log(np.random.rand()) - Total_Energy
        #logu = -Total_Energy - np.random.exponential()

        # initialize tree:
        state_left    = X_n.copy()
        state_right   = X_n.copy()
        momenta_left  = P_n.copy()
        momenta_right = P_n.copy()


        # initial tree height:
        tree_height = 0

        # the number of valid points (so far only the initial point is considered valid)
        num_valid_points = 1

        # copy the previous state as proposal:
        Stationary_Ensemble[:,ens_ind] = X_n


        # Main loop. Keep going until the criterion hits a zero
        s = 1  # s is the stop criterion (a binary flag...)

        while (s == 1):
            # choose a direction
            direction = 2*(np.random.rand()<0.5)-1  # > {-1 = left; 1=right}

            #double the size of the tree:
            if direction == -1:
                # go left:
                state_left, momenta_left, _, _, X_Star,  P_Star, n_Star, s_Star, alpha, n_alpha   = HMC_NUTS_Build_Tree(Model_Object, state_left , momenta_left, logu, direction, tree_height, Total_Energy, Forecast_State, Observation, HMC_Options)

            elif direction==1:
                # go right:
                _, _, state_right, momenta_right, X_Star, P_Star, n_Star, s_Star, alpha, n_alpha = HMC_NUTS_Build_Tree(Model_Object, state_right , momenta_right, logu, direction, tree_height, Total_Energy, Forecast_State, Observation, HMC_Options)
                #
            else:
                raise ValueError("This shouldn't be visited. direction should be -+1. direction:" +direction+"returned!" )

            #print 's_Star', s_Star
            #print 'returned X_Star', X_Star
            #print 'returned P_Star', P_Star
            # Use Metropolis-Hastings to decide whether or not to move to
            # a point from the half-tree we just generated.
            lst_Accept_Prob = 0
            lst_Energy_Loss = np.infty
            if ( (s_Star == 1) and (np.random.rand() < n_Star/num_valid_points) ):
                Stationary_Ensemble[:,ens_ind] = X_Star
                #
                Proposal_Total_Energy = HMC_Total_Energy(Model_Object, X_Star , Forecast_State, P_Star , Observation,  HMC_Options)
                Energy_Loss = Proposal_Total_Energy - Total_Energy
                prob = np.exp( - Energy_Loss )
                Accept_Prob = min( 1 , prob ) # acceptance probability. update statistics:
                #
                lst_Accept_Prob = Accept_Prob
                lst_Energy_Loss = Energy_Loss
                #
                #print 'Taken',Accept_Prob
                #print 'BurnIn step:',ens_ind,'   Accepted with  Accept_Prob',Accept_Prob
                # This check should not be reached!
                # This check should not be reached!
                if ( np.isnan(Accept_Prob) or np.isinf(Accept_Prob) ):
                    print ("WARNINIG: Convergence achieved but Acceptance probability is: "+str(lst_Accept_Prob)+" Energy Loss= "+str(lst_Energy_Loss)+", s_Star="+s_Star)
                    Accept_Prob =0
                    # copy the previous state as proposal:
                    #Stationary_Ensemble[:,ens_ind] = Stationary_Ensemble[:,ens_ind-1]

            else:
                Proposal_Total_Energy = HMC_Total_Energy(Model_Object, X_Star , Forecast_State, P_Star , Observation,  HMC_Options)
                Energy_Loss = Proposal_Total_Energy - Total_Energy
                prob = np.exp( - Energy_Loss )
                Accept_Prob = min( 1 , prob ) # acceptance probability. update statistics:
                #print 'Not Taken', Accept_Prob
                #Accept_Prob = 0

            #print 'BurnIn step:',burn_ind,' <OUT>    Accept_Prob',Accept_Prob
            # update HMC statistics
            HMC_Options['HMC_Statistics']['P_Acceptance'].append(lst_Accept_Prob)
            #

            #Monitor accepted states:
            if lst_Accept_Prob:
                HMC_Options['HMC_Statistics']['Accepted_Proposals'].append(True)
            else:
                HMC_Options['HMC_Statistics']['Accepted_Proposals'].append(False)


            # Update number of valid points we've seen.
            num_valid_points+= n_Star

            # Decide if it's time to stop.
            s = s_Star and HMC_NUTS_stop_criterion(Model_Object,  state_left, state_right , momenta_left, momenta_right )
            # Increment depth.
            tree_height+=1
            #print 's=',s

            if Screen_out:
                #
                J_val = HMC_Potential_Energy(Model_Object, Stationary_Ensemble[:,ens_ind], Forecast_State, Observation, HMC_Options )
                out_str = '\r>> Inside NUTS; Sampling Stage. stop criterion=['+str(s).rjust(2)+']; P(Accept)='+str(lst_Accept_Prob).rjust(8)+' |X_S-X|='+str(linalg.norm(Stationary_Ensemble[:,ens_ind]-Stationary_Ensemble[:,ens_ind-1])).rjust(18)+' J(X_S)='+str(J_val).rjust(18)+'     ',
                print out_str[0],
                sys.stdout.flush()
                print('\r'+'\r'.rjust(100)),
                #

            # Monitor negative-log of the target kernel:
            if not Screen_out:
                J_val = HMC_Potential_Energy(Model_Object, Burned_Ensemble[:,burn_ind], Forecast_State, Observation, HMC_Options )
            HMC_Options['HMC_Statistics']['Kernel_NegLog_Vals'].append(J_val)


        #eta = 1./( ens_ind + t0 )
        #Hbar = (1-eta)*Hbar + eta*(Target_Acceptance_Prob - alpha/n_alpha)


    Posterior_Ensemble = Stationary_Ensemble[:,::Mixing_Steps]
    #print np.mean(Posterior_Ensemble,1)
    #J_val = HMC_Potential_Energy(Model_Object, np.mean(Posterior_Ensemble,1) , Forecast_State, Observation, HMC_Options )
    #print 'Mean of the posterior ensemble has J=', J_val
    #sys.exit("Manual CheckPoint!")

    #
    HMC_Options['Hamiltonian_number_of_steps'] = Hamiltonin_num_steps_bkup

    return Posterior_Ensemble, Burned_Ensemble , Stationary_Ensemble
    #------------------------------------------------------




def HMC_NUTS_Build_Tree(Model_Object,  in_state, in_momenta , logu , direction, tree_height, initial_total_energy , xf , yobs, HMC_Options , alpha_Star_in=0, n_alpha_Star_in=0):
    """
    Build a binary tree given the current tree width:
    xf: the forecast state,
    yobs: the observation vector
    """
    #print '*'
    #Because of recursion, we need self copies for safety
    X_in = in_state.copy()
    P_in = in_momenta.copy()
    j = tree_height # not necessary


    if j==0:
        # step forward the given state and the given momentum in the given direction
        # update the direction of the Hmiltonian integrator first,
        HMC_Options['Hamiltonian_step_size'] = direction* np.abs(HMC_Options['Hamiltonian_step_size'])
        X_Star , P_Star = HMC_Hamiltonian_FWD(Model_Object, X_in, P_in, xf, yobs, HMC_Options, Perturb_step_size=False ) # let's try the effect of this switch later. keep it off for now
        #
        proposal_total_Energy = HMC_Total_Energy(Model_Object, X_Star , xf, P_Star , yobs, HMC_Options )

        # check if the proposed state is in the slice:
        n_Star = logu< -proposal_total_Energy

        # rough flag of the simulation accuracy
        s_Star = logu-1000 < -proposal_total_Energy
        #print 'rough s_Star',s_Star

        # prepare for return fromt the initial level of the tree:
        state_left    = X_Star.copy()
        state_right   = X_Star.copy()
        momenta_left  = P_Star.copy()
        momenta_right = P_Star.copy()

        Energy_Loss = proposal_total_Energy - initial_total_energy
        #print 'proposal_total_Energy', proposal_total_Energy
        #print 'initial_total_energy', initial_total_energy
        #chk_initial_Energy = HMC_Total_Energy(Model_Object, X_in , xf, P_in , yobs, HMC_Options )
        #print 'chk_initial_Energy', chk_initial_Energy
        #print 'Energy_Loss', Energy_Loss

        prob = np.exp( - Energy_Loss )
        alpha_Star = np.min([ 1 , prob ]) # acceptance probability. update statistics:
        n_alpha_Star = 1
        if ( np.isnan(alpha_Star) or np.isinf(alpha_Star) ):
            print ("WARNING: this should not happen! a single acceptable step is rejected!")
            #alpha_Star =0
            #n_alpha_Star = 0


    elif j>0:
        # Recursively build right-and-left subtrees:
        state_left, momenta_left, state_right, momenta_right, X_Star, P_Star, n_Star, s_Star, alpha_Star, n_alpha_Star = \
                                                    HMC_NUTS_Build_Tree(Model_Object,  X_in, P_in , logu , direction, j-1, initial_total_energy , xf , yobs, HMC_Options , alpha_Star_in, n_alpha_Star_in)

        # on convergence stop and return optimal states:
        if (s_Star ==1):
            #
            if (direction == -1):
                # get left states:
                state_left, momenta_left, _, _, X_Star_2, P_Star_2, n_Star_2, s_Star_2, alpha_Star_2, n_alpha_Star_2 = \
                                                    HMC_NUTS_Build_Tree(Model_Object,  state_left.copy() , momenta_left.copy() , logu , direction, j-1, initial_total_energy , xf , yobs, HMC_Options , alpha_Star_in, n_alpha_Star_in)

            elif (direction ==1):
                # get right states:
                _, _, state_right, momenta_right, X_Star_2, P_Star_2, n_Star_2, s_Star_2, alpha_Star_2, n_alpha_Star_2 = \
                                                    HMC_NUTS_Build_Tree(Model_Object,  state_right.copy() , momenta_right.copy()  , logu , direction, j-1, initial_total_energy , xf , yobs, HMC_Options , alpha_Star_in, n_alpha_Star_in)


            else:
                raise ValueError("direction must be either +1 or -1. direction: "+direction+" is not recognized!")


            # Choose which subtree to propagate a sample from:
            if (n_Star + n_Star_2)!=0:
                if (np.random.rand() < n_Star_2/(n_Star + n_Star_2)):
                    X_Star = X_Star_2.copy()
                    P_Star = P_Star_2.copy()

            # update the number of valid states
            n_Star += n_Star_2

            # update stopping criterion:
            s_Star = s_Star_2 and HMC_NUTS_stop_criterion(Model_Object,  state_left, state_right , momenta_left, momenta_right )

            # update acceptance probability stats
            alpha_Star = alpha_Star_in + alpha_Star_2
            alpha_Star = min(1,alpha_Star) #?
            n_alpha_Star = n_alpha_Star_in + n_alpha_Star_2

            if ( np.isnan(alpha_Star) or np.isinf(alpha_Star) ):
                print ("WARNING: this should not happen! a single acceptable step is rejected!")


    else:
        raise ValueError("Level error in the tree! Negative passed!!")

    #print 'in level: ',j, ' in the tree. more P(accept)=', alpha_Star
    #print HMC_Options['Hamiltonian_step_size']
    #print HMC_Options['Hamiltonian_number_of_steps']
    #print 'X_in', X_in
    #print 'X_Star',X_Star
    #print direction

    return state_left, momenta_left, state_right, momenta_right, X_Star, P_Star, n_Star, s_Star, alpha_Star, n_alpha_Star




def HMC_NUTS_stop_criterion(Model_Object,  state_left, state_right , momenta_left, momenta_right ):
    """
    """
    state_diff = state_right - state_left
    criterion = ( np.dot(state_diff, momenta_left) >=0 ) and ( np.dot(state_diff, momenta_right) >=0 )

    return criterion



def HMC_NUTS_reasonable_epsilon(Model_Object,  Initial_State, Forecast_State, Observation, HMC_Options , Screen_Out=False):
    """
    Use a simple huristic to find a reasonable step size for the Hamiltonian trajectory.
    This should work fine for both the standard HMC and the NUTS version.
    """
    X_n = Initial_State.copy()
    xf  = Forecast_State.copy()
    yobs = Observation.copy()
    State_Size = Model_Object._state_size

    try:
        #step_size = HMC_Options['NUTS_Options']['initial_step_size']
        step_size = np.abs(HMC_Options['Hamiltonian_step_size'])
    except:
        step_size = 1
        #HMC_Options['NUTS_Options'].update({'initial_step_size': step_size})
        HMC_Options.update({'Hamiltonian_step_size': step_size})

    #HMC_Options['NUTS_Options'].update({'tuned_step_size': step_size})


    # get the total energy and gradient at the initial state: (May/Will be skipped...)
    #Initial_Potential_Energy, Initial_Potential_Energy_Gradient = HMC_Potential_Energy_Func_and_Gradient(Model_Object,  X_n, xf, yobs, HMC_Options )

    # Start modifying the steps size:
    # Generate a random momentum: Will be fixed here
    P_n = HMC_Generate_Momentum_Vec( State_Size, HMC_Options )

    # step forward the initial state and the current/initial momentum
    X_Star , P_Star = HMC_Hamiltonian_FWD(Model_Object, X_n, P_n, xf, yobs, HMC_Options, Perturb_step_size=False )
    #Potential_Energy, Potential_Energy_Gradient = HMC_Potential_Energy_Func_and_Gradient(Model_Object,  Initial_State, xf, yobs, HMC_Options )

    H_P_X = HMC_Total_Energy(Model_Object, X_n, xf, P_n, yobs, HMC_Options )
    H_P_X_Star = HMC_Total_Energy(Model_Object, X_Star, xf , P_Star, yobs, HMC_Options )
    Energy_Loss = H_P_X_Star - H_P_X
    #print 'getting reasonable step size'
    #print "HMC_Options['Hamiltonian_step_size']",HMC_Options['Hamiltonian_step_size']
    #print 'xf',xf
    #print 'yobs',yobs
    #print 'X_n',X_n
    #print
    #print 'X_Star',X_Star
    #print 'P_n',P_n
    #print 'P_Star',P_Star
    #print 'Energy_Loss',Energy_Loss
    Accept_Prob = min(1,np.exp( - Energy_Loss ) )
    #Accept_Prob = min(1,np.exp( - Energy_Loss ) )
    a = 2*(Accept_Prob>0.5) - 1

    # avoid division by 0
    if Accept_Prob==0:
       Accept_Prob=1e-100

    while((Accept_Prob**a) > (2**(-a)) ):    # modify the current step size if the probability unacceptable
        step_size = step_size * (2**a)
        #print 'step_size',step_size
        HMC_Options['Hamiltonian_step_size'] = step_size
        #
        # advance the initial state and momentem with the proposed step size
        X_Star , P_Star = HMC_Hamiltonian_FWD(Model_Object, X_n, P_n, xf, yobs, HMC_Options, Perturb_step_size=False )
        H_P_X_Star = HMC_Total_Energy(Model_Object, X_Star, xf , P_Star, yobs, HMC_Options )
        Energy_Loss = H_P_X_Star - H_P_X
        Accept_Prob = min(1,np.exp( - Energy_Loss ) )
        #
        #print "HMC_Options['Hamiltonian_step_size']",HMC_Options['Hamiltonian_step_size']
        #print 'xf',xf
        #print 'yobs',yobs
        #print 'X_n',X_n
        #print
        #print 'X_Star',X_Star
        #print 'P_n',P_n
        #print 'P_Star',P_Star
        #print 'Energy_Loss',Energy_Loss
        #
        if Screen_Out:
            out_str = '\r>> Tuning step size. Step size= '+str(step_size).rjust(4)+']; P(Accept)='+str(Accept_Prob).rjust(18)+'     '.rjust(50),
            print out_str[0],
            sys.stdout.flush()
            print('\r'+'\r'.rjust(100)),

            #
        if Accept_Prob==0:
            Accept_Prob=1e-25

    #print 'Accept_Prob = ', Accept_Prob,'. Step size = ',step_size,' Is it a reasonable step size!'

    reasonable_step_size = step_size

    # This output should be removed permanently later
    if Screen_Out:
        out_str = '\r>> Tuning step size. Step size tuned to: '+str(reasonable_step_size).rjust(5)+' => Accept_Prob = '+ str(Accept_Prob)+'     '.rjust(40)
        print out_str
        #sys.stdout.flush()
        #print('\r'+'\r'.rjust(100)),

    return reasonable_step_size
