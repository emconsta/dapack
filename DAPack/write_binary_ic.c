#include <stdio.h>
#include <stdlib.h>
#include <string.h>
// write_binary_ic.c
// this is incorrect for more than one dimension...
// corrections will be made once 1D models are working properly
int main()
{


   int n_dims , n_vars , grid_size , stateVec_size ;
   double *grid , *stateVec , *arngd_state;
   FILE *fptr , *out;


   /*---------------------------------------------------------------------------
   |  read grid and state vector information from the temporary text file      \
   \  text file name is fixed here to "op.dat.txt"  as decided in Hypar_DA     |
   ---------------------------------------------------------------------------*/
   if ((fptr=fopen("op.dat.txt","r"))==NULL){
       printf("Error! opening file");
       return 1;         /* Program exits if file pointer returns NULL. */
   }

   // read grid size and state vector size information
   fscanf(fptr,"%d",&n_dims);
   fscanf(fptr,"%d",&n_vars);
   fscanf(fptr,"%d",&grid_size);
   fscanf(fptr,"%d",&stateVec_size);

   // read grid and state vector entries
   grid     = (double*) calloc (grid_size     , sizeof(double));
   stateVec = (double*) calloc (stateVec_size , sizeof(double));
   for (int i=0;i<grid_size;i++)fscanf(fptr,"%lf",&grid[i]);
   for (int i=0;i<stateVec_size;i++)fscanf(fptr,"%lf",&stateVec[i]);
   fclose(fptr);

    //printf("grid size %d \n state vecsize %d \n",grid_size , stateVec_size);
    //for(int i=0;i<stateVec_size;i++) printf("%1.16e\n",stateVec[i]);





   /*---------------------------------------------------------------------------
   \  write the grid and the state as initial condition in binary format       \
   ---------------------------------------------------------------------------*/
    /* write initial solution file */
    if ((out = fopen("initial.inp","wb"))==NULL){
       printf("Error! opening file");
       return 1;         /* Program exits if file pointer returns NULL. */
    }

    // reorder the state vector components:
    arngd_state = (double*) calloc (stateVec_size , sizeof(double));
    for(int grd=0;grd<grid_size;grd++)
       for (int var=0;var<n_vars;var++)
           arngd_state[grd*n_vars + var] = stateVec[grid_size*var+grd];


    #if 0
    if (n_dims==1){
        for(int grd=0;grd<grid_size;grd++)
            for (int var=0;var<n_vars;var++){
                arngd_state[grd*n_vars + var] = stateVec[grid_size*var+grd];
            }
        }
    else if (n_dims==2){
        printf("Error!: Not yet implemented");
        return(1);
        }
    else if (n_dims==3){
        printf("Error!: Not yet implemented");
        return(1);
        }
    else{
        printf("Error!: n_dims cannot exceed 3!");
        return(1);
        }
    #endif



    // write grid and state vector entries
    fwrite(grid,sizeof(double),grid_size,out);
    fwrite(stateVec,sizeof(double),stateVec_size,out);
    //fwrite(arngd_state,sizeof(double),stateVec_size,out);
    fclose(out);

    /* clean up */
    free(grid);
    free(stateVec);
    free(arngd_state);

   printf("Binary initial condition written\n");


   #if 0
   //rescanning-(debugging)-----------------------------------------------------
   printf("\n^^^^^^^^^^^^^^^^^^^^^^^\n Rescanning\n^^^^^^^^^^^^^^^^^^^^^^^^^^^\n");
   if ((fptr=fopen("initial.inp","rb"))==NULL){
       printf("Error! opening file");
       return 1;         /* Program exits if file pointer returns NULL. */
   }
   fread(grid,sizeof(double),grid_size,fptr);
   fread(stateVec,sizeof(double),stateVec_size,fptr);
   fclose(fptr);
   //
   for(int i=0;i<stateVec_size;i++) printf("%1.16e\n",stateVec[i]);
   //rescanning-----------------------------------------------------------------
   #endif


   return 0;
}
