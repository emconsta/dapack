import os
import sys

import shutil

from _filter_base import DAfilterBase
import numpy as np
from scipy import linalg
import collections

from _Assimilation_Filters import *

import _utility

#================================================================================================================================#
#  A class describing the data assimilation filter objects. Should be separated to a new file later...
#================================================================================================================================#
class DAFilter(DAfilterBase):
    """
    This creates an object that that will take care of the whole DA process.
    To initialize an object:
       ``>> Filter_Object = DAFilter(Config_dict)``

    while ``Config_dict`` is a configuration dictionary obtained by reading the file ``da_solver.inp``.
    This can be obtained as follows:
       ``>> Config_dict=_utility.read_filter_configurations([file_name = <file_name='da_solver.inp'>])``


    Filter_Object is the data assimilation object with the necessary filter configurations set.
    """

    def __init__(self, DA_Filter_Configs):
        """
        Initialize the data assimilation object and set the necessary configurations.
        """

        #print filter_configs
        self._DA_Filter_Name               = DA_Filter_Configs['filter_name']
        self._Nens                         = DA_Filter_Configs['ensemble_size']
        self._T_Initial                    = DA_Filter_Configs['initial_time']
        self._T_Final                      = DA_Filter_Configs['final_time']
        self._Cycle_Length                 = DA_Filter_Configs['cycle_length']
        self._Obs_Operator_Type            = DA_Filter_Configs['observation_operator_type']
        self._Obs_Spacing_Type             = DA_Filter_Configs['observation_spacing_type']
        self._Obs_Steps_per_Filter_Steps   = DA_Filter_Configs['observation_steps_per_filter_steps'] # per steps between T_Initial and T_Final. has nothing to do with the original model configurations.
        self._Obs_Chance_per_Filter_Steps  = DA_Filter_Configs['observation_chance_per_filter_steps']
        self._Screen_Output                = DA_Filter_Configs['screen_output']
        self._File_Output                  = DA_Filter_Configs['file_output']
        self._File_Output_Means_Only       = DA_Filter_Configs['file_output_means_only']
        self._Background_Error_Cov_Method  = DA_Filter_Configs['background_errors_covariance_method']
        self._Observation_Error_Cov_Method = DA_Filter_Configs['observation_errors_covariance_method']

        #initialize the monitor collection
        self._DAMonitor={}

        if DA_Filter_Configs.has_key('model_error_steps_per_model_steps'):
            #
            self._Add_Model_Errors = True
            self._ModelError_Steps_per_Model_Steps  = DA_Filter_Configs['model_error_steps_per_model_steps'] # This has to to do with the original model time step

            #
            if DA_Filter_Configs.has_key('model_errors_covariance_method'):
                self._Model_Error_Cov_Method  = DA_Filter_Configs['model_errors_covariance_method']
            else:
                self._Model_Error_Cov_Method  = 'diagonal'
            #
            if DA_Filter_Configs.has_key('model_noise_type'):
                self._Model_Noise_Type  = DA_Filter_Configs['model_noise_type']
            else:
                self._Model_Noise_Type  = 'gaussian'
            #
            if DA_Filter_Configs.has_key('model_noise_level'):
                self._Model_Noise_Level  = DA_Filter_Configs['model_noise_level']
            #
            else:
                self._Model_Noise_Level  = np.infty

        else:
            self._Add_Model_Errors = False
            self._ModelError_Steps_per_Model_Steps  = np.infty




        if DA_Filter_Configs.has_key('background_noise_type'):
            self._Background_Noise_Type  = DA_Filter_Configs['background_noise_type']
        else:
            self._Background_Noise_Type  = 'gaussian'

        if DA_Filter_Configs.has_key('observation_noise_type'):
            self._Observation_Noise_Type       = DA_Filter_Configs['observation_noise_type']
        else:
            self._Observation_Noise_Type  = 'gaussian'


        if DA_Filter_Configs.has_key('observed_variables_jump'):
            self._Observed_Variables_Jump = DA_Filter_Configs['observed_variables_jump']
        else:
            self._Observed_Variables_Jump = 1






        if DA_Filter_Configs.has_key('background_noise_level'):
            self._Background_Noise_Level  = DA_Filter_Configs['background_noise_level']
        else:
            self._Background_Noise_Level  = np.infty

        if DA_Filter_Configs.has_key('observation_noise_level'):
            self._Observation_Noise_Level  = DA_Filter_Configs['observation_noise_level']
        else:
            self._Observation_Noise_Level  = np.infty


        if (DA_Filter_Configs['screen_output']):
            self._Screen_Output_Iter      = DA_Filter_Configs['screen_output_iter']
        if (DA_Filter_Configs['file_output']):
            self._File_Output_Iter        = DA_Filter_Configs['file_output_iter']

        if DA_Filter_Configs.has_key('decorrelate'):
            self._Decorrelate             = DA_Filter_Configs['decorrelate']
        else:
            self._Decorrelate             = False

        if(self._Decorrelate):
            self._Decorrelation_Radius    = DA_Filter_Configs['decorrelation_radius']
        else:
            self._Decorrelation_Radius    = np.infty


        if DA_Filter_Configs.has_key('use_sparse_packages'):
            self._Use_Sparse         = DA_Filter_Configs['use_sparse_packages']
        else:
            self._Use_Sparse         = False

        if DA_Filter_Configs.has_key('periodic_decorrelation'):
            self._Periodic_BC        = DA_Filter_Configs['periodic_decorrelation']
        else:
            self._Periodic_BC        = False

        if DA_Filter_Configs.has_key('read_decorrelation_from_file'):
            self._Use_Existing_Decorr        = DA_Filter_Configs['read_decorrelation_from_file']
        else:
            self._Use_Existing_Decorr        = False

        if DA_Filter_Configs.has_key('update_B_factor'):
            self._Update_B_Factor  = DA_Filter_Configs['update_B_factor']
        else:
            self._Update_B_Factor  = 0.0

        if DA_Filter_Configs.has_key('inflation_factor'):
            self._Inflation_Factor  = DA_Filter_Configs['inflation_factor']
        else:
            self._Inflation_Factor  = 1.0


        if self._DA_Filter_Name.lower()=='bootstrap_pf':
            self._DAMonitor['ess']=[]
            if DA_Filter_Configs.has_key('particle_filter_resampling_strategy'):
                self._PF_Resampling  = DA_Filter_Configs['particle_filter_resampling_strategy']
            else:
                self._PF_Resampling  = 'stratified'

        if self._DA_Filter_Name.lower()=='pf':
            self._DAMonitor['ess']=[]
            if DA_Filter_Configs.has_key('particle_filter_resampling_strategy'):
                self._PF_Resampling  = DA_Filter_Configs['particle_filter_resampling_strategy']
            else:
                self._PF_Resampling  = 'stratified'

        if DA_Filter_Configs.has_key('linear_system_solver'):
            self._Linear_System_Solver  = DA_Filter_Configs['linear_system_solver']
        else:
            if self._Use_Sparse:
                self._Linear_System_Solver  = 'splu'
            else:
                self._Linear_System_Solver  = 'lu'


        if DA_Filter_Configs.has_key('prior_distribution'):
            self._Prior_Distribution = DA_Filter_Configs['prior_distribution']
        else:
            self._Prior_Distribution  = 'gaussian'



        if self._DA_Filter_Name.lower()=='hmc':
            self._DAMonitor['ess']=[]
            self._HMC_Options = {'ensemble_size':self._Nens}
            self._HMC_Options.update({'update_B_factor':self._Update_B_Factor})
            self._HMC_Options.update({'inflation_factor':self._Inflation_Factor})
            self._HMC_Options.update({'use_sparse':self._Use_Sparse})
            self._HMC_Options.update({'linear_system_solver':self._Linear_System_Solver})


            #print DA_Filter_Configs
            if DA_Filter_Configs.has_key('hamiltonian_integrator'):
                self._Hamiltonian_Integrator  = DA_Filter_Configs['hamiltonian_integrator']
            else:
                self._Hamiltonian_Integrator  = 'verlet'
            self._HMC_Options.update({'Hamiltonian_Integrator':self._Hamiltonian_Integrator})

            if DA_Filter_Configs.has_key('hamiltonian_sampling_strategy'):
                self._Hamiltonian_Sampling_Strategy = DA_Filter_Configs['hamiltonian_sampling_strategy']
            else:
                self._Hamiltonian_Sampling_Strategy   = 'vanilla_hmc'
            self._HMC_Options.update({'sampling_strategy':self._Hamiltonian_Sampling_Strategy})

            if DA_Filter_Configs.has_key('hamiltonian_step_size'):
                self._Hamiltonian_Step_Size  = DA_Filter_Configs['hamiltonian_step_size']
            else:
                self._Hamiltonian_Step_Size   = 0.01
            self._HMC_Options.update({'Hamiltonian_step_size':self._Hamiltonian_Step_Size})

            if DA_Filter_Configs.has_key('hamiltonian_number_of_steps'):
                self._Hamiltonian_Number_of_Steps  = DA_Filter_Configs['hamiltonian_number_of_steps']
            else:
                self._Hamiltonian_Number_of_Steps   = 10
            self._HMC_Options.update({'Hamiltonian_number_of_steps':self._Hamiltonian_Number_of_Steps})


            if DA_Filter_Configs.has_key('hamiltonian_burn_in_steps'):
                self._Hamiltonian_BurnIn_Steps  = DA_Filter_Configs['hamiltonian_burn_in_steps']
            else:
                self._Hamiltonian_BurnIn_Steps  = 100
            self._HMC_Options.update({'Hamiltonian_BurnIn_Steps':self._Hamiltonian_BurnIn_Steps})

            if DA_Filter_Configs.has_key('hamiltonian_mixing_steps'):
                self._Hamiltonian_Mixing_Steps  = DA_Filter_Configs['hamiltonian_mixing_steps']
            else:
                self._Hamiltonian_Mixing_Steps  = 20
            self._HMC_Options.update({'Hamiltonian_Mixing_Steps':self._Hamiltonian_Mixing_Steps})

            if DA_Filter_Configs.has_key('mass_matrix_strategy'):
                self._Mass_Matrix_Strategy  = DA_Filter_Configs['mass_matrix_strategy']
            else:
                self._Mass_Matrix_Strategy  = 'prior_precisions'
            self._HMC_Options.update({'mass_matrix_strategy':self._Mass_Matrix_Strategy})

            if DA_Filter_Configs.has_key('mass_matrix_scale_factor'):
                self._Mass_Matrix_Scale_Factor  = DA_Filter_Configs['mass_matrix_scale_factor']
            else:
                self._Mass_Matrix_Scale_Factor  = 1
            self._HMC_Options.update({'mass_matrix_scale_factor':self._Mass_Matrix_Scale_Factor})

            self._HMC_Options.update({'prior_distribution':self._Prior_Distribution})

        else:
            self._HMC_Options = None



        self._CWD = os.getcwd()
        if not(self._CWD.endswith('/')):
            self._CWD = self._CWD+'/'




    def __str__(self):
        return 'DA filtre: '+self._DA_Filter_Name




    def set_DA_filter(self, Model_Object , set_uncertainty_parameters=True , create_initial_ensemble=True):
        """
        .. py:function:: set_DA_filter(Model_Object , [set_uncertainty_parameters=True] , [create_initial_ensemble=True])

        Sets up the data assimilation model/filter based on information in the configurations file.
        a shallow copy of the model object is passes just to extract necessary information


        :param object Model_Object: An object created from the class ``ModelBase_HyPar``.

        :param bool set_uncertainty_parameters: if, True (default) uncertainty parameters such as model
                                                error covariance matrix, background error covariance matrix and
                                                observation error covariance matrix, based on settings in the
                                                filter configuration file ``da_solver.inp``.

        :param bool create_initial_ensemble: if True (default) an intial ensemble is created and saved to disk by
                                             calling ``Model_Object.generate_initialForecast()``.
        """

        #<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        # adding information to the model configurations obtained from from filter configurations
        #>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        Model_Object._observation_Errors_Covariance_Method = self._Observation_Error_Cov_Method
        Model_Object._observation_Operator_Type  = self._Obs_Operator_Type
        Model_Object._observation_Noise_Level    = self._Background_Noise_Level
        Model_Object._observation_Noise_Type     = self._Observation_Noise_Type

        Model_Object._observed_Variables_Jump    = self._Observed_Variables_Jump

        Model_Object._background_Errors_Covariance_Method = self._Background_Error_Cov_Method
        Model_Object._background_Noise_Level              = self._Background_Noise_Level
        Model_Object._background_Noise_Type               = self._Background_Noise_Type
        Model_Object._decorrelate                         = self._Decorrelate

        Model_Object._Decorrelation_Radius   = self._Decorrelation_Radius
        Model_Object._Use_Sparse             = self._Use_Sparse
        Model_Object._Periodic_BC            = self._Periodic_BC
        Model_Object._Filter_CWD             = self._CWD
        Model_Object._Use_Existing_Decorr    = self._Use_Existing_Decorr

        if self._Add_Model_Errors:
            Model_Object._add_Model_Errors = True
            #
            Model_Object._model_Errors_Covariance_Method    = self._Model_Error_Cov_Method
            Model_Object._model_Noise_Type                  = self._Model_Noise_Type
            Model_Object._model_Noise_Level                 = self._Model_Noise_Level
            Model_Object._modelError_Steps_per_Model_Steps  = self._ModelError_Steps_per_Model_Steps
            #
        else:
            Model_Object._add_Model_Errors = False
            Model_Object._modelError_Steps_per_Model_Steps = np.infty
        #<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<




        if  ((self._T_Final - self._T_Initial) < Model_Object._dt) or (self._Cycle_Length <Model_Object._dt):
            raise ValueError("\n\t Correct time bounds 'da_solver.inp'. Model._dt=%f " %Model_Object._dt)

        # model Timespan based on assimilation cycles. (inter-steps are considered for model propagation for stability)
        self._Filter_Time_Span     = _utility.linspace(self._T_Initial , self._T_Final , self._Cycle_Length )
        self._Filter_Num_of_Steps  = np.size(self._Filter_Time_Span)-1


        #^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        # Set observations temporal grid
        #^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        # Observations Timespan based on observation strategy
        #
        if (self._Obs_Spacing_Type.lower() == 'fixed'):
            obs_dt_per_filter_dt   = self._Obs_Steps_per_Filter_Steps
            num_steps             = self._Filter_Num_of_Steps

            self._Observation_Ind_Loc      = np.arange(obs_dt_per_filter_dt, num_steps+1 , obs_dt_per_filter_dt)
            self._Observation_Time_Span    = self._Filter_Time_Span[self._Observation_Ind_Loc]
            self._Num_of_Obs_Points        = np.size(self._Observation_Time_Span)

        elif(self._Obs_Spacing_Type.lower() == 'random'):
            observation_prob         = self._Obs_Chance_per_Filter_Steps
            rndObsChance             = np.random.rand(np.size(self._Filter_Time_Span))
            obsFlags                          = rndObsChance <= observation_prob
            if obsFlags[0]:
                obsFlags[0] = False # make sure we skip initial time.
            self._Observation_Ind_Loc      = np.squeeze(np.where(obsFlags))
            self._Observation_Time_Span    = self._Filter_Time_Span[obsFlags]
            self._Num_of_Obs_Points        = np.size(self._Observation_Time_Span)
        else:
            raise NotImplementedError("The observation strategey[ "+self._Obs_Spacing_Type+" ] is not implemented! Check HyPar_DAFiltering")


        if set_uncertainty_parameters:
            _utility.prepare_uncertainty_parameters(Model_Object)

        if create_initial_ensemble:
            #
            print "Generating forecast initial condition..."
            frcst_initial_condition  = Model_Object.generate_initialForecast()
            # Create initial ensemble
            print "Generating an Initial Ensemble..."
            self.create_Initil_Ensemble(Model_Object , frcst_initial_condition )




    def DA_Monitor_Add(self,monitor):

        pass

    def _DA_monitor_pre_filter(self):

        pass

    def _DA_monitor_post_filter(self):

        pass



    def DA_filtering_cycle(self, Model_Object , Xf , X_k1 ,ObsVec , nens , update_B=True ):
        """
        .. py:function:: DA_filtering_cycle(Model_Object, Xf, X_k1, ObsVec, nens , [update_B=True])
        given observation vector, background state/ensemble, and statistics at a specific time instant,
        this function produces the analysis ensemble for the given cycle.

        :param object Model_Object: An object created from the class ``ModelBase_HyPar``.

        :param numpy.ndarray Xf: the forecast ensemble array.
                                   (Nvar x Nens): each memeber is stored as a column.

        :param numpy.ndarray X_k1: previous analysis ensemble (at the previous assimilation time instance).
                                   (Nvar x Nens): each memeber is stored as a column.

        :param numpy.ndarray ObsVec: observation vector at the current time instance.

        :return: Xa, Filter_Statistics.

                     - Xa is the analysis ensemble.
                       (Nvar x Nens): each memeber is stored as a column.

                     - Filter_Statistics is a dictionary containing filter statistics such as:

                                 - Effective sample size,
                                 - Acceptance probabilities,
                                 - Number of objective function evaluations,
                                 - Number of gradient evaluations,
                                 - etc.
                       Statistics differ from filter to filter.

        """
        #nobsVars = ObsVec.size
        nvar , nens = Xf.shape
        if (nens!=self._Nens):
            print "ATTENTION: Ensemble size has changed! check HyPar_DAFiltering.DA_filtering_cycle."

        #
        self._DA_monitor_pre_filter()

        if (self._DA_Filter_Name.lower()  == "EnKF".lower()         ):
            Xa, Filter_Statistics = DA_Stoch_EnKF_Cycle(Model_Object , Xf ,ObsVec ,nens , update_B)

        elif(self._DA_Filter_Name.lower() == "SqrEnKF".lower()      ):
            Xa, Filter_Statistics = DA_Sqrt_EnKF_Cycle(Model_Object , Xf ,ObsVec ,nens  , update_B)

        elif(self._DA_Filter_Name.lower() == "BootStrap_PF".lower() ):
            Xa, Filter_Statistics = DA_BootstrapPF_Cycle(self, Model_Object , Xf ,ObsVec ,nens ,resampling_strategy=self._PF_Resampling)

        elif(self._DA_Filter_Name.lower() == "PF".lower() ):
            Xa, Filter_Statistics = DA_PF_Cycle(self, Model_Object , Xf, X_k1  ,ObsVec ,nens ,resampling_strategy=self._PF_Resampling)

        elif(self._DA_Filter_Name.lower() == "HMC".lower()          ):
            Xa, Filter_Statistics = DA_Hamiltonian_MC_Cycle(Model_Object, Xf ,ObsVec ,HMC_Options=self._HMC_Options, screen_output =True )

        elif(self._DA_Filter_Name.lower() == "Tempered_Sampling".lower()          ):
            Xa, Filter_Statistics = DA_Tempered_Sampling_Cycle(Model_Object, Xf ,ObsVec ,HMC_Options=self._HMC_Options, screen_output =True )

        else:
            raise NotImplementedError

        self._DA_monitor_post_filter()



        return Xa , Filter_Statistics






    #<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    # PS: For now, I assumed that observations are available at all filter time
    #     which is not the same as model time points, however, we will make it
    #     even more flexible for testing.
    #     This needs to be modified to accomodate self._Observation_Time_Span
    #     Next step before working with higher dimensional models...
    #<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    def DA_filtering_process(self, Model_Object , in_Ref_IC=None , keep_all_in_memory=False ):
        """
        .. py:function:: DA_filtering_process(Model_Object, [in_Ref_IC=None])

           Sweep the filter timespan and do assimilation at points where observations are available as requested in
           the filter configurations file ``da_solver.inp``.

        :param object Model_Object: An object created from the class ``ModelBase_HyPar``.

        :param numpy.ndarray in_Ref_IC: reference initial condition vector.
                                        This is needed for RMSE evaluation and to generate observation if it is synthetic.
                                        It is created by default as ``Model_Objec._Ref_IC`` when the model object is
                                        initalized.
                                        For example, in `HyPar <http://debog.github.io/codes/hypar.html>`_ models this
                                        is called by compiling ``aux/init.c/C file in the model directory.

        :param boolean keep_all_in_memory: If true, all states (Forecast, Analysis, Observations, Reference, etc)
                                           at all observation time instances are kept in memory during the whole
                                           process. It is highly recommended to avoid setting it to True unless the
                                           problem is too small (e.g. Lorenz*). Memeory Errors should be expected
                                           if used with large-models!

        :return: This function does not return something as a variable. It saves the assimilation results to the
                  assimilation results directory which by default is ``results_directory=assimilation_results``

        """
        # TODO: Ahmed remove unnecessary variables and reduce memory usage as possible

        # Make sure proper strategy is followed to save states at all observation time instances
        if not keep_all_in_memory and not self._File_Output:
            raise UserWarning("Either states must be kept in memory, or written to / retrieved from files!\n Setting to defaults (Using Files IO)...")
            self._File_Output = True


        if in_Ref_IC is None:
            try:
                in_Ref_IC = Model_Object._Ref_IC
            except:
                raise ValueError("Cannot find a proper reference initial condition. Either you create it in the model or pass it here as a second argument!")

        #
        print "\nStarting the sequential assimilation process...\n"
        # ensemble members files' prefix and directory location (keep the defaults) ;
        # This is a temporary folder to be cleaned out after all cycles are done.
        file_prefix = 'ensemble_mem'
        out_dir = 'ensemble_out/'



        if self._File_Output:
            # Create a folder: "assimilation_results" to hold all analysis results according to filter configurations
            results_directory = "assimilation_results/"
            if os.path.isdir(results_directory):
                shutil.rmtree(results_directory)
            os.mkdir(results_directory)


        #ndims = Model_Object._ndims
        #nvars = Model_Object._nvars
        #dims  = Model_Object._size

        state_size = Model_Object._state_size
        obsVec_size = Model_Object._obs_vec_size
        nens = self._Nens


        if keep_all_in_memory:
            # Keep everything in memory if requested:
            self._referenceTrajectory      = np.empty((state_size , self._Filter_Num_of_Steps+1))
            self._NOassimilationTrajectory = np.empty((state_size , self._Filter_Num_of_Steps+1))
            self._forecastTrajectory       = np.empty((state_size , self._Filter_Num_of_Steps+1))
            self._analysisMeans            = np.empty((state_size , self._Filter_Num_of_Steps+1))
            self._Observations             = np.empty((obsVec_size , self._Num_of_Obs_Points))

            self._analysis_RMSE       = np.zeros(self._Filter_Num_of_Steps+1)
            self._forecast_RMSE       = np.zeros(self._Filter_Num_of_Steps+1)
            self._NOassimilation_RMSE = np.zeros(self._Filter_Num_of_Steps+1)



        Ref_state = in_Ref_IC.copy()
        #print 'Ref_state\n----------------' , Ref_state
        if keep_all_in_memory:
            self._referenceTrajectory[:,0] = Ref_state

            # get the intial ensemb from files
            Analysis_Ensemble = _utility.read_analysis_ensemble_states(Model_Object=Model_Object, \
                                                                       nens=nens ,                \
                                                                       file_prefix = file_prefix, \
                                                                       out_dir = out_dir          \
                                                                       )
            analysis_mean = np.mean(Analysis_Ensemble , 1)

        else:
            # get only the requested statistic of the analysis ensemble (ensemble average by default)
            analysis_mean = _utility.evaluate_statistic_of_ensemble_states(statistic='average',        \
                                                                           Model_Object=Model_Object,  \
                                                                           nens=nens,                  \
                                                                           file_prefix = file_prefix , \
                                                                           out_dir = out_dir           \
                                                                           )

        NOassimilation_state = analysis_mean.copy()


        # Root-Mean-Squared Errors
        initial_analysis_RMSE       = linalg.norm(Ref_state - analysis_mean)/np.sqrt(state_size)
        initial_forecast_RMSE       = initial_analysis_RMSE
        initial_NOassimilation_RMSE = initial_analysis_RMSE

        # generate initial observation for consistency only (not used)
        ObsVec = Model_Object.obs_oper_prod_Vec(analysis_mean)

        if keep_all_in_memory:
            self._forecastTrajectory[:,0] = analysis_mean
            self._NOassimilationTrajectory[:,0] = NOassimilation_state
            self._analysisMeans[:,0]      = analysis_mean[:]
            self._Observations[:,0]       = ObsVec

            self._analysis_RMSE[0]        = initial_analysis_RMSE
            self._forecast_RMSE[0]        = initial_forecast_RMSE
            self._NOassimilation_RMSE[0]  = initial_NOassimilation_RMSE





        # for the initial time, the analysis ensemble is taken as the initial ensemble
        #it is already saved in the "ensemble_out" folder


        # copy initial data for outputting
        if self._File_Output:
            if self._File_Output_Means_Only:
                initial_forecast_mean = analysis_mean.copy()
                initial_analysis_mean = initial_forecast_mean
            else:
                initial_forecast_ensemble = Analysis_Ensemble.copy()
                initial_analysis_ensemble = initial_forecast_ensemble # no need for a deep copy
            initial_Ref_state  = Ref_state.copy()
            initial_NOassimilation_state = NOassimilation_state.copy()



        assim_ind = 1 # index of the next assimilation point
        #
        for time_ind in xrange(1,self._Filter_Num_of_Steps+1):

            #^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            #  Get time bounds of the current time-subinterval
            #^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            #<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            # Here we can modify the time sub-intervals and add model errors to
            # forecasted states at any point in the time (sub)intervals by calling
            # Model_Object.generate_modelNoise_Vector() to generate a vector of
            # model errors.
            # e.g.
            #   >> time_<sub>_bounds = [ from> , >to ]
            #   >> fcst_state = Model_Object.step_forward( in_state , time_<sub>_bounds )
            #   >> err_Vec = generate_modelNoise_Vector()
            #   >> fcst_state += err_Vec
            #     Or better in the step forward
            #>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            t_init  = self._Filter_Time_Span[time_ind-1]
            t_final = self._Filter_Time_Span[time_ind ]
            time_bounds = np.array([t_init , t_final])

            # Better to keep that in memory. (We can write it to the temporary directory with different file_prefix!)
            Forecast_Ensemble = np.zeros((state_size,nens))
            #^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            # propagate the reference initial condition
            #  (to generate synthetic observations)
            #^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

            Ref_state = Model_Object.step_forward( Ref_state , time_bounds  )
            #print 'time: ',time_ind , ' Ref_state',Ref_state[0:10],'>+<',Ref_state[-10:-1]
            if np.isnan(Ref_state).any():
                raise ValueError("Forward model returned NAN from the previous Ref_state!")
            if keep_all_in_memory:
                self._referenceTrajectory[:,time_ind] = Ref_state
            #print '<<<<<<<<<<<<<<<<<<<<<<<New Ref_state>>>>>>>>>>>>>>>>>>>>>>\n',Ref_state


            # Propagate the no-assimilation (free) state:
            NOassimilation_state = Model_Object.step_forward( NOassimilation_state , time_bounds  )
            #print 'time: ',time_ind , ' NOassimilation_state',NOassimilation_state[0:10],'>+<',NOassimilation_state[-10:-1]
            if np.isnan(NOassimilation_state).any():
                raise ValueError("Forward model returned NAN from the previous NOassimilation_state!")
                # may be a better idea is to replace this error with something that allows assimilation to proceed
            if keep_all_in_memory:
                self._NOassimilationTrajectory[:,time_ind] = NOassimilation_state
            #



            #^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            # Forecast step from t_init to f_final.
            # propagate the whole analysis ensemble to the
            # current time point:
            #^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            # We can avoid keeping the analysis ensemble in memory later.
            Analysis_Ensemble = _utility.read_analysis_ensemble_states(Model_Object=Model_Object , nens=nens )

            for i_ens in xrange(self._Nens):
                print '\r Forecasting Ensemble number: %d '%(i_ens+1),
                sys.stdout.flush()

                in_state = Analysis_Ensemble[:,i_ens]

                # if keep_all_in_memory:
                #     in_state = Analysis_Ensemble[:,i_ens]
                # else:
                #     #get ensemble member from the ouput folder
                #     filename = file_prefix+'_'+str(i_ens)+'.dat'
                #     in_state = _utility.read_model_txt_ouput( file_name = filename , model_tmp_dir = out_dir)
                #     #print 'ens: ',i_ens, ' Previous Analysis_state',in_state[0:10],'>+<',in_state[-10:-1]

                #write the ensemble as binary initial condition to the model
                fcst_state = Model_Object.step_forward( in_state , time_bounds )
                if np.isnan(fcst_state).any():
                    raise ValueError("Forward model returned NAN after prpagating ensemble member numer["+str(i_ens)+"]!")

                Forecast_Ensemble[:,i_ens] = fcst_state.copy()
                #print 'ens: ',i_ens, ' Forecasted to ',fcst_state[0:10],'>+<',fcst_state[-10:-1]

            print '\r Forecast Step Finished... >> Analysis...  ',
            sys.stdout.flush()
            print ('\r'+' '.rjust(50)+'\r'),


            forecast_mean = np.mean(Forecast_Ensemble , 1)
            if keep_all_in_memory:
                self._forecastTrajectory[:,time_ind] = forecast_mean.copy()
            #print 'time: ',time_ind , ' forecast_mean',forecast_mean[0:10],'>+<',forecast_mean[-10:-1]
            #return


            if assim_ind<=self._Num_of_Obs_Points:
                # Analysis step : put condition on the observation index...
                if (self._Filter_Time_Span[time_ind] == self._Observation_Time_Span[assim_ind-1]):
                    # Use new reference solution to Get the observation vector (synthetic or actual)
                    #print 'YES assimilation'
                    ObsVec= Model_Object.generate_observation(Ref_state)
                    if keep_all_in_memory:
                        self._Observations[:,assim_ind-1] = ObsVec.copy()
                    #print 'time: ',time_ind , ' ObsVec',ObsVec[0:10],'>+<',ObsVec[-10:-1]
                    #
                    Assim_Cycle = True
                    Analysis_Ensemble, Filter_Statistics = self.DA_filtering_cycle(Model_Object , Forecast_Ensemble , Analysis_Ensemble, ObsVec ,self._Nens)

                    if assim_ind<self._Num_of_Obs_Points:
                        assim_ind+=1

                else:
                    Assim_Cycle = False
                    #print 'No assimilation'
                    Analysis_Ensemble = Forecast_Ensemble.copy()

            analysis_mean = np.mean(Analysis_Ensemble , 1)
            if keep_all_in_memory:
                self._analysisMeans[:,time_ind] = analysis_mean.copy()


            #print 'state_size', state_size
            #print 'Ref_state',Ref_state
            #print 'analysis_mean',analysis_mean
            #print 'forecast_mean',forecast_mean
            #print 'max(|Ref_state - analysis_mean|)', np.max(np.abs(Ref_state - analysis_mean))
            #print 'max(|Ref_state - forecast_mean|)', np.max(np.abs(Ref_state - forecast_mean))


            # calculating errors
            analysis_RMSE       = linalg.norm(Ref_state - analysis_mean)/np.sqrt(state_size)
            forecast_RMSE       = linalg.norm(Ref_state - forecast_mean)/np.sqrt(state_size)
            NOassimilation_RMSE = linalg.norm(Ref_state - NOassimilation_state)/np.sqrt(state_size)

            if keep_all_in_memory:
                self._analysis_RMSE[time_ind]        = analysis_RMSE
                self._forecast_RMSE[time_ind]        = forecast_RMSE
                self._NOassimilation_RMSE[time_ind]  = NOassimilation_RMSE



            #save analysis ensemble states for the next cycle.
            _utility.write_analysis_ensemble_states(Analysis_Ensemble ,Model_Object , nens, file_prefix = 'ensemble_mem',out_dir = 'ensemble_out/')



            #
            # output results to files if requested
            # TODO: use numpy write functionf for faster access when possible
            #-------------------------------------------------------------------
            if self._File_Output:
                #---------------------------------------------------------------
                if (((time_ind-1)%self._File_Output_Iter)==0):
                    #
                    #---------------------------------------------
                    if(self._File_Output_Means_Only):

                        # write the analysis means
                        with open(results_directory+"Analysis_Means.dat", "a") as fptr:
                            # Append ensemble mean only in the same file
                            # write the analysis ensemble mean of the current assimilation cycle.
                            #each ensemble will be printed on a row. empty lines will be printed between members.
                            # each row starts with index then the assimilation time then the analysis ensemble mean

                            # write initial data if this is the initial time
                            if time_ind == self._File_Output_Iter:
                                fptr.write("%4d "%0 + "%5.3f "%self._Filter_Time_Span[0] + "   ")
                                for ind in xrange(state_size):
                                    fptr.write("%+1.16e"%initial_analysis_mean[ind]+"  ")
                                fptr.write("\n")

                            # now, current time index data are written
                            fptr.write("%4d "%time_ind + "%5.3f "%self._Filter_Time_Span[time_ind] + "   ")
                            for ind in xrange(state_size):
                                fptr.write("%+1.16e"%analysis_mean[ind]+"  ")
                            fptr.write("\n")

                        # write the forecast states
                        with open(results_directory+"Forecast_Means.dat", "a") as fptr:
                            # same as in case of analysis means.
                            if time_ind == self._File_Output_Iter:
                                fptr.write("%4d "%0 + "%5.3f "%self._Filter_Time_Span[0] + "   ")
                                for ind in xrange(state_size):
                                    fptr.write("%+1.16e"%initial_forecast_mean[ind]+"  ")

                                fptr.write("\n")
                            # now, current time index data are written
                            fptr.write("%4d "%time_ind + "%5.3f "%self._Filter_Time_Span[time_ind] + "   ")
                            for ind in xrange(state_size):
                                fptr.write("%+1.16e"%forecast_mean[ind]+"  ")
                            fptr.write("\n")

                    else:
                        # Add the all ensemble members in new files.
                        #(one large file for each ensemble is probably not a good idea!
                        # may be we can add an option for that add the new analysis ensemble.
                        # each ensemble file will end with "_assimilation point index"
                        # << Forecast ensemble >>
                        if time_ind == self._File_Output_Iter:
                            filename = "Forecast_Ensemble_"+str(0)+".dat"
                            with open(results_directory+filename, "w") as fptr:
                                # same as in case of analysis means.
                                #---------------
                                for ind_i in xrange(state_size):
                                    for ind_j in xrange(nens):
                                        fptr.write("%+1.16e"%initial_forecast_ensemble[ind_i , ind_j] + "  ")
                                fptr.write("\n")

                                # << Analysis ensemble >>
                                filename = "Analysis_Ensemble_"+str(0)+".dat"
                                with open(results_directory+filename, "w") as fptr:
                                    # same as in case of analysis means.
                                    #---------------
                                    for ind in xrange(state_size):
                                        for ind_j in xrange(nens):
                                            fptr.write("%+1.16e"%initial_analysis_ensemble[ind_i , ind_j] + "  ")
                                    fptr.write("\n")

                        #
                        filename = "Forecast_Ensemble_"+str(time_ind)+".dat"
                        with open(results_directory+filename, "w") as fptr:
                            # same as in case of analysis means.
                            #---------------
                            for ind_i in xrange(state_size):
                                for ind_j in xrange(nens):
                                    fptr.write("%+1.16e"%Forecast_Ensemble[ind_i , ind_j] + "  ")
                            fptr.write("\n")

                            # << Analysis ensemble >>
                            filename = "Analysis_Ensemble_"+str(time_ind)+".dat"
                            with open(results_directory+filename, "w") as fptr:
                                # same as in case of analysis means.
                                #---------------
                                for ind in xrange(state_size):
                                    for ind_j in xrange(nens):
                                        fptr.write("%+1.16e"%Analysis_Ensemble[ind_i , ind_j] + "  ")
                                fptr.write("\n")




                    #---------------------------------------------
                    # save Filtering Timespan to a separate file.
                    filename="Filtering_Times.dat"
                    with open(results_directory+filename, "a") as fptr:
                        if time_ind == self._File_Output_Iter:
                            # write index and assimilation intial time points in two columns.
                            fptr.write("%4d \t"%0 +"%5.3f "%self._Filter_Time_Span[0] + "\n")
                        fptr.write("%4d \t"%time_ind +"%5.3f "%self._Filter_Time_Span[time_ind] + "\n")

                    # save reference states, NO-Assimilation states, and observations.
                    # write the reference states
                    with open(results_directory+"Reference_States.dat", "a") as fptr:
                        # writes down the reference states at the requested assimilation instances.
                        # Empty lines will be printed between consecutive states.
                        # Each row starts with index then the assimilation time then the reference state
                        if time_ind == self._File_Output_Iter:
                            fptr.write("%4d "%0 + "%5.3f "%self._Filter_Time_Span[0] + "   ")
                            for ind in xrange(state_size):
                                fptr.write("%+1.16e"%initial_Ref_state[ind]+"  ")
                            fptr.write("\n")
                        #
                        fptr.write("%4d "%time_ind + "%5.3f "%self._Filter_Time_Span[time_ind] + "   ")
                        for ind in xrange(state_size):
                            fptr.write("%+1.16e"%Ref_state[ind]+"  ")
                        fptr.write("\n")

                    # write the NO-Assimilation states,
                    with open(results_directory+"NOassimilation_States.dat", "a") as fptr:
                        # writes down the no-assimilation states at the requested assimilation instances.
                        # Empty lines will be printed between consecutive states.
                        # Each row starts with index then the assimilation time then the reference state
                        if time_ind == self._File_Output_Iter:
                            fptr.write("%4d "%0 + "%5.3f "%self._Filter_Time_Span[0] + "   ")
                            for ind in xrange(state_size):
                                fptr.write("%+1.16e"%initial_NOassimilation_state[ind]+"  ")
                            fptr.write("\n")
                        #
                        fptr.write("%4d "%time_ind + "%5.3f "%self._Filter_Time_Span[time_ind] + "   ")
                        for ind in xrange(state_size):
                            fptr.write("%+1.16e"%NOassimilation_state[ind]+"  ")
                        fptr.write("\n")

                    # write the observation vector (If there is one)
                    if Assim_Cycle:
                        with open(results_directory+"Observations.dat", "a") as fptr:
                            # writes down the reference states at the requested assimilation instances.
                            # Empty lines will be printed between consecutive states.
                            # Each row starts with index then the assimilation time then the observation vector
                            fptr.write("%4d "%(assim_ind-1) + "%5.3f "%self._Filter_Time_Span[time_ind] + "   ")
                            for ind in xrange(obsVec_size):
                                fptr.write("%+1.16e"%ObsVec[ind]+"  ")
                            fptr.write("\n")

                        # save observations' times to a separate file. -> redundancy.
                        filename="Observation_Times.dat"
                        with open(results_directory+filename, "a") as fptr:
                            # write index and assimilation time points in two columns.
                            fptr.write("%4d \t"%(assim_ind-1) +"%5.3f "%self._Filter_Time_Span[time_ind] + "\n")

                    #---------------------------------------------

                    # write errors:
                    filename="RMSE.dat"
                    with open(results_directory+filename, "a") as fptr:
                        # first column:   index
                        # second column:  times
                        # third column:   NO Assimilation RMSE
                        # fourth column: forecast RMSE
                        # fifth column: analysis RMSE
                        if (time_ind==self._File_Output_Iter):
                            fptr.write("%4d \t"%0 +"%5.3f "%self._Filter_Time_Span[0] +"%1.16e "%initial_NOassimilation_RMSE+"%1.16e "%initial_forecast_RMSE +"%1.16e "%initial_analysis_RMSE + "\n")
                        #
                        fptr.write("%4d \t"%time_ind +"%5.3f "%self._Filter_Time_Span[time_ind] +"%1.16e "%NOassimilation_RMSE+"%1.16e "%forecast_RMSE +"%1.16e "%analysis_RMSE + "\n")
                        #


                    # write Filter_Statistics:
                    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                    #These are mainly accociated to HMC samplers
                    try:
                        # check for HMC options
                        Func_Evals = Filter_Statistics['Func_Evals']
                        Grad_Evals = Filter_Statistics['Grad_Evals']
                        #print Func_Evals
                        #print Grad_Evals
                        # write Func_Evals (number of function evaluations), and Grad_Evals (Number of gradient evaluations):
                        filename="Num_Func_Grad_Evals.dat"
                        with open(results_directory+filename, "a") as fptr:
                            # first column:   index
                            # second column:  time
                            # third column:   Number of function evaluations
                            #  fourth column: Number of gradient evaluations
                            fptr.write("%4d \t"%time_ind +"%5.3f "%self._Filter_Time_Span[time_ind] +"%6d "%Func_Evals+"%6d "%Grad_Evals + "\n")
                        #
                    except:
                        pass

                    #>>>>>>>
                    try:
                        # check for HMC options
                        P_Acceptance = np.asarray(Filter_Statistics['P_Acceptance'])
                        with open(results_directory+"Proposals_Acceptace_Prob.dat", "a") as fptr:
                            # writes down the probabilities of acceptance of proposed states
                            # Empty lines will be printed between consecutive probabilities in consequtive cycles.
                            # Each row starts with index then the acceptance probabilities
                            fptr.write("%4d "%time_ind + "%5.3f "%self._Filter_Time_Span[time_ind] + "   ")
                            for ind in xrange(P_Acceptance.size):
                                fptr.write("%+1.16e"%P_Acceptance[ind]+"  ")
                            fptr.write("\n")
                        #
                    except:
                        pass

                    #>>>>>>>
                    try:
                        # check for HMC options
                        Kernel_NegLog_Vals = np.asarray(Filter_Statistics['Kernel_NegLog_Vals'])
                        with open(results_directory+"Kernel_NegativeLog_Vals.dat", "a") as fptr:
                            # writes down the negative log values of the Kernel of the target PDF
                            # Empty lines will be printed between consecutive negative log values of the Kernel of the target PDF.
                            fptr.write("%4d "%time_ind + "%5.3f "%self._Filter_Time_Span[time_ind] + "   ")
                            for ind in xrange(Kernel_NegLog_Vals.size):
                                fptr.write("%+1.16e"%Kernel_NegLog_Vals[ind]+"  ")
                            fptr.write("\n")
                        #
                    except:
                        pass

                    #>>>>>>>
                    try:
                        # check for HMC options
                        Accepted_Proposals = np.asarray(Filter_Statistics['Accepted_Proposals'])
                        with open(results_directory+"Accepted_Proposals_Flags.dat", "a") as fptr:
                            # writes down the probabilities of acceptance of proposed states
                            # Empty lines will be printed between consecutive probabilities in consequtive cycles.
                            # Each row starts with index then the acceptance probabilities
                            fptr.write("%4d "%time_ind + "%5.3f "%self._Filter_Time_Span[time_ind] + "   ")
                            for ind in xrange(Accepted_Proposals.size):
                                fptr.write("%2d"%np.int(Accepted_Proposals[ind])+"  ")
                            fptr.write("\n")
                        #
                    except:
                        pass

                    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                    #>>>>>>>
                    # start adding other possible statistics from other filters similarly


                    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


            else:
                # No file output is requested. do nothing.
                pass

                #---------------------------------------------------------------
                #

            #-------------------------------------------------------------------

            #
            # Screen ouput:
            if self._Screen_Output:
                #
                if time_ind ==1:
                    divider= "======================================================================================="
                    print divider+"\n"+"Assimilation Results".rjust(52)+"\n"+divider
                    print("i".rjust(5)+"  Time".ljust(10)+"NO-Assim RMSE".rjust(19)+"Forecast RMSE".rjust(24)+"Analysis RMSE".rjust(24))
                    print divider

                if (((time_ind-1)%self._Screen_Output_Iter)==0):
                    #print("%4d"%time_ind +' '+ "%6.4f"%self._Filter_Time_Span[time_ind] +'  Forecast RMSE=' +str(Forecast_RMSE[time_ind])+'  Analysis RMSE=' +str(Analysis_RMSE[time_ind])+'  Anal2ObsRMSE=' +str(Anal2Obs_RMSE[time_ind]) )
                    if (self._Filter_Time_Span[time_ind] in self._Observation_Time_Span):
                        print("%5d"%time_ind +'  '+"%5.3f"%self._Filter_Time_Span[time_ind] +'\t  ' +"%1.12e"%NOassimilation_RMSE +'\t  ' +"%1.12e"%forecast_RMSE +'\t  ' +"%1.12e"%analysis_RMSE +" <--(assim)")
                    else:
                        print("%5d"%time_ind +'  '+"%5.3f"%self._Filter_Time_Span[time_ind] +'\t  ' +"%1.12e"%NOassimilation_RMSE +'\t  ' +"%1.12e"%forecast_RMSE +'\t  ' +"%1.12e"%analysis_RMSE )
                #
                if time_ind ==self._Filter_Num_of_Steps:
                    print divider
                #
            else:
                # No file output is requested. do nothing.
                pass


        if self._File_Output:
            # write-down the (updated) model configurations in case they are needed to ease the plotting process. Filter Configurations are added to the same file as well.
            # I think it is a better idea to just copy both configurations files as is to the output directory. We have configurations readers already for both
            _utility.write_model_and_filter_configs(target_directory=results_directory)


        try:
            if self._File_Output:
                # write-down the grid information to be available for plotting without reloading the model';
                file_name="Grids.dat"
                for grid_ind in range(Model_Object._ndims):
                    grid = Model_Object._Grids_dict[str(grid_ind)]
                    with open(results_directory+file_name, "a") as fptr:
                        for grd in grid:
                            fptr.write("%1.16e  "%grd)
                        fptr.write("\n")
        except:
            # This will likely be thrown for 0D models only . No need to raise an error...
            raise UserWarning(" Grid information could not be written to files!")

        try:
            if self._File_Output:
                # write-down observed variables indices. This is valid mostly for linear observation operators
                file_name="Observed_Vars.dat"
                np.savetxt(results_directory+file_name , Model_Object._vars_observed)
        except:
            #
            raise UserWarning("Observed variables information could not be written to files!")



        #Clean-up the temporary ensemble folder
        if os.path.isdir(out_dir):
            shutil.rmtree(out_dir)
        else:
            raise IOError("Something off happened! 'ensemble_out' went missing!!!")



        print "\n Assimilation process is complete...\n"

        #Tspan = self._Filter_Time_Span[:]
        #return Tspan , Forecast_RMSE , Analysis_RMSE #, Anal2Obs_RMSE



    def create_Initil_Ensemble(self , Model_Object , forecast ):
        """
        .. py:function:: create_Initil_Ensemble(Model_Object, forecast)

           Create and save an initial ensemble to be used as analysis at the initial time point.
           The current scheme is so simple. More advanced schemes will be considered!

        :param object Model_Object: An object created from the class ``ModelBase_HyPar``.

        :param numpy.ndarray forecast: background/forecast initial condition vector. This is obtained by perturbing
                                       the initial reference solution by adding gaussian noise with covariance matrix B.

        :return: This function does not return something as a variable. It saves the initial ensemble in the
                 temporary directory ``out_dir = 'ensemble_out``

        """

        #nvars     = Model_Object._nvars
        statesize = Model_Object._state_size
        #ndims     = Model_Object._ndims
        #dims      =  Model_Object._size


        nens = self._Nens

        # create an initial ensemble:
        # this is idealized for gaussian prior. more complications can be added later
        #
        Initial_Ensemble = np.empty((statesize , nens) )
        for iens in xrange(nens):
            randVec = _utility.randn_Vec(statesize)
            if self._Use_Sparse:
                Initial_Ensemble[:,iens] = forecast + np.array(Model_Object._sqrtB0.dot(randVec)) # now just for testing...
            else:
                Initial_Ensemble[:,iens] = forecast + np.dot(Model_Object._sqrtB0 , randVec) # now just for testing...

            #print 'Initial_Ensemble[:,',iens,']', Initial_Ensemble[:,iens]


        print "Writing the initial ensemble to files..."
        #save analysis ensemble states for the next forecast.
        _utility.write_analysis_ensemble_states(Initial_Ensemble ,Model_Object , nens, file_prefix = 'ensemble_mem',out_dir = 'ensemble_out/')






#================================================================================================================================#
