class DAfilterBase:
    """
    Abstract class for DAPack(Py-DA*) data assimilation filters. Check :mod:`HyPar_DAFiltering` for example.
    """

    def __init__(self):
        raise NotImplementedError

    def set_DA_filter(self, Model_Object):
        raise NotImplementedError

    def DA_filtering_cycle(self, Xf , Yobs):
        raise NotImplementedError
