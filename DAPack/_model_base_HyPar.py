#!/usr/bin/env

from setup_dapack import Hypar_bin, MPI_Run_Command

import os
import sys

import numpy as np
from scipy import linalg
import scipy.sparse as sparse

import matplotlib.pyplot as plt

import collections
import h5py

#
from HyPar_DAFiltering import DAFilter
from _model_base import ModelsBase

import _utility

# ================================================================================================================================#
# A class implementing the three-variables Lorenz model.
# ================================================================================================================================#
class ModelBase_HyPar(ModelsBase):
    """
    A Base class implementing the necessary functions of the one dimensional models implemented in HyPar pack.
    My goal is to have a unified class for all hypar models!
    For now, Let's just use this class for all one-D models as a base class. Functionality can be overridden o
    after inheritance
    """
    #-----------------------------------------------------------------------------------------------------------------------#
    # Constructor of the model object:
    #-----------------------------------------------------------------------------------------------------------------------#
    def __init__(self):
        #
        self._setup = False

    #
    #-----------------------------------------------------------------------------------------------------------------------#




    #-----------------------------------------------------------------------------------------------------------------------#
    # Set model information based on the configurations passed
    #-----------------------------------------------------------------------------------------------------------------------#
    # Input:
    #     > Model_Configs         : a dictionary containing the necessary model variables required to initialize the model.
    #-----------------------------------------------------------------------------------------------------------------------#
    def model_setup(self, Model_Configs):
        #
        """
        Set up model parameters: spatial dimensionality and initial time stepping parameters.
        number of iterations may change between consequtive assimilation cycles but step size
        should be kept fixed to guarantee stability.
        :param config Model_Configs: dictionary containing model configurations read from the file **solver.inp** and
        modified based on the filter configurations in **da_solver.inp**.
        """
        #
        #-----------------------------------------------------------------------
        self._ndims = Model_Configs['ndims']
        self._nvars = Model_Configs['nvars']
        self._size = np.asarray(Model_Configs['size'])
        self._grid_size = np.prod(self._size)
        #
        self._state_size = self._nvars * self._grid_size
        #
        if Model_Configs.has_key('iproc'):
            self._iproc = Model_Configs['iproc']

        if Model_Configs.has_key('model'):
            self._model_name = Model_Configs['model']
        else:
            self._model_name = 'UnProvided'
        #

        if Model_Configs.has_key('n_iter'):
            self._n_iter = Model_Configs['n_iter']

        if Model_Configs.has_key('dt'):
            self._dt = Model_Configs['dt']

        self._tmp_Model_Dir = 'temp_model'
        self._initial_Conditoin_File = '_txt_initial_condition.dat'

        #-----------------------------------------------------------------------
        #


    #-----------------------------------------------------------------------------------------------------------------------#
    #
    #-----------------------------------------------------------------------------------------------------------------------#
    def set_grids(self):
        """
        #.. py:function:: set_grids()

        Set spatial grid of the model. This is done by reading initial condition file if HyPar model is used.
        It also sets the reference initial condition of the model.
        """
        #
        self._Grids_dict = collections.OrderedDict()

        tmpdir = self._tmp_Model_Dir

        #write initial condition as text file.
        new_fields = {'ip_file_type': 'ascii'}  # ascii <-- text
        _utility.overwrite_original_model_configurations(new_fields)
        ## remove obsolete initial condition file if exists:
        #if (os.path.isfile(tmpdir+'/initial.inp')):
        #    os.remove(tmpdir+'/initial.inp')
        #
        dest = os.getcwd()

        _utility.check_initial_condition(dest, file_name=self._initial_Conditoin_File, tmp_dir=tmpdir)

        self._Ref_IC = _utility.read_model_txt_initial_condition(file_name=self._initial_Conditoin_File,
                                                                 model_tmp_dir=self._tmp_Model_Dir)
        self._Ref_IC = self._Ref_IC + 5  # For testing purposes...

        fptr = open(tmpdir + '/' + self._initial_Conditoin_File, 'r')
        file_contents = fptr.readlines()
        fptr.close()
        #
        self._grids = np.array([])
        self._spacings = np.zeros(self._ndims)
        # read the grid of the first dimension (space separated pattern).
        for i_dim in range(self._ndims):
            dim_str = file_contents[i_dim].split()
            dim_grid = np.array([float(x) for x in dim_str])
            self._grids = np.concatenate([self._grids, dim_grid])
            self._spacings[i_dim] = dim_grid[1] - dim_grid[0]  # delta in each direction
            self._Grids_dict.update({str(i_dim): dim_grid})
        #

        # restore initial condition file type to default 'binary'
        new_fields = {'ip_file_type': 'binary'}
        _utility.overwrite_original_model_configurations(new_fields)

    #
    #-----------------------------------------------------------------------------------------------------------------------#





    #-----------------------------------------------------------------------------------------------------------------------#
    #                    time stepping using forward model. This is designed for HyPar models.
    #-----------------------------------------------------------------------------------------------------------------------#
    def step_forward(self, init_state, time_bounds, rewrite_as_binary_ip=True, file_name='op.dat',
                     model_tmp_dir='temp_model'):
        """
        #.. py:function:: step_forward(init_state, time_bounds, [rewrite_as_binary_ip=True] ,[file_name='op.dat'] ,[model_tmp_dir='temp_model'] )

        Propagate the model forwad in time form ``time_bounds[0]`` to ``time_bounds[1]``.

        :param array init_state: model state (presumably) at time ``time_bounds[0].

        :param array time_bounds: np.array (or a list) with two entries representing the initial and the final time to propagate the model.
                                          Forward propagation is assumed for this implementation.

        :param str file_name: file name of the model output. This is useful since we want to deal
        a lot with black-box models.

        :param str model_tmp_dir: temporary directory where the model output is saved under the name ``file_name``.
                                  This is useful since we want to deal a lot with black-box models.

        :returns: ``out_state`` a **numpy.ndarray** vector containing the final state after propagation in time from
                  ``time_bounds[0]`` to ``time_bounds[1]``, i.e. ``out_state`` is the model state at time
                  ``time_bounds[1]``.

        """
        if (time_bounds.size != 2):
            print "time_bounds must include both initial and final time"
            raise ValueError
        else:

            # calculate the number of iterations based on dt in the original configurations of the model
            dt = self._dt
            tspan_length = time_bounds[1] - time_bounds[0]
            n_iter = int(np.ceil(tspan_length / dt))
            #
            #
            if not (self._add_Model_Errors) or \
                    np.isinf(self._modelError_Steps_per_Model_Steps) or \
                            self._modelError_Steps_per_Model_Steps > n_iter \
                    :
                #
                # propagate directly without model errors                #
                # overwrite number of iterations in solver.inp
                #
                new_configs = {'n_iter': n_iter, \
                               'screen_op_iter': 1, \
                               'file_op_iter': n_iter, \
                               'ip_file_type': 'binary' \
                    }
                _utility.overwrite_original_model_configurations(new_configs)

                #print 'init_state',init_state
                #print self._grids
                _utility.write_output_as_binary_initial_codition(self._grids, init_state, self._ndims, self._nvars)

                # integrate the model forward using the initial conditoin file. 'initial.inp'
                dest = os.getcwd()
                processes = self._iproc.prod()
                os.chdir(model_tmp_dir)
                # add Petsc-
                cmd = MPI_Run_Command + ' -np ' + str(processes) + ' ' + Hypar_bin + ' >model_screen_out.dat'
                os.system(cmd)
                #
                os.chdir(dest)
                #

                # read the grid and state information from output file 'op.dat'
                out_state = _utility.read_model_txt_ouput(file_name='op.dat', model_tmp_dir='temp_model')
                #print 'out_state' , out_state



            else:
                # propagate with model errors in intermediate steps with model errors added

                # overwrite number of iterations in solver.inp
                # calculate the number of iterations based on dt in the original configurations of the model


                #print 'n_iter ', n_iter
                #print 'self._modelError_Steps_per_Model_Steps', self._modelError_Steps_per_Model_Steps

                add_Q_iters = n_iter / self._modelError_Steps_per_Model_Steps
                #print 'add_Q_iters ',add_Q_iters
                more_iters = n_iter % self._modelError_Steps_per_Model_Steps
                #print 'more_iters ',more_iters

                for iter in range(add_Q_iters):
                    #
                    # overwrite number of iterations in solver.inp
                    #
                    new_configs = {'n_iter': self._modelError_Steps_per_Model_Steps, \
                                   'screen_op_iter': 1, \
                                   'file_op_iter': self._modelError_Steps_per_Model_Steps, \
                                   'ip_file_type': 'binary' \
                        }
                    _utility.overwrite_original_model_configurations(new_configs)

                    #print 'init_state',init_state
                    #print self._grids
                    _utility.write_output_as_binary_initial_codition(self._grids, init_state, self._ndims, self._nvars)

                    # integrate the model forward using the initial conditoin file. 'initial.inp'
                    dest = os.getcwd()
                    processes = self._iproc.prod()
                    os.chdir(model_tmp_dir)
                    # add Petsc-
                    cmd = MPI_Run_Command + ' -np ' + str(processes) + ' ' + Hypar_bin + ' >model_screen_out.dat'
                    os.system(cmd)
                    #
                    os.chdir(dest)
                    #

                    # read the grid and state information from output file 'op.dat'
                    out_state = _utility.read_model_txt_ouput(file_name='op.dat', model_tmp_dir='temp_model')
                    #print 'out_state' , out_state

                    # add model errors:
                    model_errors_Vec = self.generate_modelNoise_Vector()
                    init_state = out_state + model_errors_Vec


                    #if iter==add_Q_iters-1:
                    #    out_state = init_state
                out_state = init_state

                if more_iters > 0:
                    #
                    init_state = out_state
                    #
                    new_configs = {'n_iter': more_iters, \
                                   'screen_op_iter': 1, \
                                   'file_op_iter': more_iters, \
                                   'ip_file_type': 'binary' \
                        }
                    _utility.overwrite_original_model_configurations(new_configs)

                    #print 'init_state',init_state
                    #print self._grids
                    _utility.write_output_as_binary_initial_codition(self._grids, init_state, self._ndims, self._nvars)

                    # integrate the model forward using the initial conditoin file. 'initial.inp'
                    dest = os.getcwd()
                    processes = self._iproc.prod()
                    os.chdir(model_tmp_dir)
                    # add Petsc-
                    cmd = MPI_Run_Command + ' -np ' + str(processes) + ' ' + Hypar_bin + ' >model_screen_out.dat'
                    os.system(cmd)
                    #
                    os.chdir(dest)
                    #

                    # read the grid and state information from output file 'op.dat'
                    out_state = _utility.read_model_txt_ouput(file_name='op.dat', model_tmp_dir='temp_model')
                    #print 'out_state' , out_state



            # write output as initial condition for the next cycle if requestd
            if (rewrite_as_binary_ip):
                _utility.write_output_as_binary_initial_codition(self._grids, out_state, self._ndims, self._nvars)

        return out_state

    #
    #-----------------------------------------------------------------------------------------------------------------------#




    #-----------------------------------------------------------------------------------------------------------------------#
    # time stepping using model TLM,
    #-----------------------------------------------------------------------------------------------------------------------#
    def step_dforward_du(self, dU):
        """
        This simulates the effect of the tangent linear of the model. Propagate a vector of perturbations. This is for future use.
        :param Vec dU: a vector of perturbations
        :return:
        """
        raise NotImplementedError

    #
    #-----------------------------------------------------------------------------------------------------------------------#





    #-----------------------------------------------------------------------------------------------------------------------#
    # this constructs the observation operator, and it's effect on a vector
    #-----------------------------------------------------------------------------------------------------------------------#
    #

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    # Construct H
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    def construct_obs_oper(self, varObs_jump=None):
        """
        #.. py:function:: construct_obs_oper([varObs_jump=None])

        :param int varObs_jump: This defines the observation frequency in space (among grid points).
                                1 means all prognostic variables are observed at all gridpoints.
                                2 means all prognostic variables are observed at each second gridpoints, etc.
                                If not given, a default of 1 is used.
        :return: An np.ndarray ``self._H`` is attached to the model and saved as an observation operator.
        """
        #
        if varObs_jump is None:
            varObs_jump = self._observed_Variables_Jump

        #self._vars_observed = np.arange(self._state_size) # observe all
        #varObs_jump = 3 # each (varObs_jump) variable is observed.: 1-> observe all, 2-> each second, etc...
        if self._state_size == self._nvars:
            # for 0 and 1 dimensional models with a single variable
            self._no_of_obsPoints = self._state_size / varObs_jump
            self._vars_observed = np.zeros(self._no_of_obsPoints,
                                           dtype=np.int)  # observe all variables at selective grid-points
            self._vars_observed = np.arange(0, self._state_size, varObs_jump)
        else:
            self._no_of_obsPoints = self._grid_size / varObs_jump  # number of observation grid-points
            self._vars_observed = np.zeros(self._no_of_obsPoints * self._nvars,
                                           dtype=np.int)  # observe all variables at selective grid-points
            for gr_ind in xrange(self._no_of_obsPoints):
                self._vars_observed[self._nvars * gr_ind:self._nvars * (gr_ind + 1)] = [obs_ind for obs_ind in xrange(
                    self._nvars * gr_ind * varObs_jump, self._nvars * (gr_ind * varObs_jump + 1))]

        # we need to add observational grid here to plot contour of observations if requested


        #self._vars_observed contains indexes of all prognostic variables at only observed grid points
        self._no_of_observed_vars = np.size(self._vars_observed)


        if self._observation_Operator_Type.lower() == 'linear':
            self._obs_vec_size = np.size(self._vars_observed)

            # short-fat matrix
            vals = np.ones(self._obs_vec_size)
            i_inds = np.arange(self._obs_vec_size)
            j_inds = self._vars_observed
            #
            if self._Use_Sparse:
                # vals = np.ones(self._obs_vec_size)
                # i_inds = np.arange(self._obs_vec_size)
                # j_inds = self._vars_observed
                self._H = sparse.csr_matrix(( vals, (i_inds, j_inds)),
                                            shape=(self._obs_vec_size, self._state_size))
            else:
                self._H = np.zeros((self._obs_vec_size, self._state_size))
                # for obs_ind in range(self._obs_vec_size):
                #     self._H[obs_ind, self._vars_observed[obs_ind]] = 1
                self._H[i_inds,j_inds] = vals


        elif self._observation_Operator_Type.lower() == 'quadratic':
            raise NotImplementedError("Quadratic observation operator is yet to be implemented")
        else:
            raise NotImplementedError("Check the type of observation operator in the filter configurations file")

    #
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<



    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    # Evaluate the effect of the observation operator (H) on a state vector 'U' by multiplication
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    def obs_oper_prod_Vec(self, U):
        """
        #.. py:function:: obs_oper_prod_Vec(U)

        Evaluate the effect of the observation operator ``self._H`` on a state vector ``U``.
        For linear observation operator, this is a matrix-vector product.

        :param Vec U: state vector.
        :return: theoritical observation resulting as the effect of ``self._H`` on ``u``, i,e, ``self._H(U)``.
        """

        if self._observation_Operator_Type.lower() == 'linear':
            if self._Use_Sparse:
                return (self._H).dot(U)
            else:
                return np.dot(self._H, U)


        elif self._observation_Operator_Type.lower() == 'quadratic':
            raise NotImplementedError("Quadratic observation operator is yet to be implemented")
        else:
            raise NotImplementedError("Check the type of observation operator in the filter configurations file")

    #
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    #  Evaluate the tangent linear model of the observation operator at vector 'U'
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    def obs_doper_prod_dU(self, U):
        """
        #.. py:function:: obs_doper_prod_dU(U)

        Evaluate the tangent linear (TLM) forward observation operator at a state vector ``U``.

        :param Vec U: state vector.
        :return: TLM of the observation operator evaluated at ``U``.
        """

        if self._observation_Operator_Type.lower() == 'linear':
            #same here
            if self._Use_Sparse:
                return self._H
            else:
                return self._H


        elif self._observation_Operator_Type.lower() == 'quadratic':
            raise NotImplementedError("Quadratic observation operator is yet to be implemented")
        else:
            raise NotImplementedError("Check the type of observation operator in the filter configurations file")

    #
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<




    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    # given a state U, generate a synthetic observation for testing
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    def generate_observation(self, U):
        """
        #.. py:function:: generate_observation(U)

        Given a state ``U``, generate a synthetic observation for testing

        :param Vec U: given a *true state* ``U``, Synthetic observation is created by adding observation noise.
                             Observation noise assumed to be additive (no necessarily) but can in general be non-Gaussian.

        :return array: ``Y`` the theoritical observation vector.
        """
        if self._observation_Noise_Type.lower() in ['gaussian', 'white']:
            #
            # if self._observation_Operator_Type.lower() == 'linear':
            #     theo_Obs = self.obs_oper_prod_Vec(U)
            #     if self._Use_Sparse:
            #         Y = theo_Obs + (self._sqrtR).dot(_utility.randn_Vec(theo_Obs.size))
            #     else:
            #         Y = theo_Obs + np.dot(self._sqrtR, _utility.randn_Vec(theo_Obs.size))
            # elif self._observation_Operator_Type.lower() == 'quadratic':
            #     raise NotImplementedError("Quadratic observation operator is yet to be implemented")
            # else:
            #     raise NotImplementedError("Check the type of observation operator in the filter configurations file")

            theo_Obs = self.obs_oper_prod_Vec(U)
            if self._Use_Sparse:
                Y = theo_Obs + (self._sqrtR).dot(_utility.randn_Vec(theo_Obs.size))
            else:
                Y = theo_Obs + np.dot(self._sqrtR, _utility.randn_Vec(theo_Obs.size))



        else:
            raise NotImplementedError("Check the type of observation noise type in the filter configurations file")

        return Y

    #
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<


    #
    #-----------------------------------------------------------------------------------------------------------------------#



    #
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    #
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    def construct_background_Error_Cov(self, Initial_State=None, Filter_Tspan=None, Num_Time_Points=12):
        """
        .. :py:function:: construct_background_Error_Cov([Initial_State=None], [Filter_Tspan=None], [Num_Time_Points=12] )

        Construct the full background error covariance matrix, and square root. The inverse is evaluated later by solving a linear system.

        :param Vec Initial_State: state vector used as initial condition for the current purpose.
        :param Vec Filter_Tspan:  states are evaluated at all time instances of this timespan. Standard deviations of background errors
                                  are evaluated as a pre-specified percentage of this average magnitude.
        :param int Num_Time_Points: if ``Filter_Tspan=None``, used to create a timespan consisting of this number of time instances.
        :return array: ``self._B``, ``self._invB`` are attached to the model the modeled background error covariance matrix
                             and it's square root given by Cholesky decomposition.
        """

        if Initial_State is None:
            Initial_State = self._Ref_IC

        # A timespan on which the Reference state will be forwarded in time to get the average magnitude of the state
        if Filter_Tspan is None:
            Filter_Tspan = np.asarray([i * self._dt for i in xrange(Num_Time_Points)])


        # The decorrelation matrix self._Decorr will be either created or loaded from file.
        #This can be enhanced when we decide to parallelize the code by stacking the read/write from the hdf5 file.
        if (self._decorrelate):
            self.construct_decorrelation_matrix(Read_fromFile=self._Use_Existing_Decorr)

        #
        strategy = self._background_Errors_Covariance_Method
        #
        if strategy == 'empirical':

            #==================================
            # turn-off model errors temporarily:
            if self._add_Model_Errors:
                reactiv_ME = True
                self._add_Model_Errors = False
            else:
                reactiv_ME = False
            #==================================

            #
            # accumulate states magnitudes:
            num_steps = np.size(Filter_Tspan) - 1
            out_state = self._Ref_IC[:]
            state_mag = np.abs(out_state)
            for time_ind in range(1, num_steps + 1):
                # Get time bounds of the current time-subinterval
                t_init = Filter_Tspan[time_ind - 1]
                t_final = Filter_Tspan[time_ind]
                time_bounds = np.array([t_init, t_final])
                #print 'out_state'   , out_state
                #print 'time_bounds' , time_bounds
                out_state = self.step_forward(out_state, time_bounds)
                state_mag += np.abs(out_state)

            # average state magnitude over the given timespan
            if num_steps > 0:
                state_mag = state_mag / num_steps



            # averaging over variables...
            vars_avg = np.zeros(self._nvars)
            for var_ind in range(self._nvars):
                vars_avg[var_ind] = np.mean(state_mag[var_ind::self._nvars])
                #vars_avg[vars_avg] = np.mean( state_mag[var_ind::self._nvars] )

            avrg_mags = np.tile(vars_avg, self._grid_size)
            prtrbs = avrg_mags * self._background_Noise_Level
            fac = 0.95
            #prtrbs = (1-fac) + fac * prtrbs
            prtrbs = fac * prtrbs
            #print prtrbs

            if self._decorrelate:
                self._B0 = self.decorr_dot_matrix(np.outer(prtrbs, prtrbs))  # this will return a sparse csr_matrix
                # avoid singularity by adding (1-fac)*I to the resulting matrix:
                corr_diag = self._B0.diagonal() + (1 - fac)
                if self._Use_Sparse:
                    self._B0.setdiag(corr_diag)
                else:
                    np.fill_diagonal(self._B0, corr_diag)

            elif self._Use_Sparse:  # just to avoid sparse._matrix x np.ndarray problems...
                self._B0 = np.outer(prtrbs, prtrbs)
                np.fill_diagonal(self._B0, np.diag(self._B0) + (1 - fac))
                self._B0 = sparse.csr_matrix(self._B0)
            else:
                self._B0 = np.outer(prtrbs, prtrbs)
                np.fill_diagonal(self._B0, np.diag(self._B0) + (1 - fac))


            # the square root of B0 is needed for generating background noise (for creating initial ensemble for example!)
            if self._Use_Sparse:
                self._sqrtB0 = sparse.csr_matrix(( linalg.cholesky(self._B0.toarray(), lower=True) ))
            else:
                self._sqrtB0 = linalg.cholesky(self._B0, lower=True)

            #
            #==================================
            if reactiv_ME:
                # turn-on model errors again if requested:
                self._add_Model_Errors = True
                reactiv_ME = False
                #==================================


        elif strategy == 'diagonal':
            #

            #==================================
            # turn-off model errors temporarily:
            if self._add_Model_Errors:
                reactiv_ME = True
                self._add_Model_Errors = False
            else:
                reactiv_ME = False
            #==================================

            #
            # accumulate states magnitudes:
            num_steps = np.size(Filter_Tspan) - 1
            state_mag = np.zeros(self._state_size)
            out_state = self._Ref_IC[:]
            for time_ind in range(1, num_steps + 1):
                # Get time bounds of the current time-subinterval
                t_init = Filter_Tspan[time_ind - 1]
                t_final = Filter_Tspan[time_ind]
                time_bounds = np.array([t_init, t_final])
                #print 'out_state'   , out_state
                #print 'time_bounds' , time_bounds
                out_state = self.step_forward(out_state, time_bounds)
                state_mag += np.abs(out_state)

            # average state magnitude over the given timespan
            state_mag = state_mag / num_steps

            # averaging over variables...
            vars_avg = np.zeros(self._nvars)
            for var_ind in range(self._nvars):
                vars_avg[var_ind] = np.mean(state_mag[var_ind::self._nvars])

            avrg_mags = np.tile(vars_avg, self._grid_size)
            prtrbs = avrg_mags * self._background_Noise_Level
            fac = 0.95
            prtrbs = (1 - fac) + fac * prtrbs

            if self._Use_Sparse:  # just to avoid sparse._matrix x np.ndarray problems...
                ind = np.arange(self._state_size)
                self._B0 = sparse.csr_matrix((prtrbs ** 2, (ind, ind)), shape=(self._state_size, self._state_size))
                self._sqrtB0 = sparse.csr_matrix((prtrbs, (ind, ind)), shape=(self._state_size, self._state_size))
            else:
                self._B0 = np.diag(np.sqrt(prtrbs ** 2))
                self._sqrtB0 = np.diag(np.sqrt(prtrbs))

            #
            #==================================
            if reactiv_ME:
                # turn-on model errors again if requested:
                self._add_Model_Errors = True
                reactiv_ME = False
                #==================================


        elif strategy == 'nmc':
            raise NotImplementedError("NMC method is yet to be implemented")
        else:
            raise NotImplementedError("B0 construction method is unidentified!")

    #
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    #
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    def background_Error_Cov_prod_U(self, U):
        """
        #.. py:function:: background_Error_Cov_prod_U(U)

        Evaluate the matrix-vector product of the background error covariance matrix by a vector ``U``.

        :param Vec U: state vector.
        :return: product of ``self._B`` by ``U``.
        """
        # not general as of yet! use for testing now
        if self._Use_Sparse:
            return np.array(self._B0.dot(U))
        else:
            return np.dot(self._B0, U)

    #
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    #
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    def background_Error_Cov_Inv_prod_U(self, U):
        """
        #.. py:function:: background_Error_Cov_Inv_prod_U(U)

        Evaluate the matrix-vector product of the inverse of the background error covariance matrix by a vector ``U``.

        :param Vec U: state vector.
        :return: product of ``self._invB`` by ``U``.
        """
        # not general as of yet! use for testing now
        if self._Use_Sparse:
            return np.array(self._invB0.dot(U))
        else:
            return np.dot(self._invB0, U)

    #
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<


    #
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    #
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    def construct_decorrelation_matrix(self, Read_fromFile=False, Decorr_fileName='Decorr.hdf5'):
        """
        ..py:function:: construct_decorrelation_matrix([Read_fromFile=False], [Decorr_fileName='Decorr.hdf5'])

        Either construct the full decorrelation matrix or load it from file. Later,
        we can enhance on it, e.g. by writing/reading from different file types, etc.

        :param bool Read_fromFile: flag to decide whether to construct the decorretlation matrix or load it from a file.
        :param Decorr_fileName: ``.hdf5`` file containing the decorrelation array.

        :return array: a localization array is constructed or loaded from file.

        :exception: IOError   if ``Read_fromFile=True`` and ``Decorr_fileName`` is not found
        :
        """
        if not (self._decorrelate):
            raise ValueError("This function should not be called! No decorrelation in da_solver.inp!!!")

        if np.isinf(self._Decorrelation_Radius):
            self._decorrelate = False
            print("Decorrelation radius is set to infinity. Decorrelation is skipped.")

        if Read_fromFile:
            # load the localization array from file. It is stored as full array. Convert to sparse if needed. (later...)
            # raise IOError if Decorr file does not exist in the directory: self._Filter_CWD.
            print 'Creating Decorr...',
            #
            #if not(os.path.isfile(Decorr_fileName)):
            #    raise IOError("Decorr File cannot be found in the working directory!")
            #else:

            try:
                fptr = h5py.File(Decorr_fileName, 'r')
                Decorr_dataset = fptr['Decorr']

                if self._Use_Sparse:
                    self._Decorr = sparse.csr_matrix(Decorr_dataset[:, :])
                else:
                    self._Decorr = Decorr_dataset[:, :]
                fptr.close()
                print 'Done...'

            except:
                raise IOError("Either Decorr file is missing or the required field is not found!")

        else:
            print 'Creating Decorr...',
            # create the localization array...
            if self._ndims == 1:
                self._Decorr = _utility.create_decorrelation_1d(StateVec_Size=self._state_size, \
                                                                Nvars=self._nvars, \
                                                                dx=self._spacings[0], \
                                                                Decorr_Radius=self._Decorrelation_Radius, \
                                                                Periodic_BC=self._Periodic_BC, \
                                                                Sparse=self._Use_Sparse, \
                                                                Sparse_type='csr_matrix' \
                    )
            elif self._ndims == 2:
                self._Decorr = _utility.create_decorrelation_2d(StateVec_Size=self._state_size, \
                                                                Nvars=self._nvars, \
                                                                NI=self._size[0], \
                                                                NJ=self._size[1], \
                                                                dx=self._spacings[0], \
                                                                dy=self._spacings[1], \
                                                                Decorr_Radius=self._Decorrelation_Radius, \
                                                                Periodic_BC=self._Periodic_BC, \
                                                                Sparse=self._Use_Sparse, \
                                                                Sparse_type='csr_matrix' \
                    )
            elif self._ndims == 3:
                raise NotImplementedError()
            elif self._ndims == 4:
                raise NotImplementedError()
            else:
                raise ValueError("Number of dimensions is [ " + self._ndims + " ] > 4?!")

            print 'Done...'

    #
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    #
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    def decorr_dot_matrix(self, mat):
        """
        ..py:function:: decorr_dot_matrix(mat)

        Evaluate pointwise multiplication of the decorrelation matrix by a given matrix.

        :return array: the result of pointwise multiplication of the decorrelation matrix by a matrix ``mat`` of the same size.
        """
        #get the decorrelation array(sparse)
        #print 'Decorrelating'
        if (self._decorrelate) and (self._Use_Sparse):
            #i_inds,j_inds = self._Decorr.nonzero()
            #loc_mat = sparse.csr_matrix(( (np.squeeze(np.asarray(self._Decorr[i_inds,j_inds])) * mat[i_inds,j_inds]),(i_inds,j_inds)),shape=(self._state_size,self._state_size))
            #loc_mat = sparse.csr_matrix(np.array(self._Decorr.multiply(mat)))
            #print 'mat', mat
            #loc_mat = self._Decorr.multiply(mat)
            loc_mat = sparse.csr_matrix(np.asarray(self._Decorr.multiply(mat)))
            #print 'Decorrelated.....'
            return loc_mat  # returned as csr_matrix... (not ndarray)

        elif (self._decorrelate):
            loc_arr = self._Decorr * mat
            return loc_arr

        else:
            raise ValueError("This should not be reached. no decorrelation required!")

    #
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<



    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    #
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    def generate_initialForecast(self):
        """
        Generate a forecast state at the initial time. Add random perturbations from the distribution of background errors
        Gaussian is the most common.
        """

        if self._background_Noise_Type.lower() in ['gaussian', 'white']:
            #
            if self._Use_Sparse:
                initial_forecast_state = self._Ref_IC + np.array(self._sqrtB0.dot(_utility.randn_Vec(self._state_size)))
            else:
                initial_forecast_state = self._Ref_IC + np.dot(self._sqrtB0, _utility.randn_Vec(self._state_size))
        else:
            raise NotImplementedError("Check the type of background noise type in the filter configurations file")

        return initial_forecast_state

    #
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<


    #
    #-----------------------------------------------------------------------------------------------------------------------#





    #-----------------------------------------------------------------------------------------------------------------------#
    # Desing the observation error covariance matrix/its effect on a vector/matrix
    #-----------------------------------------------------------------------------------------------------------------------#
    #

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    #
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    def construct_observation_Error_Cov(self, Filter_Tspan=None, Num_Time_Points=12):
        """
        .. :py:function:: construct_observation_Error_Cov([Initial_State=None], [Filter_Tspan=None], [Num_Time_Points=12] )

        Construct the full observation error covariance matrix, and square root.
        The inverse is evaluated later by solving a linear system unless it is diagonal.

        :param Vec Filter_Tspan:  states are evaluated at all time instances of this timespan. Standard deviations of observation errors
                                  are evaluated as a pre-specified percentage of this average magnitude. ``self._Ref_IC`` is used as initial condition.
        :param int Num_Time_Points: if ``Filter_Tspan=None``, used to create a timespan consisting of this number of time instances.
        :return array: ``self._B``, ``self._invB`` are attached to the model the observaiton error covariance matrix
                             and it's square root given by Cholesky decomposition.
        """

        # A timespan on which the Reference state will be forwarded in time to get the average magnitude of the state
        if Filter_Tspan is None:
            Filter_Tspan = np.asarray([i * self._dt for i in xrange(Num_Time_Points)])

        strategy = self._observation_Errors_Covariance_Method
        #
        if strategy.lower() in ['empirical', 'diagonal']:

            if (self._observation_Operator_Type.lower() == 'linear'):
                #
                # turn-off model errors temporarily:
                if self._add_Model_Errors:
                    reactiv_ME = True
                    self._add_Model_Errors = False
                else:
                    reactiv_ME = False
                #
                # accumulate states magnitudes:
                num_steps = np.size(Filter_Tspan) - 1
                #state_mag = np.zeros(self._state_size)
                out_state = self._Ref_IC[:]
                state_mag = np.abs(out_state)
                for time_ind in range(1, num_steps + 1):
                    # Get time bounds of the current time-subinterval
                    t_init = Filter_Tspan[time_ind - 1]
                    t_final = Filter_Tspan[time_ind]
                    time_bounds = np.array([t_init, t_final])
                    #print 'out_state'   , out_state
                    #print 'time_bounds' , time_bounds

                    out_state = self.step_forward(out_state, time_bounds)
                    state_mag += np.abs(out_state)

                if num_steps > 0:
                    state_mag = state_mag / num_steps

                # We need better R, we assume all grid-points are observed here
                #prtrb = self.obs_oper_prod_Vec(state_mag) * self._observation_Noise_Level

                # averaging over variables...
                vars_avg = np.zeros(self._nvars)
                for var_ind in range(self._nvars):
                    vars_avg[var_ind] = np.mean(state_mag[var_ind::self._nvars])

                # We need better R, we can assume all grid-points are observed here
                avrg_mags = np.tile(vars_avg, self._grid_size)
                # avrg_mags = np.tile(vars_avg, self._no_of_obsPoints)

                prtrb = self.obs_oper_prod_Vec(avrg_mags) * self._observation_Noise_Level

                fac = 0.95
                prtrb = (1 - fac) + fac * prtrb

                if self._Use_Sparse:
                    ind = [i for i in xrange(self._obs_vec_size)]

                    self._R = sparse.csr_matrix((prtrb ** 2, (ind, ind) ),
                                                shape=(self._obs_vec_size, self._obs_vec_size))
                    self._invR = sparse.csr_matrix((1 / (prtrb ** 2), (ind, ind) ),
                                                   shape=(self._obs_vec_size, self._obs_vec_size))
                    self._sqrtR = sparse.csr_matrix((prtrb, (ind, ind) ),
                                                    shape=(self._obs_vec_size, self._obs_vec_size))

                    self._detR = np.exp(np.log(
                        self._R.diagonal()).sum())  # determinant of R. Needed for Likelihood evaluation to avoid underflow issues caused by numpy
                    #self._detR = np.prod(self._R.diag())

                else:
                    self._R = np.diag(prtrb ** 2)
                    self._invR = np.diag(1 / (prtrb ** 2))
                    self._sqrtR = np.diag(prtrb)

                    self._detR = np.exp((np.log(np.diag(
                        self._R))).sum())  # determinant of R. Needed for Likelihood evaluation to avoid underflow issues caused by numpy
                    #self._detR = np.prod(np.diag(self._R))

                if reactiv_ME:
                    # turn-on model errors again if requested:
                    self._add_Model_Errors = True
                    reactiv_ME = False

            #
            elif (self._observation_Operator_Type == 'quadratic'):
                raise NotImplementedError("Quadratic observation operator is not yet implemented")
                #
            #
            else:
                raise NotImplementedError("Observation operator is not yet implemented")
                #

        else:
            raise NotImplementedError("R construction method is unidentified!")


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    #
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    def observation_Error_Cov_prod_Y(self, Y):
        """
        #.. py:function:: observation_Error_Cov_prod_Y(Y)

        Evaluate the effect of the observation error covariance matrix ``self._R`` on observation vector ``Y``.

        :param Vec Y: observation vector.
        :return: product of observation error covariance matrix ``self._R`` by observation ``Y``.
        """

        if self._Use_Sparse:
            return np.array(self._R.dot(Y))
        else:
            return np.dot(self._R, Y)


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    #
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    def observation_Error_Cov_Inv_prod_Y(self, Y):
        """
        #.. py:function:: observation_Error_Cov_Inv_prod_Y(Y)

        Evaluate the effect of the inverse of the observation error covariance matrix ``self._R`` on observation vector ``Y``.

        :param Vec Y: observation vector.
        :return: product of observation error covariance matrix ``self._invR`` by observation ``Y``.
        """
        if self._Use_Sparse:
            return np.array(self._invR.dot(Y))
        else:
            return np.dot(self._invR, Y)

    #
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<


    #
    #-----------------------------------------------------------------------------------------------------------------------#




    #-----------------------------------------------------------------------------------------------------------------------#
    # Desing the model error covariance matrix/its effect on a vector/matrix
    #-----------------------------------------------------------------------------------------------------------------------#
    #

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    #
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    def construct_model_Error_Cov(self, Filter_Tspan=None, Num_Time_Points=12):
        """
        Construct the model error covariance matrix <Model>._Q, its inverse <Model>_sqrtQ, and its inverse <Model>._invQ.
        The way it is constructed is similar to the background error covariance matrix except for the uncertainty level.
        """

        # A timespan on which the Reference state will be forwarded in time to get the average magnitude of the state
        if Filter_Tspan is None:
            Filter_Tspan = np.asarray([i * self._dt for i in xrange(Num_Time_Points)])

        #
        if not (self._add_Model_Errors):
            #raise ValueError("Model Errors are not required to be added!")
            self._add_Model_Errors = False  # this might happen if no model errors are requested but the user called this function in the driver. To avoid this, I will add this call and the similar one to a preprocessing step.
            return

        if np.isinf(self._model_Noise_Level):
            print "WARNING: Noise level is not found in the configuration file: 'da_solver.inp'. Turning model errors off!"
            self._add_Model_Errors = False
            return

        #
        strategy = self._model_Errors_Covariance_Method

        #
        if strategy.lower() in ['diagonal']:
            #
            # turn-off model errors temporarily:
            if self._add_Model_Errors:
                reactiv_ME = True
                self._add_Model_Errors = False
            else:
                reactiv_ME = False

            #
            # accumulate states magnitudes:
            num_steps = np.size(Filter_Tspan) - 1
            #state_mag = np.zeros(self._state_size)
            out_state = self._Ref_IC[:]
            state_mag = np.abs(out_state)
            for time_ind in range(1, num_steps + 1):
                # Get time bounds of the current time-subinterval
                t_init = Filter_Tspan[time_ind - 1]
                t_final = Filter_Tspan[time_ind]
                time_bounds = np.array([t_init, t_final])
                #print 'out_state'   , out_state
                #print 'time_bounds' , time_bounds

                out_state = self.step_forward(out_state, time_bounds)
                state_mag += np.abs(out_state)

            if num_steps > 0:
                state_mag = state_mag / num_steps
            #prtrb = self.obs_oper_prod_Vec(state_mag) * self._model_Noise_Level

            # averaging over variables...
            vars_avg = np.zeros(self._nvars)
            for var_ind in range(self._nvars):
                vars_avg[var_ind] = np.mean(state_mag[var_ind::self._nvars])

            avrg_mags = np.tile(vars_avg, self._grid_size)
            prtrb = self.obs_oper_prod_Vec(avrg_mags) * self._model_Noise_Level

            fac = 0.95
            prtrb = (1 - fac) + fac * prtrb

            if self._Use_Sparse:
                ind = [i for i in xrange(self._obs_vec_size)]
                self._Q = sparse.csr_matrix((prtrb ** 2, (ind, ind) ),
                                            shape=(self._obs_vec_size, self._obs_vec_size))
                self._invQ = sparse.csr_matrix((1 / (prtrb ** 2), (ind, ind) ),
                                               shape=(self._obs_vec_size, self._obs_vec_size))
                self._sqrtQ = sparse.csr_matrix((prtrb, (ind, ind) ),
                                                shape=(self._obs_vec_size, self._obs_vec_size))
            else:
                self._Q = np.diag(prtrb ** 2)
                self._invQ = np.diag(1 / (prtrb ** 2))
                self._sqrtQ = np.diag(prtrb)

            if reactiv_ME:
                # turn-on model errors again if requested:
                self._add_Model_Errors = True
                reactiv_ME = False
        #

        #
        elif strategy.lower() in ['empirical']:
            raise NotImplementedError("Model Error strategy: " + strategy + ", is not yet implemented!")

            #
        else:
            raise NotImplementedError("Model Error strategy: " + strategy + ", is not yet implemented!")
            #

    #
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<




    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    #
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    def model_Error_Cov_prod_U(self, U):
        """
        #.. py:function:: model_Error_Cov_prod_U( U)

        Evaluate the effect of the model error covariance matrix ``self._Q`` on a state vector ``U``.

        :param Vec U: state vector.
        :return: product of model error covariance matrix ``self._Q`` by state vector ``U``.
        """

        if self._Use_Sparse:
            return np.array(self._Q.dot(U))
        else:
            return np.dot(self._Q, U)

    #
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<



    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    #
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    def model_Error_Cov_Inv_prod_U(self, U):
        """
        #.. py:function:: model_Error_Cov_Inv_prod_U( U)

        Evaluate the effect of the inverse of the model error covariance matrix ``self._invQ`` on a state vector ``U``.
        This is found by solving a linear system

        :param Vec U: state vector.
        :return: product of inverse of model error covariance matrix ``self._invQ`` by state vector ``U``.
        """

        if self._Use_Sparse:
            return np.array(self._invQ.dot(U))
        else:
            return np.dot(self._invQ, U)

    #
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    #
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    def generate_modelNoise_Vector(self):
        """ generate a state vector perturbed by model noise.
        """
        if self._model_Noise_Type.lower() in ['gaussian', 'white']:
            #
            if self._Use_Sparse:
                model_noise_vector = np.array(self._sqrtQ.dot(_utility.randn_Vec(self._state_size)))
            else:
                model_noise_vector = np.dot(self._sqrtQ, _utility.randn_Vec(self._state_size))

        else:
            raise NotImplementedError("Check the type of model noise type in the filter configurations file")

        return model_noise_vector

        #
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<

        #
        #-----------------------------------------------------------------------------------------------------------------------#


#
#================================================================================================================================#
#                                                           Test Case
#================================================================================================================================#


if __name__ == "__main__":
    #
    pass

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
