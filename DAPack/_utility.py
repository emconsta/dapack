from setup_dapack import DA_Pack_RootPATH, CCompiler

import numpy as np
import os
import shutil
import sys
import ConfigParser
import collections
import scipy.sparse as sparse

from sklearn.mixture import GMM as GMM_Model
from sklearn.mixture import VBGMM as VBGMM_Model

import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.ticker import MultipleLocator
from mpl_toolkits.axes_grid1 import make_axes_locatable


import h5py

DA_Pack_PATH = DA_Pack_RootPATH+'DAPack/'

#===============================================================================
def randn_Vec(vec_size ):
    """
    Function generating a standard normal random vector with values truncated at -/+3
    """
    randVec = np.random.randn(vec_size)
    flg = randVec>3
    randVec[flg] = 3
    flg = randVec<-3
    randVec[flg] = -3

    return randVec
    #===========================================================================




#===============================================================================
def linspace(start, end , step , endpoint=True):
    """
    Function generating equally spaced values from start to (<=end) with step step
    for convenience only.
    np already has its own linspace but it requires number of steps.
    """
    if endpoint:
        num_steps = int(np.abs( np.ceil((end-start)/step) ))
        #Vec = np.zeros(num_steps+1)

        Vec = np.asarray([start+(i*step)  for i in xrange((num_steps+1))])
        if (step>0) and (end>start):
            #for i in xrange(num_steps+1):
            #    Vec[i] = start+(i*step)
            ex_locs = np.where(Vec>end)
            if np.size(ex_locs)>0:
                Vec = Vec[0:np.min(ex_locs)]
        elif (step<0) and (end<start):
            #for i in xrange(num_steps+1):
            #    Vec[i] = start + (i*step)
            ex_locs = np.where(Vec<end)
            if np.size(ex_locs)>0:
                Vec = Vec[0:np.min(ex_locs)]
        else:
            raise ValueError('Follow the rule: |start<=end & step>=0 | or |start>=end & step<=0 |')

    else:
        Vec = np.arange(start,end,step)

    return Vec
    #===========================================================================




#===============================================================================
def write_filter_configs_template(file_name = 'solver_template.ini'):
    """ write a filter configurations template file in the given path
        if path is not given, file is written in the cwd.
    """

    FilterConfigs = ConfigParser.ConfigParser()

    # create and set the filter configurations section
    FilterConfigs.add_section('SecFilterConfigs')
    FilterConfigs.set('SecFilterConfigs','filter_name','EnKF')# {EnKF/sqrEnKF/BootStrap_PF/HMC...}
    FilterConfigs.set('SecFilterConfigs','particle_filter_resampling_strategy','stratified') # {systematic,stratified,...}
    FilterConfigs.set('SecFilterConfigs','ensemble_size',30)
    FilterConfigs.set('SecFilterConfigs','initial_time',0)
    FilterConfigs.set('SecFilterConfigs','final_time',20)
    FilterConfigs.set('SecFilterConfigs','cycle_length',0.5) # length of each assimilation cycle

    FilterConfigs.set('SecFilterConfigs','observation_operator_type','linear')
    FilterConfigs.set('SecFilterConfigs','observation_noise_type','gaussian')
    FilterConfigs.set('SecFilterConfigs','observation_noise_level',0.05)
    FilterConfigs.set('SecFilterConfigs','observation_spacing_type','fixed') # {fixed/random}
    FilterConfigs.set('SecFilterConfigs','observation_steps_per_filter_steps',10)
    FilterConfigs.set('SecFilterConfigs','observation_chance_per_filter_steps',0.5)
    FilterConfigs.set('SecFilterConfigs','observed_variables_jump',1) # 1 observe all gridpoints. 2- every second, etc...

    FilterConfigs.set('SecFilterConfigs','screen_output','yes')
    FilterConfigs.set('SecFilterConfigs','screen_output_iter','1')
    FilterConfigs.set('SecFilterConfigs','file_output','yes')
    FilterConfigs.set('SecFilterConfigs','file_output_iter',1)
    FilterConfigs.set('SecFilterConfigs','file_output_means_only','yes')
    FilterConfigs.set('SecFilterConfigs','decorrelate','yes')
    FilterConfigs.set('SecFilterConfigs','decorrelation_radius',4)
    FilterConfigs.set('SecFilterConfigs','read_decorrelation_from_file','no')

    FilterConfigs.set('SecFilterConfigs','background_errors_covariance_method','nmc') # {empirical, nmc ,...}
    FilterConfigs.set('SecFilterConfigs','background_noise_level',0.08) # {empirical, nmc ,...}
    FilterConfigs.set('SecFilterConfigs','background_noise_type','gaussian')
    FilterConfigs.set('SecFilterConfigs','update_B_factor',1.)

    FilterConfigs.set('SecFilterConfigs','model_errors_covariance_method','diagonal')
    FilterConfigs.set('SecFilterConfigs','model_error_steps_per_model_steps',10)
    FilterConfigs.set('SecFilterConfigs','model_noise_type','gaussian')
    FilterConfigs.set('SecFilterConfigs','model_noise_level',0.04)

    FilterConfigs.set('SecFilterConfigs','use_sparse_packages','yes') # use scipy.sparse.*

    FilterConfigs.set('SecFilterConfigs','periodic_decorrelation','no') # for periodic BC, we should use periodic decorrelation

    FilterConfigs.set('SecFilterConfigs','linear_system_solver','lu')

    # configurations for HMC sampler only
    FilterConfigs.set('SecFilterConfigs','Hamiltonian_integrator','verlet') # {verlet/2stage/3stage/4stage}
    FilterConfigs.set('SecFilterConfigs','Hamiltonian_step_size',0.001)
    FilterConfigs.set('SecFilterConfigs','Hamiltonian_number_of_steps',30)
    FilterConfigs.set('SecFilterConfigs','Hamiltonian_burn_in_steps',100)
    FilterConfigs.set('SecFilterConfigs','Hamiltonian_mixing_steps',20)
    FilterConfigs.set('SecFilterConfigs','hamiltonian_sampling_strategy','NUTS')
    FilterConfigs.set('SecFilterConfigs','mass_matrix_strategy','prior_precisions') #{scaled_identity/prior_variances/prior_precisions}
    FilterConfigs.set('SecFilterConfigs','mass_matrix_scale_factor',1.)

    FilterConfigs.set('SecFilterConfigs','prior_distribution','gaussian')






    #write, save, and close the configurations file
    ConfigsFile = open(file_name,'w')
    FilterConfigs.write(ConfigsFile)
    ConfigsFile.close()
    #===========================================================================



#===============================================================================
def write_model_and_filter_configs(src_model_configs_file="solver.inp",     src_model_directory="temp_model/",         \
                                   src_filter_configs_file="da_solver.inp", src_filter_directory="./",                 \
                                   target_configs_file="Configs.dat",        target_directory="assimilation_results/"  \
                                  ):
    """
    read both model and filter configurations files and save both in the assimilation results directory.
    """
    #============== 1: combine in a single configurations file
#    # read filter configurations,
#    filter_configs = read_filter_configurations(src_filter_directory+src_filter_configs_file)
#
#    # read model configurations
#    model_configs = read_original_model_configurations(file_name=src_model_configs_file , model_tmp_dir=src_model_directory)
#    # combine configurations from both model and filter
#    Combined_Configurations = {}
#
#
#
#    # write combined configurations. Some conflicts are removed.
#    #
#    # create and set the filter configurations section
#    Combined_Configs_Parser = ConfigParser.ConfigParser()
#    Combined_Configs_Parser.add_section('SecConfigs')
#    for key in Combined_Configurations.keys():
#        Combined_Configs_Parser.set('SecFilterConfigs',key,Combined_Configurations[key])
#    #
#    #write, save, and close the configurations file
#    trgt_file=target_directory+target_configs_file
#    ConfigsFile = open(trgt_file,'w')
#    Combined_Configs_Parser.write(ConfigsFile)
#    ConfigsFile.close()
#============== 2: just copy both...
    # copy model configurations file:
    src_file  = src_filter_directory + src_filter_configs_file
    shutil.copy(src_file , target_directory)

    #copy filter configurations file
    src_file  = src_model_directory + src_model_configs_file
    shutil.copy(src_file , target_directory)

    #===========================================================================




#===============================================================================
def read_filter_configurations(file_name = 'da_solver.inp'):
    """ read filter configurations and the model configurations and
        return two dictionaries containing the options needed.
    """
    FilterConfigs = ConfigParser.ConfigParser()
    FilterConfigs.read(file_name)



    # I think solver.inp does not require order
    #FilterConfigurations = collections.OrderedDict()
    FilterConfigurations = {}

    Filtersection = 'SecFilterConfigs'
    if not(FilterConfigs.has_section(Filtersection)):
        print " 'SectFilterConfigs' must be part of the configurations file"
        raise IOError
    else:
        # all options' keys are loaded in lower case...
        options = FilterConfigs.options(Filtersection)
        for option in options:
            if (option =='ensemble_size'):
                option_val = FilterConfigs.getint(Filtersection,option)
            elif (option =='initial_time'):
                option_val = FilterConfigs.getfloat(Filtersection,option)
            elif (option =='final_time'):
                option_val = FilterConfigs.getfloat(Filtersection,option)
            elif (option =='cycle_length'):
                option_val = FilterConfigs.getfloat(Filtersection,option)
            elif(option =='observation_steps_per_filter_steps'):
                option_val = FilterConfigs.getint(Filtersection,option)
            elif(option =='observation_chance_per_filter_steps'):
                option_val = FilterConfigs.getfloat(Filtersection,option)
            elif(option =='background_noise_level'):
                option_val = FilterConfigs.getfloat(Filtersection,option)
            elif(option =='model_error_steps_per_model_steps'):
                option_val = FilterConfigs.getint(Filtersection,option)
            elif(option =='update_B_factor'):
                option_val = FilterConfigs.getfloat(Filtersection,option)
            elif(option =='model_noise_level'):
                option_val = FilterConfigs.getfloat(Filtersection,option)
            elif(option =='observation_noise_level'):
                option_val = FilterConfigs.getfloat(Filtersection,option)
            elif(option =='observed_variables_jump'):
                option_val = FilterConfigs.getint(Filtersection,option)
            elif(option =='screen_output_iter'):
                option_val = FilterConfigs.getint(Filtersection,option)
            elif(option =='screen_output_iter'):
                option_val = FilterConfigs.getint(Filtersection,option)
            elif(option =='file_output_iter'):
                option_val = FilterConfigs.getint(Filtersection,option)
            elif(option =='decorrelation_radius'):
                option_val = FilterConfigs.getfloat(Filtersection,option)
            elif(option =='screen_output'):
                option_val = FilterConfigs.getboolean(Filtersection,option)
            elif(option =='file_output'):
                option_val = FilterConfigs.getboolean(Filtersection,option)
            elif(option =='file_output_means_only'):
                option_val = FilterConfigs.getboolean(Filtersection,option)
            elif(option =='decorrelate'):
                option_val = FilterConfigs.getboolean(Filtersection,option)
            elif(option =='read_decorrelation_from_file'):
                option_val = FilterConfigs.getboolean(Filtersection,option)
            elif(option =='use_sparse_packages'):
                option_val = FilterConfigs.getboolean(Filtersection,option)
            elif(option =='periodic_decorrelation'):
                option_val = FilterConfigs.getboolean(Filtersection,option)
            elif(option =='hamiltonian_step_size'):
                option_val = FilterConfigs.getfloat(Filtersection,option)
            elif(option =='hamiltonian_number_of_steps'):
                option_val = FilterConfigs.getint(Filtersection,option)
            elif(option =='hamiltonian_burn_in_steps'):
                option_val = FilterConfigs.getint(Filtersection,option)
            elif(option =='hamiltonian_mixing_steps'):
                option_val = FilterConfigs.getint(Filtersection,option)
            elif(option =='mass_matrix_scale_factor'):
                option_val = FilterConfigs.getfloat(Filtersection,option)
            else:
                option_val = FilterConfigs.get(Filtersection,option)

            FilterConfigurations.update({option:option_val})



    return FilterConfigurations
    #===========================================================================




#===============================================================================
def validate_filter_configurations(FilterConfigurations, ModelConfigurations):
    """ A function to make sure all necessary fields are conformable
        Inputs:
              - FilterConfigurations: a dictionary containing the filter options
              - ModelConfigurations : a dictionary containing the model options
        Outputs:
              - Ver_FilterConfigurations: a dictionary containing the verified filter options
              - Ver_ModelConfigurations : a dictionary containing the verified model options

    """
    # for now, lets assume the user follows the rules, and no validation is required.
    # will be writted after having a couple of models running.
    Ver_FilterConfigurations = FilterConfigurations
    Ver_ModelConfigurations  = ModelConfigurations

    return Ver_FilterConfigurations , Ver_ModelConfigurations
    #===========================================================================



#
##===============================================================================
#def rewrite_model_configurations(ModelConfigurations,file_name = 'solver.inp'):
#    """ write the configurations to a new solver.ini file given the new configurations.
#        this should be called between successive assimilation cycles.
#    """
#    #===========================================================================



#===============================================================================
def read_original_model_configurations(file_name = 'solver.inp',model_tmp_dir = 'temp_model'):
    """ read the configurations of the hypar model from solver.inp file.
        return a dictionary holding the necessary information.
        This should be called after calling get_hypar_model_files()
    """
    #
    original_model_configs = collections.OrderedDict()

    if model_tmp_dir.endswith('/'):
        file_name = model_tmp_dir+file_name
    else:
        file_name = model_tmp_dir+'/'+file_name


    src_cfile = open(file_name,'r')

    conf_lines = src_cfile.read()
    src_cfile.close()

    configs_list = conf_lines.splitlines()
    for ind in range(len(configs_list)):
        cfg_line = configs_list[ind]
        #
        if ( len(cfg_line.split())>=2 ):
            portions = cfg_line.split()
            if (portions[0]=='size') or (portions[0]=='iproc'):
                option_val     = np.array([int(i) for i in portions[1:]])
                original_model_configs.update({portions[0]:option_val})
            elif ( (portions[0] == 'ndims') or  (portions[0] == 'nvars' ) or  \
                   (portions[0] == 'ghost') or  (portions[0] == 'n_iter') or  \
                   (portions[0] == 'restart_iter')                        or \
                   (portions[0] == 'screen_op_iter')                      or \
                   (portions[0] == 'file_op_iter')
                 ):
                 original_model_configs.update( {portions[0]:int(portions[1])} )
            elif (portions[0] == 'dt')  :
                original_model_configs.update( {portions[0]:float(portions[1])} )
            else:
                # start verifying types...
                original_model_configs.update({portions[0]:' '.join(portions[1:])})
        elif(len(cfg_line.split())==1):
            pass
        else:
            raise IOError


    return original_model_configs

#----------------------------                  ---------------------------------
#----------------------------                  ---------------------------------

def overwrite_original_model_configurations(new_fields , file_name = 'solver.inp',model_tmp_dir = 'temp_model'):
    """ overwrite the original configurations of the hypar model from solver.inp file.
        given the new fields
        new_fields: a dictionary containing new fields. the corresponding fields will be overwritten in solver.inp
    """
    Flag = False # True if the required fields are overwritten. False if some fields are missing...
                 # I don't think we will want to add more configs to the model solver.inp (at least as of now!)
    cntr = 0 # counts the number of overwritten fields
    #

    if model_tmp_dir.endswith('/'):
        file_name = model_tmp_dir+file_name
    else:
        file_name = model_tmp_dir+'/'+file_name

    #original_model_configs = read_original_model_configurations()
    updated_model_configs = collections.OrderedDict()
    new_fields_keys = new_fields.keys()

    # read configurations from original 'file_name'
    src_cfile = open(file_name,'r')
    conf_lines = src_cfile.read()
    src_cfile.close()

    configs_list = conf_lines.splitlines()

    #
    for ind in range(len(configs_list)):
        cfg_line = configs_list[ind]
        if len(cfg_line.split())>=2 :
            #
            portions = cfg_line.split()
            updated_model_configs.update({portions[0]:portions[1:]})
            #
            for key in new_fields_keys:
                if key == portions[0]:
                    configs_list[ind]=configs_list[ind].replace(' '.join(portions[1:]) , str(new_fields[key]).strip("[ ]"))
                    updated_model_configs[portions[0]] = new_fields[key]
                    cntr+=1
                    break
    #
    # add absent configs:
    configs_list = configs_list[0:-1]
    for ind in range(len(new_fields_keys)):
        if not(updated_model_configs.has_key(new_fields_keys[ind])):
            new_key = new_fields_keys[ind]
            updated_model_configs.update({new_key:new_fields[new_key]})
            new_line = "  "+new_key.ljust(18)+str(new_fields[new_key]).strip("[ ]")
            configs_list.append(new_line)
    configs_list.append('end')

    #overwrite the solver.inp file inside the 'model_tmp_dir'
    trgt_cfile = open(file_name,'w')
    trgt_cfile.write('\n'.join(configs_list))
    trgt_cfile.close()

    if cntr==len(new_fields_keys):
        Flag = True
    return Flag , updated_model_configs
    #


def create_original_model_configurations(initial_fields={} , file_name = 'solver.inp',model_tmp_dir = 'temp_model'):
    """ create a configuration file, initialized to fields in "initial_fields".
        new_fields: a dictionary containing new fields.
        If "solver.inp" exists, it will be overwritten.
    """
    #

    if model_tmp_dir.endswith('/'):
        file_name = model_tmp_dir+file_name
    else:
        file_name = model_tmp_dir+'/'+file_name

    keys = initial_fields.keys()
    trgt_cfile = open(file_name,'w')
    trgt_cfile.write('Begin \n')

    for ind in range(len(keys)):
        new_key = keys[ind]
        new_option = initial_fields[new_key]
        if new_key=='size' or new_key=='iproc':
            new_option = str(new_option).strip("[ ]")

        new_line = "  "+new_key.ljust(18)+str(new_option) +"\n"
        trgt_cfile.write(new_line)

    trgt_cfile.write('End')
    trgt_cfile.close()
    #




#----------------------------                  ---------------------------------
#----------------------------                  ---------------------------------
#
def detect_initial_condition_format(file_name = 'solver.inp',model_tmp_dir = 'temp_model'):
    """ check the format of the initial condition file: asccii, binary
    """

    if model_tmp_dir.endswith('/'):
        file_name = model_tmp_dir+file_name
    else:
        file_name = model_tmp_dir+'/'+file_name

    #
    original_model_configs = read_original_model_configurations()

    if original_model_configs.has_key('ip_file_type'):
        ip_file_type = original_model_configs['ip_file_type']
    else:
        ip_file_type = 'ascii'

    return ip_file_type




#----------------------------                  ---------------------------------
#----------------------------                  ---------------------------------
#
def read_model_txt_ouput( file_name = 'op.dat',model_tmp_dir = 'temp_model'):
    """
    """

    if model_tmp_dir.endswith('/'):
        file_name = model_tmp_dir+file_name
    else:
        file_name = model_tmp_dir+'/'+file_name

    model_Configs = read_original_model_configurations()

    ndims = model_Configs['ndims']
    nvars = model_Configs['nvars']
    size = model_Configs['size']
    grid_size_sum = size.sum()

    #
    #fptr = open(file_name,'r')
    out_txt = np.genfromtxt(file_name)
    #fptr.close()

    # calculate the state vector size
    try:
        n,m = out_txt.shape
    except:
        m = out_txt.size
        n=1
    else:
        pass

    stateVec_size = nvars*size.prod()

    # validate against model_Configs info
    if ( (n*nvars) != stateVec_size) :
        raise ValueError( "Dimension missmatch! Check 'read_model_txt_ouput' " )

    lst = m-1
    frst = 2*ndims # this is for index and grid columns
    rng = range(frst,lst+1)


    # read state vector (1D,2D,3D)
    stateVec = np.zeros(stateVec_size)
    if n==1:
        stateVec = out_txt[ frst:lst+1 ]
    else:
        stateVec = out_txt[ 0:n , frst:lst+1 ]
        #stateVec = stateVec.reshape(stateVec.size)
        stateVec = stateVec.ravel()
        if np.isnan(stateVec.any()):
            raise ValueError("\n\t Model failed to integrate the initial condition in 'initial.inp' forward in time!")

        #for i in range(len(rng)):
        #    #print 'out_txt[ : , ',i+frst ,']',out_txt[ : , i+frst ]
        #    if np.isnan(out_txt[:,i+frst]).any():
        #        raise ValueError("\n\t Model failed to integrate the initial condition in 'initial.inp' forward in time!")
        #    stateVec[i*n : (i+1)*n ] = out_txt[ : , i+frst ]





    #if (ndims ==1):
    #    stateVec = out_txt[:,2]
    #elif (ndims ==2):
    #    pass
    #    #
    #elif (ndims ==3):
    #    pass
    #    #
    #else:
    #    raise IOError("Check indexing in 'read_model_txt_ouput' ")


    #read grid vector
    # there is a better way to write this down...
    #the first (ndim) columns are indices, then grids, then variables.
    grid = np.zeros(grid_size_sum)

    if (ndims ==1):
        if n==1:
            grid = np.array([0])
        else:
            grid = out_txt[:,1]

    elif (ndims ==2):
        grid[ 0:size[0] ] = out_txt[0:size[0] , 2]
        locs = np.arange( 0 , size[0]*size[1] , size[0] )
        grid[ size[0]:size[0]+size[1] ] = out_txt[locs , 3]

    elif (ndims ==3):
        pass
        grid[ 0:size[0] ] = out_txt[ 0:size[0] ,3]
        locs = np.arange(0,size[0]*size[1],size[0])
        grid[ size[0]:size[0]+size[1] ] = out_txt[locs , 4]
        locs = np.arange(0,size[0]*size[1]*size[2],size[0]*size[1])
        grid[size[0]+size[1] : size[0]+size[1]+size[2] ] = out_txt[locs , 5]

    elif (ndims ==4):
        raise ValueError("4D case is not handled yet! Check indexing in 'read_model_txt_ouput' ")
    else:
        raise ValueError("Check indexing in 'read_model_txt_ouput' ")


    #return grid , stateVec
    #return arngd_state
    return stateVec



#----------------------------                  ---------------------------------
#----------------------------                  ---------------------------------
#
def read_model_txt_initial_condition( file_name = 'initial.dat',model_tmp_dir = 'temp_model'):
    """
       read the grid and the state vecor from text initial condition file
    """

    if model_tmp_dir.endswith('/'):
        file_name = model_tmp_dir+file_name
    else:
        file_name = model_tmp_dir+'/'+file_name

    model_Configs = read_original_model_configurations()

    ndims = model_Configs['ndims']
    nvars = model_Configs['nvars']
    size  = model_Configs['size']

    #
    fptr = open(file_name,'r')
    file_contents = fptr.readlines()
    fptr.close()


    grid_size  = np.prod(size)
    state_size = nvars*grid_size
    stateVec   = np.zeros(state_size)

    #var_size   = state_size/nvars


    # read the grid of the first dimension (space separated pattern).
    for var_ind in range(ndims ,ndims+nvars):
        var_str = file_contents[var_ind].split()
        var_val = np.array([float(x) for x in var_str])
        #strt = (var_ind-ndims) * var_size
        #end  = strt + var_size - 1
        #stateVec[strt: end+1] = var_val[:]

        Ind = (var_ind-ndims) + nvars*np.array(range(grid_size))
        stateVec[Ind] = var_val[:]


        #

    return stateVec



# write the state as text ouput state.
def write_state_as_model_ouput(state, Model_Object , file_name = 'op.dat',model_tmp_dir = 'temp_model'):
    """
    write the grid and the state as text ouput state.
    """




#----------------------------                  ---------------------------------
#----------------------------                  ---------------------------------
#
def write_output_as_binary_initial_codition(grid , state_Vec , ndims , nvars, model_tmp_dir = 'temp_model'):
    """ write the solution of the data assimilation filter as initial condition of the model for the next cycle in
        binary format.
        this is done for now by writing as text then convert in C to binary format for portability.
        This is to avoid portability problems for the moment!!!
    """
    dest = os.getcwd()

    # rewrite all initial condition files in binary as a first step (to rewrite topology,etc.) in binary
    check_initial_condition(dest )



    os.chdir(model_tmp_dir)
    fptr = open('op.dat.txt','w')

    #write number of dimensions
    fptr.write("%d"%ndims +"\n")

    #write number of prognostic variables
    fptr.write("%d"%nvars +"\n")

    #write size of grid vector
    fptr.write("%d"%grid.size +"\n")
    #write size of state vector
    fptr.write("%d"%state_Vec.size +"\n")

    #write dimensions (grid)
    for grd_pnt in grid:
        fptr.write("%1.16e"%grd_pnt +" ")

    fptr.write("\n")
    #write state
    for X_i in state_Vec:
        fptr.write("%1.16e"%X_i +" ")

    fptr.close()


    # Convert the text file into C-binary-format initial conditon file
    # the executable should be here or the exec_dir should be changed

    binary_writer = DA_Pack_PATH+'write_binary_ic.o'+ '>model_screen_out.dat'
    #binary_writer = './write_binary_ic.o'

    #print binary_writer
    if os.path.isfile(binary_writer):
        os.system(binary_writer)
    elif os.path.isfile(DA_Pack_PATH+'write_binary_ic.c'):
        #os.system(CCompiler+' write_binary_ic.c -o ' +DA_Pack_PATH+'write_binary_ic.o')
        os.system(CCompiler+' '+DA_Pack_PATH+'write_binary_ic.c'+' -o ' +binary_writer)

        os.system(binary_writer)
    else:
        raise IOError

    #os.remove('op.dat.txt')
    os.chdir(dest)




    #
    #===========================================================================


def clean_model_dir(tmp_dir='temp_model'):
    """
    """
    if(os.path.exists(tmp_dir)):
        print 'old files exist, cleaning-up || ',
        shutil.rmtree(tmp_dir)

    os.mkdir(tmp_dir)





#===============================================================================
def prepare_hypar_model_files(dest,create_IC=False , tmp_dir='temp_model'):
    """ Copy the necessary model files ot a new directory.
        This includes the PETSc necessary files if PETSc is to be used for time-stepping.
        This should be the first thing to do while working with hypar models after calling read_filter_configurations()!
        dest: is the full path of the working directory where the test model function resides.
        create_IC : flag to create initial condition if not existent
        temp_model : dirctory to hold the necessary model files
    """
    # get the corresponding directory of original hypar directory: dest --> src
    dirs = dest.split('/')
    dirs.reverse()
    try:
        dapack_ind = len(dirs)-dirs.index('dapack')-1
    except:
        dapack_ind = len(dirs)-dirs.index('dapack_attia_activeresearch')-1

    dirs.reverse()
    dapack_RealPath='/'.join(dirs[0:dapack_ind+1])
    example_RelativePath = '/'.join(dirs[dapack_ind+2:len(dirs)+1])
    source_model_path = dapack_RealPath + '/models/'+example_RelativePath



    # create new directory with folder_name and copy model files in it
    if(os.path.exists(tmp_dir)):
        print 'old files exist, cleaning-up || ',
        shutil.rmtree(tmp_dir)

    # copy the whole contents of source folder
    print 'copying necessary model files || ',
    model_dest = dest+'/'+tmp_dir
    shutil.copytree(source_model_path , model_dest)


    if (create_IC):
        check_initial_condition(dest)


    #return the mpdel path.
    return model_dest

#--------------------------   ----------------------   -------------------------
#--------------------------   ----------------------   -------------------------

def check_initial_condition(dest , file_name = 'initial.inp' , tmp_dir='temp_model' ):
    """ compile init.c/C file to create the first initial conditon
    """
    # look for the initial condition file; if unavailable, compile the ini.c/ini.C and generate it.
    if tmp_dir.endswith('/'):
        initial_sol_file = tmp_dir+file_name
        def_file_name    = tmp_dir+'initial.inp'
    else:
        initial_sol_file = tmp_dir+'/'+file_name
        def_file_name    = tmp_dir+'/'+'initial.inp'

    if (os.path.isfile(initial_sol_file)):
        #print 'Initial conditions file exists... backing up'
        os.rename(initial_sol_file , initial_sol_file+'.bk') # overwrite .bk if exists

    if (os.path.isfile(def_file_name)):
        os.rename(def_file_name, def_file_name+'.bk') # overwrite .bk if exists


    #print 'initial_sol_file', initial_sol_file
    #print os.path.isfile(initial_sol_file)

    if not(os.path.isfile(initial_sol_file)):
        os.chdir(tmp_dir)
        if not(os.path.exists('aux')):
            print "\nthere is no folder named: 'aux'!!!"
            raise IOError
        else:
            #print 'Creating initial condition'
            #access, compile and run the init.c file.
            # WE NEED to add a CHECK for the c compiler...
            # Try :
            # >>> from sysconfig import get_config_vars
            # >>> get_config_vars('CC', 'CXX', 'LDSHARED') and get first argument of splits...
            filelist = [ f for f in os.listdir('aux/') if (f=='init.c' or f=='init.C') ]
            if len(filelist)>1:
                raise IOError("\n\tMore than one file to create initial conditions exist!!!")
            elif len(filelist)==1:
                if(os.path.isfile('./aux/run_options')):
                    with open('./aux/run_options.txt','r') as opts:
                        run_options = opts.read()
                        run_options = run_options.splitlines()
                        run_options = " ".join(run_options)
                else:
                    run_options = ''


                #CCompiler = 'g++'
                #print CCompiler+' ./aux/'+filelist[0]+' '+run_options+' '+' -o create.out'
                os.system(CCompiler+' ./aux/'+filelist[0]+' '+run_options+' '+' -o create.out')
                os.system('./create.out >model_screen_out.dat')
                os.remove('./create.out')
            else:
                raise IOError("\n\tThe file required to create initial conditons is missing!!!")
            #print 'Initial condition created...'
        os.chdir(dest)

    else:
        # Initial condition file already exists.
        print 'Initial condition file exists. Will be used as is...' # may be we need to make sure it is a binary file!!!


    if (file_name !='initial.inp'):
        if os.path.isfile(def_file_name):
            os.rename(def_file_name , initial_sol_file)
        else:
            raise IOError("\n\tinitial.inp file is not found! Mostly, the ip_file_format is not supported!")





    #If neither the initial conditions or the necessary init.c do not exist rise an error

    #===========================================================================





#===============================================================================
def create_decorrelation_1d(StateVec_Size , Nvars , dx , Decorr_Radius, Decorr_Tol=1e-5 , Periodic_BC=False , Sparse=False ,Sparse_type='lil_matrix'):
    """ Create localization array for a one-dimensional grid with or without periodic boundaries.
        Here I assume the distance between any two grid-points is set to 1. The radius is chosen accordingly.
        Generalization to different scales is straight forward. Will discuss with Emil first...
    """
    if Sparse and ( not(Sparse_type.lower() in ['lil_matrix','csr_matrix','csc_matrix']) ):
        raise ValueError("Sparse_type ["+Sparse_type+"] is not supported! output must be in: ['lil_matrix','csr_matrix','csc_matrix'] ")

    # initialize the localization matrix
    if Sparse:
        # Sparse array with structure specified in Sparse_type
        Decorr_mat = sparse.lil_matrix((StateVec_Size , StateVec_Size))
        # set main diagonal to 1:
        #Decorr_mat.setdiag(1)
    else:
        #Decorr_mat = np.eye(StateVec_Size)
        Decorr_mat = np.zeros((StateVec_Size,StateVec_Size))



    # the main diagonal and the upper (_nvars) diagonals are all set to one...
    # this the decorrelation on the same grid point for different prognostic variables...
    #i_inds = [i for i in xrange(StateVec_Size)]
    #for ind_var in xrange(1,Nvars):
    #    if Periodic_BC:
    #        j_inds = [j%StateVec_Size for j in xrange(ind_var ,StateVec_Size+ind_var)]
    #        Decorr_mat[i_inds, j_inds] = 1
    #        Decorr_mat[j_inds, i_inds] = 1 # by syemmetry. Assuming the grid is
    #    else:
    #        j_inds = [j for j in xrange(ind_var ,StateVec_Size)]
    #        Decorr_mat[i_inds[0:-ind_var], j_inds] = 1
    #        Decorr_mat[j_inds, i_inds[0:-ind_var]] = 1 # by syemmetry. Assuming the grid is

    # Loop to create the upper triangular part.
    grid_size = (StateVec_Size/Nvars)
    for i_gr in xrange(grid_size):
        i_ind = i_gr*Nvars
        #;
        if Periodic_BC: #  consider interchanging the outer loop with this test.
            # go forward and backward... break after mid-distance  or coeff<1e-16.
            #The goal is to reduce computational cost and reduce redundancy,
            for j_gr in xrange(i_gr , np.int(np.ceil((grid_size+1-i_gr)/2.0)) ):
                j_ind = j_gr*Nvars
                #
                dst = (i_gr - j_gr)*dx
                decorr_coeff = np.exp(-((float(dst)/Decorr_Radius)**2))
                ##print decorr_coeff
                if decorr_coeff<Decorr_Tol:
                    break
                else:
                    # first direction
                    i_inds,j_inds = np.meshgrid([var for var in xrange(i_ind , i_ind+Nvars)] ,[var for var in xrange(j_ind , j_ind+Nvars)] )
                    Decorr_mat[i_inds, j_inds] = decorr_coeff
                    if i_gr!=j_gr:
                        Decorr_mat[j_inds, i_inds] = decorr_coeff # by symmetry

                    # opposit direction
                    jend_gr = grid_size - j_gr
                    jend_ind = jend_gr*Nvars
                    i_inds,j_inds = np.meshgrid([var for var in xrange(i_ind , i_ind+Nvars)] ,[var for var in xrange(jend_ind , jend_ind+Nvars)] )
                    Decorr_mat[i_inds, j_inds] = decorr_coeff # from periodicity
                    if i_gr!=j_gr:
                        Decorr_mat[j_inds, i_inds] = decorr_coeff # by symmetry
            #
            # Periodic BCs case is done...



        else:
            # one break statement (easy)
            for j_gr in xrange(i_gr , grid_size):
                j_ind = j_gr*Nvars
                #
                dst = (i_gr - j_gr)*dx
                decorr_coeff = np.exp(-((float(dst)/Decorr_Radius)**2))
                if decorr_coeff<Decorr_Tol:
                    break
                else:
                    i_inds,j_inds = np.meshgrid([var for var in xrange(i_ind , i_ind+Nvars)] ,[var for var in xrange(j_ind , j_ind+Nvars)] )
                    Decorr_mat[i_inds, j_inds] = decorr_coeff
                    if i_gr!=j_gr:
                        Decorr_mat[j_inds, i_inds] = decorr_coeff # by symmetry
            #
            # Non-periodic BCs case is done...



    # change sparsity structure if needed!
    if Sparse and (Sparse_type.lower()!='lil_matrix'):
        if (Sparse_type=='csr_matrix'):
            Decorr_mat = Decorr_mat.tocsr()
        elif (Sparse_type=='csc_matrix'):
            Decorr_mat = Decorr_mat.tocsc()
        else:
            raise ValueError("This error should not appear. Sparse_type should not take value: ["+Sparse_type+"]!")



    return Decorr_mat
    #===========================================================================







#===============================================================================
def create_decorrelation_2d(StateVec_Size , Nvars , NI , NJ ,dx,dy, Decorr_Radius ,Decorr_Tol=1e-5  ,  Periodic_BC=False , Sparse=False ,Sparse_type='lil_matrix'):
    """ Create localization array for a one-dimensional grid with or without periodic boundaries.
        Here I assume the distance between any two grid-points is set to 1. The radius is chosen accordingly.
        Generalization to different scales is straight forward. Will discuss with Emil first...
        To be tested...
    """
    if Sparse and ( not(Sparse_type.lower() in ['lil_matrix','csr_matrix','csc_matrix']) ):
        raise ValueError("Sparse_type ["+Sparse_type+"] is not supported! output must be in: ['lil_matrix','csr_matrix','csc_matrix'] ")

    # initialize the localization matrix
    if Sparse:
        # Sparse array with structure specified in Sparse_type
        Decorr_mat = sparse.lil_matrix((StateVec_Size , StateVec_Size))
    else:
        Decorr_mat = np.zeros((StateVec_Size,StateVec_Size))


    if Periodic_BC:
        #-----------------------------------------
        # Assume periodicity at j=0, j=NJ-1
        #-----------------------------------------
        cut_off_radius = np.sqrt(NI**2 + NJ**2) # this is the maximum distance between two grid points. (equally-spaced points)
        #Loop over all grid point given x-y coordinates...
        for x_coord in xrange(NI):
            for y_coord in xrange(NJ):
                # point location in the state vector 2D->1d
                #P1_loc_in_Vec = x_coord*NJ+y_coord
                P1_loc_in_Vec = y_coord*NI+x_coord
                #
                # now-loop over grid-points after (x_coord,y_coord)
                for i_ind in xrange(x_coord,NI):
                    # stop proping x-direction if the cutt-off radius is passed.
                    if ((i_ind-x_coord)*dx)>=cut_off_radius:
                        #print 'Cut at i= ',i_ind , 'from fixed i= ',x_coord
                        break

                    # prope to the right (max half the NJ distance to the right
                    next_j_inds = np.asarray([j_right for j_right in xrange(y_coord,y_coord+(NJ/2) )] ) # j>NJ-1 gets a negative value later
                    # out of bounds to the right is actuall at the left bound
                    excess_locs = next_j_inds>(NJ-1)
                    adj_next_j_inds = next_j_inds[:]
                    if excess_locs.any():
                        adj_next_j_inds[excess_locs] = adj_next_j_inds[excess_locs]-(NJ-1)
                    for loc in xrange(adj_next_j_inds.size):
                        j_ind = adj_next_j_inds[loc]
                        # point location in the state vector 2D->1d
                        #P2_loc_in_Vec = i_ind*NJ+j_ind
                        P2_loc_in_Vec = j_ind*NI+i_ind
                        #
                        #Evaluate the Euclidean distance: (use the shorter distance)
                        dst = np.sqrt(((x_coord-i_ind)*dx)**2 + ((y_coord-next_j_inds[loc])*dy)**2)
                        decorr_coeff = np.exp(-((float(dst)/Decorr_Radius)**2))
                        #print decorr_coeff
                        if decorr_coeff<Decorr_Tol:
                            decorr_coeff = 0
                            # reduce the cut-off radius based on results in the first row of the grid. Never exceed this radius in x direction from now on.
                            if x_coord==0 and dst<cut_off_radius:
                                cut_off_radius = dst
                            #if max(abs(i_ind-x_coord),abs(next_j_inds[loc]-y_coord))<cut_off_radius :
                            #    cut_off_radius=max((i_ind-x_coord),(next_j_inds[loc]-y_coord))
                            #print '>>',P1_loc_in_Vec,
                            #print '>>',P2_loc_in_Vec
                            break
                        # ----> Update the decorrelations as long as break is not hit
                        # set the decorrelation coefficients between variables at these grid points to decorr_coeff
                        # all variables in grid-points "P1_loc_in_Vec","P1_loc_in_Vec" is assigned the same decorrelation coefficient
                        P1_inds,P2_inds = np.meshgrid( [ var for var in xrange( (P1_loc_in_Vec*Nvars) , (P1_loc_in_Vec*Nvars)+Nvars) ] ,    \
                                                       [ var for var in xrange( (P2_loc_in_Vec*Nvars) , (P2_loc_in_Vec*Nvars)+Nvars) ]      \
                                                     )
                        Decorr_mat[P1_inds, P2_inds] = decorr_coeff
                        if not( x_coord==i_ind and y_coord==j_ind ):
                            Decorr_mat[P2_inds, P1_inds] = decorr_coeff # by symmetry


                    # prope to the left (max half the NJ distance to the left
                    prev_j_inds = np.asarray([j_left for j_left in xrange(y_coord-1,y_coord-(NJ/2) )] ) # evaluate distance using these
                    # out of bounds to the right is actuall at the left bound
                    excess_locs = prev_j_inds<0
                    adj_prev_j_inds = prev_j_inds[:]
                    if excess_locs.any():
                        adj_prev_j_inds[excess_locs] = adj_prev_j_inds[excess_locs]+(NJ-1) # store in these locations
                    for loc in xrange(adj_prev_j_inds.size):
                        j_ind = adj_prev_j_inds[loc]
                        if x_coord==i_ind: # this will remove redundancy
                            break
                        # point location in the state vector 2D->1d
                        #P2_loc_in_Vec = i_ind*NJ+j_ind
                        P2_loc_in_Vec = j_ind*NI+i_ind
                        #
                        #Evaluate the Euclidean distance:
                        dst = np.sqrt(((x_coord-i_ind)*dx)**2 + ((y_coord-prev_j_inds[loc])*dy)**2)
                        decorr_coeff = np.exp(-((float(dst)/Decorr_Radius)**2))
                        #print decorr_coeff
                        if decorr_coeff<Decorr_Tol:
                            decorr_coeff = 0
                            if x_coord==0 and dst<cut_off_radius:
                                cut_off_radius = dst
                            #if max((i_ind-x_coord) , (prev_j_inds[loc]-y_coord) )<cut_off_radius:
                            #    cut_off_radius=max((i_ind-x_coord) , (prev_j_inds[loc]-y_coord) )
                            #print '>>',P1_loc_in_Vec,
                            #print '>>',P2_loc_in_Vec
                            break
                        # ----> Update the decorrelations as long as break is not hit
                        # set the decorrelation coefficients between variables at these grid points to decorr_coeff
                        # all variables in grid-points "P1_loc_in_Vec","P1_loc_in_Vec" is assigned the same decorrelation coefficient
                        P1_inds,P2_inds = np.meshgrid( [ var for var in xrange( (P1_loc_in_Vec*Nvars) , (P1_loc_in_Vec*Nvars)+Nvars) ] ,    \
                                                       [ var for var in xrange( (P2_loc_in_Vec*Nvars) , (P2_loc_in_Vec*Nvars)+Nvars) ]      \
                                                     )
                        Decorr_mat[P1_inds, P2_inds] = decorr_coeff
                        if not( x_coord==i_ind and y_coord==j_ind ):
                            Decorr_mat[P2_inds, P1_inds] = decorr_coeff # by symmetry
        #

    else:
        #
        #cut_off_radius = np.infty
        cut_off_radius = np.sqrt(NI**2 + NJ**2) # this is the maximum distance between two grid points. (equally-spaced points)
        #Loop over all grid point given x-y coordinates...
        for x_coord in xrange(NI):
            for y_coord in xrange(NJ):
                # point location in the state vector 2D->1d
                #P1_loc_in_Vec = x_coord*NJ+y_coord
                P1_loc_in_Vec = y_coord*NI+x_coord
                #
                # now-loop over grid-points after (x_coord,y_coord)
                for i_ind in xrange(x_coord,NI):
                    # stop proping x-direction if the cutt-off radius is passed.
                    if ((i_ind-x_coord)*dx)>=cut_off_radius:
                        #print 'Cut at i= ',i_ind , 'from fixed i= ',x_coord
                        break
                    # prope to the right
                    for j_ind in xrange(y_coord,NJ):
                        # point location in the state vector 2D->1d
                        #P2_loc_in_Vec = i_ind*NJ+j_ind
                        P2_loc_in_Vec = j_ind*NI+i_ind
                        #
                        #Evaluate the Euclidean distance:
                        dst = np.sqrt(((x_coord-i_ind)*dx)**2 + ((y_coord-j_ind)*dy)**2)
                        decorr_coeff = np.exp(-((float(dst)/Decorr_Radius)**2))
                        #print decorr_coeff
                        if decorr_coeff<Decorr_Tol:
                            decorr_coeff = 0
                            if x_coord==0 and dst<cut_off_radius:
                                cut_off_radius = dst
                            #if max((i_ind-x_coord),(j_ind-y_coord))<cut_off_radius :
                            #    cut_off_radius=max((i_ind-x_coord),(j_ind-y_coord))
                            #print '>>',P1_loc_in_Vec,
                            #print '>>',P2_loc_in_Vec
                            break
                        # ----> Update the decorrelations as long as break is not hit
                        # set the decorrelation coefficients between variables at these grid points to decorr_coeff
                        # all variables in grid-points "P1_loc_in_Vec","P1_loc_in_Vec" is assigned the same decorrelation coefficient
                        P1_inds,P2_inds = np.meshgrid( [ var for var in xrange( (P1_loc_in_Vec*Nvars) , (P1_loc_in_Vec*Nvars)+Nvars) ] ,    \
                                                       [ var for var in xrange( (P2_loc_in_Vec*Nvars) , (P2_loc_in_Vec*Nvars)+Nvars) ]      \
                                                     )
                        Decorr_mat[P1_inds, P2_inds] = decorr_coeff
                        if not( x_coord==i_ind and y_coord==j_ind ):
                            Decorr_mat[P2_inds, P1_inds] = decorr_coeff # by symmetry


                    # prope to the left
                    for j_ind in xrange(y_coord-1,-1,-1):
                        if x_coord==i_ind: # this will remove redundancy
                            break
                        # point location in the state vector 2D->1d
                        #P2_loc_in_Vec = i_ind*NJ+j_ind
                        P2_loc_in_Vec = j_ind*NI+i_ind
                        #
                        #Evaluate the Euclidean distance:
                        dst = np.sqrt(((x_coord-i_ind)*dx)**2 + ((y_coord-j_ind)*dy)**2)
                        decorr_coeff = np.exp(-((float(dst)/Decorr_Radius)**2))
                        #print decorr_coeff
                        if decorr_coeff<Decorr_Tol:
                            decorr_coeff = 0
                            if x_coord==0 and dst<cut_off_radius:
                                cut_off_radius = dst
                            #if max((i_ind-x_coord),(j_ind-y_coord))<cut_off_radius :
                            #    cut_off_radius=max((i_ind-x_coord),(j_ind-y_coord))
                            #print '>>',P1_loc_in_Vec,
                            #print '>>',P2_loc_in_Vec
                            break
                        # ----> Update the decorrelations as long as break is not hit
                        # set the decorrelation coefficients between variables at these grid points to decorr_coeff
                        # all variables in grid-points "P1_loc_in_Vec","P1_loc_in_Vec" is assigned the same decorrelation coefficient
                        P1_inds,P2_inds = np.meshgrid( [ var for var in xrange( (P1_loc_in_Vec*Nvars) , (P1_loc_in_Vec*Nvars)+Nvars) ] ,    \
                                                       [ var for var in xrange( (P2_loc_in_Vec*Nvars) , (P2_loc_in_Vec*Nvars)+Nvars) ]      \
                                                     )
                        Decorr_mat[P1_inds, P2_inds] = decorr_coeff
                        if not( x_coord==i_ind and y_coord==j_ind ):
                            Decorr_mat[P2_inds, P1_inds] = decorr_coeff # by symmetry



    #print repr(Decorr_mat)
    return Decorr_mat
    #===========================================================================


#===============================================================================
def create_decorrelation_3d(StateVec_Size , Nvars , NI , NJ ,dx,dy,dz, Decorr_Radius ,Decorr_Tol=1e-5  ,  Periodic_BC=False , Sparse=False ,Sparse_type='lil_matrix'):
    """ Create localization array for a three-dimensional grid with or without periodic boundaries.
    """
    raise NotImplementedError()



def oneD_to_twoD_indexes(index , state_size , nvars , NI , NJ):
    """
    Find the i,j coordinates in a 2-D grid corresponding to 1-D index.
    """
    oneD_ind = state_size%nvars # now all points have single variable.
    i_ind = oneD_ind/NI # the integer part of the result
    j_ind = oneD_ind%NI # the remainder part of the result

    return i_ind,j_ind



#===============================================================================
def create_and_save_Decorr(stateVec_Size,             n_vars,              n_dims,                   \
                           size_arr,                  spacings_arr,        decorr_radius,            \
                           decorr_Tol=1e-5,           periodic_BC=False,   sparse_flg=False,         \
                           sparse_type='lil_matrix',  file_type='hdf5',    file_name='Decorr.hdf5'   \
                          ):
    """ Create and save a localization array for a One, Two, or Three dimensional model grid:
    """

    if sparse_flg and ( not(sparse_type.lower() in ['lil_matrix','csr_matrix','csc_matrix']) ):
        raise ValueError("Sparse_type ["+sparse_type+"] is not supported! output must be in: ['lil_matrix','csr_matrix','csc_matrix'] ")

    if not(file_type.lower() in ['hdf5'] ):
        raise ValueError("file_type ["+file_type+"] is not supported! As of now, output file_type must be in: ['hdf5''] ")

    if n_dims==1:
        Decorr_mat = create_decorrelation_2d(StateVec_Size = stateVec_Size            , \
                                             Nvars         = n_vars                   , \
                                             dx            = spacings_arr[0]          , \
                                             Decorr_Radius = decorr_radius            , \
                                             Decorr_Tol    = decorr_Tol               , \
                                             Periodic_BC   = periodic_BC              , \
                                             Sparse        = sparse_flg               , \
                                             Sparse_type   = sparse_type                \
                                            )

    elif n_dims==2:
        Decorr_mat = create_decorrelation_1d(StateVec_Size = stateVec_Size            , \
                                             Nvars         = n_vars                   , \
                                             NI            = size_arr[0]              , \
                                             NJ            = size_arr[1]              , \
                                             dx            = spacings_arr[0]          , \
                                             dy            = spacings_arr[1]          , \
                                             Decorr_Radius = decorr_radius            , \
                                             Decorr_Tol    = decorr_Tol               , \
                                             Periodic_BC   = periodic_BC              , \
                                             Sparse        = sparse_flg               , \
                                             Sparse_type   = sparse_type                \
                                            )

    elif n_dims==3:
        raise NotImplementedError()
    elif n_dims==4:
        raise NotImplementedError()
    else:
        raise ValueError("Number of dimensions is [ "+n_dims+" ] > 4?!")


    # the localization array has to be saved as full array even if it is created as sparse.
    fptr = h5py.File(file_name , 'w')
    if sparse_flg:
        # convert to full before saving, and write the decorrelation array to file.
        fptr.create_dataset('Decorr',data=Decorr_mat.toarray())
    else:
        # Write the decorrelation array to file.
        fptr.create_dataset('Decorr',data=Decorr_mat)

    # save and close
    fptr.close()

    # to read the data in this file, you need to slice the data set.
    # >>> fptr = h5py.File(file_name , 'r')
    # >>> Decorr_data_set = fptr['Decorr']   <-- does not read immediately from disk
    # >>> Decorr_mat = Decorr_data_set[:,:]  <-- read from disk based on the needed slice
    # >>> fptr.close()    <-- do not forget to close your file!!!













def write_analysis_ensemble_states(Ensemble , Model_Object , nens,file_prefix = 'ensemble_mem',out_dir = 'ensemble_out/'):
    """
    write each column of "Initial_Ensemble" as output vector to be propagated forward during the forecast step
    """
    n,m = np.shape(Ensemble)


    if not(out_dir.endswith('/')):
        out_dir = out_dir+'/'

    if not(os.path.exists(out_dir)):
        os.mkdir(out_dir)



    # get model grids:
    ndims = Model_Object._ndims
    dims  = Model_Object._size
    nvars = Model_Object._nvars
    grids = Model_Object._Grids_dict # keys are strings in range(ndims)
    sizes = Model_Object._size


    # This can be written in a better and concise way...
    for i_ens in range(nens):
        # write the state in column i_ens
        file_name = out_dir + file_prefix+'_'+str(i_ens)+'.dat'
        fptr      = open(file_name , 'w')

        if ndims==1:
            grid = grids['0']
            var  = 0
            jump = np.prod(sizes)
            for i_dim in range(dims[0]):
                fptr.write( "%4d"%i_dim +" "+ "%1.16e"%grid[i_dim])
                for var_ind in range(nvars):
                    #fptr.write(" "+"%1.16e"%Ensemble[var+jump*var_ind][i_ens])
                    fptr.write(" "+"%1.16e"%Ensemble[var+var_ind , i_ens])
                fptr.write("\n")
                #var+=1
                var+=nvars

        elif ndims==2:
            var = 0
            jump = np.prod(sizes)
            grid_i = grids['0']
            grid_j = grids['1']
            for j_dim in range(dims[1]):
                for i_dim in range(dims[0]):
                    fptr.write( "%4d"%i_dim +" "+"%4d"%j_dim +" "+ "%1.16e"%grid_i[i_dim]+" "+ "%1.16e"%grid_j[j_dim])
                    for var_ind in range(nvars):
                        #fptr.write(" "+"%1.16e"%Ensemble[var+(jump*var_ind)][i_ens])
                        fptr.write(" "+"%1.16e"%Ensemble[var+var_ind , i_ens])
                    fptr.write("\n")
                    #var+=1
                    var+=nvars

        elif ndims==3:
            var = 0
            jump = np.prod(sizes)
            grid_i = grids['0']
            grid_j = grids['1']
            grid_k = grids['2']
            for k_dim in range(dims[2]):
                for j_dim in range(dims[1]):
                    for i_dim in range(dims[0]):
                        fptr.write( "%4d"%i_dim +" "+"%4d"%j_dim +" "+"%4d"%k_dim +" "+ "%1.16e"%grid_i[i_dim]+" "+ "%1.16e"%grid_j[j_dim]+ "%1.16e"%grid_k[k_dim])
                        for var_ind in range(nvars):
                            #fptr.write(" "+"%1.16e"%Ensemble[var+(jump*var_ind)][i_ens])
                            fptr.write(" "+"%1.16e"%Ensemble[var+var_ind , i_ens])
                        fptr.write("\n")
                        #var+=1
                        var+=nvars

        else:
            raise NotImplementedError("More than 3 dimensions will be considered soon")


        fptr.close()




def read_analysis_ensemble_states(Model_Object=None , nens=None , file_prefix = 'ensemble_mem',out_dir = 'ensemble_out/', output=False):
    """
    Read all ensemble members from out_dir and return an array with each member as a column
    """


    if not(out_dir.endswith('/')):
        out_dir = out_dir+'/'

    if not(os.path.exists(out_dir)):
        raise IOError('Ensemble results are not in the right path!!!')


    # get the state size to initialize ensemble array
    if Model_Object is None:
        # infer the state size from the first ensemble.
        try:
            # try the first ensemble file
            i_ens = 0
            filename = file_prefix+'_'+str(i_ens)+'.dat'
            tmp_member = read_model_txt_ouput( file_name = filename , model_tmp_dir = out_dir )
            state_size = tmp_member.size
            if output:
                print 'State size = ', state_size


        except:
            raise IOError("Output file: ["+filename+"] is not found in directory["+out_dir+"]")

    else:
        state_size = Model_Object._state_size


    # get the ensemble size to initialize ensemble array
    if nens is None:
        # infer the state size from the first ensemble.
        i_ens = 0
        nens = 0
        filename = file_prefix+'_'+str(i_ens)+'.dat'
        while os.path.isfile(filename):
            nens+=1
            i_ens+=1
            filename = file_prefix+'_'+str(i_ens)+'.dat'

        if output:
            print 'ensemble size = ', nens


    Ensemble = np.zeros( (state_size , nens) )

    for i_ens in range(nens):
        # read the state in column i_ens
        filename = file_prefix+'_'+str(i_ens)+'.dat'
        member = read_model_txt_ouput( file_name = filename , model_tmp_dir = out_dir )
        Ensemble[:,i_ens] = member[:]


    return Ensemble



def evaluate_statistic_of_ensemble_states(statistic,                      \
                                          Model_Object=None,              \
                                          nens=None,                      \
                                          file_prefix = 'ensemble_mem',   \
                                          out_dir = 'ensemble_out/',      \
                                          output=False,                   \
                                          ):
    """
    evaluate a sample statistic obtained from the ensemble.
    """

    if not(out_dir.endswith('/')):
        out_dir = out_dir+'/'

    if not(os.path.exists(out_dir)):
        raise IOError('Ensemble results are not in the right path!!!')


    # get the state size to initialize ensemble array
    if Model_Object is None:
        # infer the state size from the first ensemble.
        try:
            # try the first ensemble file
            i_ens = 0
            filename = file_prefix+'_'+str(i_ens)+'.dat'
            tmp_member = read_model_txt_ouput( file_name = filename , model_tmp_dir = out_dir )
            state_size = tmp_member.size
            if output:
                print 'State size = ', state_size


        except:
            raise IOError("Output file: ["+filename+"] is not found in directory["+out_dir+"]")

    else:
        state_size = Model_Object._state_size


    # get the ensemble size to initialize ensemble array
    if nens is None:
        # infer the state size from the first ensemble.
        i_ens = 0
        nens = 0
        filename = file_prefix+'_'+str(i_ens)+'.dat'
        while os.path.isfile(filename):
            nens+=1
            i_ens+=1
            filename = file_prefix+'_'+str(i_ens)+'.dat'

        if output:
            print 'ensemble size = ', nens




    # read ensemble members and evaluate the statistic requested:
    if statistic.lower() in ['mean','average']:
        # Evaluate the ensemble mean
        statistic_val = np.zeros(state_size , dtype=np.float64)

        for i_ens in range(nens):
            # read the state in column i_ens
            filename = file_prefix+'_'+str(i_ens)+'.dat'
            ensemble_member = read_model_txt_ouput( file_name = filename , model_tmp_dir = out_dir )
            statistic_val += ensemble_member
        statistic_val /= float(nens)

    elif statistic.lower() in ['variances','vars']:
        # Evaluate the variances (only diagonal of covariance matrix)

        statistic_val = np.zeros(state_size , dtype=np.float64)

        # find the mean of the ensemble:
        ensemble_mean = evaluate_statistic_of_ensemble_states(statistic    = 'mean',          \
                                                              Model_Object = Model_Object,    \
                                                              nens         = nens,            \
                                                              file_prefix  = file_prefix,     \
                                                              out_dir      = out_dir,         \
                                                              output       = output,          \
                                                              )

        for i_ens in range(nens):
            # read the state in column i_ens
            filename = file_prefix+'_'+str(i_ens)+'.dat'
            ensemble_member = read_model_txt_ouput( file_name = filename , model_tmp_dir = out_dir )
            statistic_val += (ensemble_member-ensemble_mean)**2

        statistic_val /= (nens-1)

    elif statistic.lower() in ['covariance','covar','covariances','covars']:
        # Evaluate the covariance matrix

        # try:
        #     use_sparse = Model_Object._Use_Sparse
        # except:
        #     use_sparse = False

        statistic_val = np.zeros((state_size,state_size) , dtype=np.float64)

        # find the mean of the ensemble:
        ensemble_mean = evaluate_statistic_of_ensemble_states(statistic    = 'mean',          \
                                                              Model_Object = Model_Object,    \
                                                              nens         = nens,            \
                                                              file_prefix  = file_prefix,     \
                                                              out_dir      = out_dir,         \
                                                              output       = output,          \
                                                              )

        perturbations = np.zeros((state_size,nens) , dtype=np.float64)
        for i_ens in range(nens):
            # read the state in column i_ens
            filename = file_prefix+'_'+str(i_ens)+'.dat'
            ensemble_member = read_model_txt_ouput( file_name = filename , model_tmp_dir = out_dir )
            perturbations[:,i_ens] = ensemble_member-ensemble_mean

        statistic_val =  np.dot(perturbations , perturbations.T) /(nens-1)


        # decorrelate if the decorrelation matrix is defined and decorrelation is requested in the configurations file
        try:
            if Model_Object._decorrelate:
                statistic_val = Model_Object.decorr_dot_matrix(statistic_val)
        except:
            pass

    else:
        raise ValueError("Statistics requested is not supported. Please consider adding to _utility.evaluate_statistic_of_ensemble_states!")


    return statistic_val



def prepare_model_for_filtering(Model_Object):
    """
    overwrite configuration file to suite the filtering process. Set-up and read the grids
    """

    # read the original model configurations
    originalModelConfigs = read_original_model_configurations()
    #if originalModelConfigs.has_key('model'):
    #    model_name = originalModelConfigs['model']
    #else:
    #    model_name = None

    n_iter       = originalModelConfigs['n_iter']
    #scr_op_iter  = originalModelConfigs['screen_op_iter']
    #file_op_iter = originalModelConfigs['file_op_iter']
    # initial update of model configurations
    new_fields = { 'op_file_format'   : 'text'     ,      \
                   'op_overwrite'     : 'yes'      ,      \
                   'ip_file_type'     : 'ascii'    ,      \
                   'screen_op_iter'   :  n_iter    ,      \
                   'file_op_iter'     :  n_iter           \
                  }
    overwrite_original_model_configurations(new_fields )
    ModelConfigs = read_original_model_configurations()


    # intialize the model object
    print 'Initializing the model'
    Model_Object.model_setup(ModelConfigs)


    #Retrieve model grids and convert initial condition to portable text file.
    print 'Obtaining model grids'
    Model_Object.set_grids()


    #return model_name


def prepare_uncertainty_parameters(Model_Object):
    """
    Generate necessary uncertainty parameters: e.g error covariance matrices...
    """


    # Use the reference initial condition to create a modeled background error covariance matrix
    print "Creating background error covariance matrix..."
    Model_Object.construct_background_Error_Cov( )

    # Construct the observation operator
    Model_Object.construct_obs_oper( )

    # Construct an observaiton error covariance matrix
    Model_Object.construct_observation_Error_Cov()



def create_initial_ensemble(Model_Object , Filter_Object):
    """
    create initial forecast state and initial ensemble
    """
    # Perturb initial condition  --> initial forecast
    print "Generating forecast initial condition..."
    frcst_initial_condition  = Model_Object.generate_initialForecast()

    # Create initial ensemble
    print "Generating an Initial Ensemble..."
    Filter_Object.create_Initil_Ensemble(Model_Object , frcst_initial_condition )




def try_file_name(directory,file_prefix,extension):
    """
    Try to find a suitable file name file_prefix_<number>.<extension>
    """
    success = False
    cntr = 0
    while not success:
        file_name = file_prefix+'_'+str(cntr)+'.'+extension.strip('.')

        if not (os.path.isfile(directory+file_name) ):
            #print file_name, 'not found!'
            success = True
            break
        else:
            #print file_name, 'found!'
            pass
        cntr+=1

    return file_name




def clean_executables():
    """
    remove executable files
    """
    cwd = os.getcwd()
    os.chdir(DA_Pack_RootPATH)
    os.system("find -name '*.pyc' -delete")
    os.chdir(DA_Pack_PATH)
    os.system("find -name '*.o' -delete")
    os.chdir(cwd)





#==============================================================================================#
#     Set of functions used to build a Gaussian Mixture Model (GMM) for a given ensemble.      #
#==============================================================================================#
#
def clustering_process( Forecast_Ensemble                  = None,    \
                        nens                               = None,    \
                        clustering_model                   = 'gmm',   \
                        cov_type                           = None,    \
                        inf_criteria                       = 'aic',   \
                        number_of_components               = None,    \
                        min_number_of_components           = None,    \
                        max_number_of_components           = None,    \
                        min_number_of_points_per_component = 1,       \
                        invert_uncertainty_param           = False    \
                      ):
    """
    Gaussian mixture model fitting to an ensemble of states.
    Part of ongoing research in collaboration with Adrian Sandu and Azam Moosavi.
    Ahmed Attia (attia@vt.edu)
    CSL@CS.VT.EDU
    2015
    """
    # Forecast_Ensemble = np.asrray(Forecast_Ensemble,dtype = np.float64)

    if Forecast_Ensemble is None:
        # Read the forecast ensemble
        Forecast_Ensemble = ( read_analysis_ensemble_states( nens=nens , file_prefix = 'ensemble_mem',out_dir = 'ensemble_out/') ).T

    if nens is None:
        nens = Forecast_Ensemble.shape[0]


    # default maximum number of mixture components (half the ensemble size) if it is not passed as argument
    if max_number_of_components is None or max_number_of_components > nens:
        max_number_of_components = np.int(nens/2)


    #
    clustering_model = clustering_model.lower()

    print (" Clustering Process of %4d ensemble members statrted..." %nens ) ,
    sys.stdout.flush()
    print ('\r'+' '.rjust(50)+'\r'),

    #
    if number_of_components is None:
        #
        if min_number_of_components is None:
            min_number_of_components = 1

        #  information criteria (aic,bic). value: {the lower the better}
        optimal_inf_criterion = np.infty

        # Loop over all number of components to find the one with optimal information criterion value
        for tmp_num_comp in range(min_number_of_components, max_number_of_components):
            #
            # #cluster and return the cluster model and the number of components.
            # we can either save all or just add one more clustering step for optimal number of components (storage vs speed).
            #
            if clustering_model =='gmm':
                temp_Model, temp_criterion_value, temp_covar_type = GMM_clustering(Forecast_Ensemble, tmp_num_comp , cov_type, inf_criteria)

            elif clustering_model in ['vbgmm' , 'vb_gmm']:
                temp_Model , temp_criterion_value , temp_covar_type = VBGMM_clustering(Forecast_Ensemble, tmp_num_comp, cov_type, inf_criteria)

            else:
                raise ValueError("\n\tclustering algorithm ["+clustering_model+"] is not implemented!")

            # compare criterion to previously evaluated
            if temp_criterion_value < optimal_inf_criterion:
                optimal_inf_criterion = temp_criterion_value
                optimal_Model = temp_Model
                optimal_covar_type = temp_covar_type


        # Prediction to find the lables and hyperparameters of the GM model
        Converged   = optimal_Model.converged_
        Lables      = optimal_Model.predict(Forecast_Ensemble)  # labels start at  'zero'
        Weights     = np.round(optimal_Model.weights_,3)
        Means       = optimal_Model.means_.T

        if clustering_model == 'gmm':
            Covariances = optimal_Model.covars_
            Precisions  = None
        elif clustering_model in ['vbgmm','vb_gmm']:
            Covariances = None
            Precisions  = optimal_Model.precs_
        else:
            # shouldn't be reached at all
            raise ValueError("\n\tclustering algorithm ["+clustering_model+"] is not implemented!")


        # optionally evaluate the precisions array(s)
        #TODO


    else:
        # no loop for finiding information criteria
        #

        if clustering_model =='gmm':
            optimal_Model , optimal_inf_criterion , temp_covar_type = GMM_clustering(Forecast_Ensemble, number_of_components, cov_type, inf_criteria)

        elif clustering_model=='vbgmm':
            optimal_Model , optimal_inf_criterion , temp_covar_type = VBGMM_clustering(Forecast_Ensemble, number_of_components, cov_type, inf_criteria)

        else:
            raise ValueError("\n\tclustering algorithm ["+clustering_model+"] is not implemented!")


        # Prediction to find the lables and hyperparameters of the GM model
        Converged   = optimal_Model.converged_
        Lables      = optimal_Model.predict(Forecast_Ensemble)  # labels start at  'zero'
        Weights     = np.round(optimal_Model.weights_,3)
        Means       = optimal_Model.means_.T
        optimal_covar_type = temp_covar_type

        if clustering_model == 'gmm':
            Covariances = optimal_Model.covars_
            Precisions = None
            #
        elif clustering_model in ['vbgmm','vb_gmm']:
            Precisions  = optimal_Model.precs_
            Covariances = None
            #
        else:
            # shouldn't be reached at all
            raise ValueError("\n\tclustering algorithm ["+clustering_model+"] is not implemented!")


        # Optionally invert the Covariance/Precision array(s):
        if invert_uncertainty_param:
            #
            if Precisions is None and Covariances is not None:
                #
                if optimal_covar_type in ['diag' , 'spherical']:
                    Precisions = 1/Covariances
                elif optimal_covar_type =='tied':
                    Precisions = None
                elif optimal_covar_type == 'full':
                    Precisions = None
                else:
                    # shouldn't be reached at all
                    raise ValueError("\n\tclustering algorithm ["+clustering_model+"] is not implemented!")
                #
            elif Covariances is None and Precisions is not None:
                #
                if optimal_covar_type in ['diag' , 'spherical']:
                    Covariances = 1/Precisions
                elif optimal_covar_type =='tied':
                    Precisions = None
                elif optimal_covar_type == 'full':
                    Precisions = None
                else:
                    # shouldn't be reached at all
                    raise ValueError("\n\tclustering algorithm ["+clustering_model+"] is not implemented!")
                #
            else:
                raise ValueError("Unexpected Error caused by Precisions and Covariances. Seems both are None or both are known!")


    print (" Clustering Process of [%4d] ensemble members Finished. Clusters found: [ %4d ]" %(nens , np.max(Lables)+1 ) ),
    sys.stdout.flush()
    print ('\r'+' '.rjust(60)+'\r'),


    return Converged, Lables, Weights, Means, Covariances, Precisions, optimal_covar_type

#



def GMM_clustering(Ensemble, num_comp , covariance_type, inf_criteria ):
    """
    Standard Gaussian Mixture model with EM
    """
    #
    inf_criteria = inf_criteria.lower()

    if covariance_type is not None:

        # one model
        #
        gmm_model = GMM_Model(n_components=num_comp , covariance_type=covariance_type)
        gmm_model.fit(Ensemble)
        #
        if inf_criteria =='aic':
            opt_inf_criterion = gmm_model.aic(Ensemble)
        elif inf_criteria == 'bic':
            opt_inf_criterion=gmm_model.bic(Ensemble)
        else:
            raise ValueError("\n\t Information Criterion passed is not recognized! AIC or BIC only are accepted, recieved ["+inf_criteria+"]!")

        optimal_covar_type = covariance_type


    else:
        covariance_types = {'diag', 'spherical', 'tied', 'full'}
        opt_inf_criterion = np.infty

        # best model by comparing several covariance types:
        for cov_type in covariance_types:
            #
            temp_gmm_model = GMM_Model(n_components=num_comp, covariance_type=cov_type)
            temp_gmm_model.fit(Ensemble)
            #
            if inf_criteria=='aic':
                temp_inf_criterion = temp_gmm_model.aic(Ensemble)
            elif inf_criteria.lower()=='bic':
                temp_inf_criterion=temp_gmm_model.bic(Ensemble)
            else:
                raise ValueError("\n\t Information Criterion passed is not recognized! AIC or BIC only are accepted, recieved ["+inf_criteria+"]!")

            if temp_inf_criterion < opt_inf_criterion:
                opt_inf_criterion  = temp_inf_criterion
                gmm_model      = temp_gmm_model
                optimal_covar_type = cov_type

    return gmm_model , opt_inf_criterion , optimal_covar_type
#




def VBGMM_clustering(Ensemble, num_comp , covariance_type, inf_criteria ,  \
                     alpha=1.0, random_state=None, thresh=None, tol=0.001, \
                     verbose=False, min_covar=None, n_iter=10,             \
                     params='wmc', init_params='wmc'                       \
                    ):
    """
    Variational Gaussian Mixture model with EM
    """
    #
    inf_criteria = inf_criteria.lower()

    if covariance_type is not None:

        # one model
        #
        gmm_model = VBGMM_Model(n_components=num_comp , covariance_type=covariance_type , alpha=alpha, \
                                random_state=random_state, thresh=thresh, tol=tol, verbose=verbose,    \
                                min_covar=min_covar, n_iter=n_iter, params=params, init_params=init_params)
        gmm_model.fit(Ensemble)
        #
        if inf_criteria =='aic':
            opt_inf_criterion = gmm_model.aic(Ensemble)
        elif inf_criteria == 'bic':
            opt_inf_criterion = gmm_model.bic(Ensemble)
        else:
            raise ValueError("\n\t Information Criterion passed is not recognized! AIC or BIC only are accepted, recieved ["+inf_criteria+"]!")

        optimal_covar_type = covariance_type


    else:
        covariance_types = {'diag', 'spherical', 'tied', 'full'}
        opt_inf_criterion = np.infty

        # best model by comparing several covariance types:
        for cov_type in covariance_types:
            #
            temp_gmm_model = VBGMM_Model(n_components=num_comp , covariance_type=cov_type , alpha=alpha, \
                                random_state=random_state, thresh=thresh, tol=tol, verbose=verbose,    \
                                min_covar=min_covar, n_iter=n_iter, params=params, init_params=init_params)

            temp_gmm_model.fit(Ensemble)
            #
            if inf_criteria=='aic':
                temp_inf_criterion = temp_gmm_model.aic(Ensemble)
            elif inf_criteria.lower()=='bic':
                temp_inf_criterion = temp_gmm_model.bic(Ensemble)
            else:
                raise ValueError("\n\t Information Criterion passed is not recognized! AIC or BIC only are accepted, recieved ["+inf_criteria+"]!")

            if temp_inf_criterion < opt_inf_criterion:
                opt_inf_criterion  = temp_inf_criterion
                gmm_model      = temp_gmm_model
                optimal_covar_type = cov_type

    return gmm_model , opt_inf_criterion , optimal_covar_type
#
#==============================================================================================#
#                                      GMM functions done.                                     #
#==============================================================================================#




# Validate gradient of a scalar function

def Validate_gradient(state , gradient , func , *func_params, **fd_type_and_output ):
    #----------------------------Keep for debugging----------------------------<
    # Complex-step FD are more accurate but will cause pain through other modules!
    # Also, complex-step may be problematic with time integrators in other packages such as HyPar...
    # Validate the gradient:

    # default values of un-named keyword args, issues o avoid portability ito older versions.
    if fd_type_and_output.has_key('FD_type'):
        FD_type = fd_type_and_output['FD_type']
    else:
        FD_type = 'centeral'

    if fd_type_and_output.has_key('screen_output'):
        screen_output = fd_type_and_output['screen_output']
    else:
        screen_output = False

    # Initialize finite difference approximation gradient and relative error vector:
    Grad_FD = np.zeros(state.size , dtype=np.float64)
    Rel_ERR = np.zeros(state.size , dtype=np.float64)

    eps = 1e-5

    if FD_type == 'left':
        # left finite difference approximation
        #
        F_1 = func( state , *func_params )
        #
        for i in range(state.size):
            X_2   = state.copy()
            X_2[i] = state[i] - eps
            F_2 = func( X_2 , *func_params )

            Grad_FD[i] = (F_1 - F_2)/(eps)
            Rel_ERR[i] = (gradient[i]-Grad_FD[i])/Grad_FD[i]

            if screen_output:
                print ( ' i=[%4d]:    Gradient=%12.9e;    |    FD-Grad=%12.9e;    |    Rel-Err =%12.9e ' %(i , gradient[i] , Grad_FD[i] , Rel_ERR[i] ) )

    elif FD_type =='right':
        # right finite difference approximation
        #
        F_1 = func( state , *func_params )
        #
        for i in range(state.size):
            X_2   = state.copy()
            X_2[i] = state[i] + eps
            F_2 = func( X_2 , *func_params )

            Grad_FD[i] = (F_2-F_1)/(eps)
            Rel_ERR[i] = (gradient[i]-Grad_FD[i])/Grad_FD[i]

            if screen_output:
                print ( ' i=[%4d]:    Gradient=%12.9e;    |    FD-Grad=%12.9e;    |    Rel-Err =%12.9e ' %(i , gradient[i] , Grad_FD[i] , Rel_ERR[i] ) )

    elif FD_type == 'central':
        #
        # for i in range(state.size):
        for i in range(10):
            X_1   = state.copy()
            X_2   = state.copy()
            X_1[i] = state[i] - eps
            X_2[i] = state[i] + eps
            F_1 = func( X_1 , *func_params )
            F_2 = func( X_2 , *func_params )

            Grad_FD[i] = (F_2-F_1)/(2*eps)
            Rel_ERR[i] = (gradient[i]-Grad_FD[i])/Grad_FD[i]

            if screen_output:
                print ( ' i=[%4d]:    Gradient=%+12.9e;    |    FD-Grad=%+12.9e;    |    Rel-Err =%+12.9e ' %(i , gradient[i] , Grad_FD[i] , Rel_ERR[i] ) )

        # FUNC_Params:: model_Object, forecast_state, observation, HMC_Options

    elif FD_type in ['complex','complex_step','complex-step']:
        raise NotImplementedError("Complex-step finite difference approximation of derivatives is not currently supported!")
    else:
        raise ValueError("Finite difference Approximation strategy ["+FD_type+"] is unrecognized or not supported!")

    #
    #----------------------------Keep for debugging----------------------------<




    return Grad_FD , Rel_ERR

