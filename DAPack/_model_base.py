
class ModelsBase:
    """
    Abstract class for DAPack(Py-DA*) models. Check :mod:`_model_base_HyPar` for example.
    """
    def __init__(self):
        self._setup=False

    def model_setup(self):
        raise NotImplementedError


    def set_grids(self):
        raise NotImplementedError


  
    
    def step_forward_function(self,state,time):
        raise NotImplementedError

    def step_dforward_dx_function(self,state,time):
        raise NotImplementedError
        



    def step_forward(self,state):
        raise NotImplementedError

    def step_dforward_dx(self,state_dx):
        raise NotImplementedError

        


    def obs_oper(self,dummy):
        raise NotImplementedError
        
    
    def obs_doper_dx(self):
        raise NotImplementedError

