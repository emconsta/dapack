import os
import _utility

import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
from matplotlib.ticker import MultipleLocator
from mpl_toolkits.axes_grid1 import make_axes_locatable


class DA_Visualization:
    """
    A class containing visualization tools useful for plotting data assimilation results.
    This implementation is old. New design is being written to be easily updated.
    """


    def plot_states_trajectories(self, Model_Object=None , Filter_Object=None ,                 \
                                 plot_type='lines' ,vars_to_plot=None,                          \
                                 animated_plot=False,                                           \
                                 max_num_plots=None , read_from_files=True,                     \
                                 create_subplots=True, analysis_statistic="mean",               \
                                 plot_observed_states=False ,Font_Size=14, plt_h_spacing=0.0,   \
                                 results_directory = "assimilation_results/",                   \
                                 grids_file_name='Grids.dat',                                   \
                                 observed_vars_file_name = "Observed_Vars.dat",                 \
                                 figure_width =14,figure_height = 24,repeat_animation=False,    \
                                 show_fig = False                                               \
                                ):
        """
        plot trajectory of states along filtering timespan
        vars_to_plot : an array of indexes of entries of the state vector to be plotted
        """

        if (Model_Object is None or Filter_Object is None) and not read_from_files:
            raise ValueError("Proper model and filter objects have to be passed you don't want to read from files!")


        if not (Model_Object is None):
            if not(Filter_Object._File_Output):
                raise UserWarning("Results directory exists, however filter configurations says no file output!")


        # get necessary variables from configurations files or objects if passed
        if read_from_files:
            print("Reading model and filter information...")
            # read model and filter configurations:
            model_configs = _utility.read_original_model_configurations()
            _ndims = model_configs['ndims']
            _nvars = model_configs['nvars']
            _size  = model_configs['size']
            _model_name = model_configs['model']
            _state_size = np.prod(_size) * _nvars
            _vars_observed = np.loadtxt(results_directory+observed_vars_file_name)

            with open(results_directory+grids_file_name , 'r') as fptr:
                grids_txt = fptr.readlines()
            _Grids_dict = {}
            for dim in range(_ndims):
                grid_arr = np.array([ np.float(i) for i in grids_txt[dim].split() ] )
                _Grids_dict.update( { str(dim) : grid_arr} )
            #
            filter_configs = _utility.read_filter_configurations()
            _DA_Filter_Name = filter_configs['filter_name']
            _File_Output_Iter = filter_configs['file_output_iter']
            _File_Output_Means_Only = filter_configs['file_output_means_only']

            _T_Initial = filter_configs['initial_time']
            _T_Final = filter_configs['final_time']
            _Cycle_Length = filter_configs['cycle_length']
            _Filter_Time_Span     = _utility.linspace( _T_Initial , _T_Final , _Cycle_Length )
            _Filter_Num_of_Steps  = np.size(_Filter_Time_Span)-1
        else:
            # get necessary information from the filter and model objects

            _ndims = Model_Object._ndims
            _nvars = Model_Object._nvars
            _model_name = Model_Object._model_name
            _state_size = Model_Object._state_size
            _vars_observed = Model_Object._vars_observed
            _Grids_dict = Model_Object._Grids_dict
            #
            _DA_Filter_Name = Filter_Object._DA_Filter_Name
            _File_Output_Iter = Filter_Object._File_Output_Iter
            _File_Output_Means_Only = Filter_Object._File_Output_Means_Only
            _Filter_Num_of_Steps  = Filter_Object._Filter_Num_of_Steps




        # time indexes at which files are saved
        #print _Filter_Num_of_Steps
        #print _File_Output_Iter
        output_indexes = np.arange(0, _Filter_Num_of_Steps+1 , _File_Output_Iter)


        filter_name = _DA_Filter_Name

        # chose variables to plot
        state_size =  _state_size
        if vars_to_plot is None:
            # plot all variables
            vars_to_plot = np.arange(state_size)
            num_vars_to_plot = state_size
        else:
            vars_to_plot = np.array(vars_to_plot) # unify data structur provided
            num_vars_to_plot = vars_to_plot.size

        if not max_num_plots is None:
            if num_vars_to_plot>max_num_plots:
                vars_to_plot = vars_to_plot[:max_num_plots]
                num_vars_to_plot = max_num_plots



        # Obtain trajectories and timespans
        if read_from_files:
            print("Reading results from files...")
            # check the results folder named "assimilation_results" and attempt to read results from files

            if not(os.path.isdir(results_directory)):
                raise IOError("Cannot find results redurectory!")

            # obtaind trajectories from the saved files: check if only means are saved!
            # *- filtering times
            try:
                #index and assimilation time points are written in two columns: not needed now..
                file_name = "Filtering_Times.dat"
                out_txt = np.genfromtxt(results_directory+file_name )
                Assimilation_Tspan = out_txt[:,1]
            except:
                raise IOError("please check the file: "+results_directory+file_name+". Either the file is missing or format is not recognized!")

            # 1- Reference Trajectory:
            try:
                # The reference states at the requested assimilation instances are written
                # Empty lines will be printed between consecutive states.
                # Each row starts with index then the assimilation time then the reference state
                file_name = "Reference_States.dat"
                out_txt = np.genfromtxt(results_directory+file_name )
                Reference_Tspan = out_txt[:,1]
                Reference_Trajectory = (out_txt[:,2:])[:,vars_to_plot].T # now each state is saved in a column

            except:
                raise IOError("please check the file: "+results_directory+file_name+". Either the file is missing or format is not recognized!")


            # 2- No-Assimilation Trajectory:
            try:
                # The reference states at the requested assimilation instances are written
                # Empty lines will be printed between consecutive states.
                # Each row starts with index then the assimilation time then the reference state
                file_name = "NOassimilation_States.dat"
                out_txt = np.genfromtxt(results_directory+file_name )
                NoAssimilation_Tspan = out_txt[:,1]
                NoAssimilation_Trajectory = (out_txt[:,2:])[:,vars_to_plot].T # now each state is saved in a column

            except:
                raise IOError("please check the file: "+results_directory+file_name+". Either the file is missing or format is not recognized!")


            # 3- Observations
            try:
                # The observation at the requested assimilation instances are written
                # Empty lines will be printed between consecutive states.
                # Each row starts with index then the assimilation time then the observation vector
                file_name = "Observations.dat"
                out_txt = np.genfromtxt(results_directory+file_name )
                Observed_Variables =  _vars_observed # to plot with corresponding variables only
                Observations_Tspan = out_txt[:,1]
                Observations_Trajectory = out_txt[:,2:].T # now each state is saved in a column

            except:
                raise IOError("please check the file: "+results_directory+file_name+". Either the file is missing or format is not recognized!")



            # Read forecast trajectory:
            if( _File_Output_Means_Only):
                # 3- Read forecast states:
                try:
                    # The analysis means at the requested assimilation instances are written
                    # each row starts with index then the assimilation time then the analysis ensemble mean
                    file_name = "Analysis_Means.dat"
                    out_txt = np.genfromtxt(results_directory+file_name )
                    Analysis_Tspan = out_txt[:,1]
                    Analysis_Trajectory = (out_txt[:,2:])[:,vars_to_plot].T # now each state is saved in a column
                except:
                    raise IOError("please check the file: "+results_directory+file_name+". Either the file is missing or format is not recognized!")

                # 4- Read analysis trajectory:
                try:
                    # The analysis means at the requested assimilation instances are written
                    # each row starts with index then the assimilation time then the analysis ensemble mean
                    file_name = "Forecast_Means.dat"
                    out_txt = np.genfromtxt(results_directory+file_name )
                    Forecast_Tspan = out_txt[:,1]
                    Forecast_Trajectory = (out_txt[:,2:])[:,vars_to_plot].T # now each state is saved in a column
                except:
                    raise IOError("please check the file: "+results_directory+file_name+". Either the file is missing or format is not recognized!")



            else:
                # All ensembles are saved.
                # based on the analysis_statistic, find an analysis state to plot

                # get time span of analysis and forecasting:
                file_name = "Filtering_Times.dat"
                out_txt = np.genfromtxt(results_directory+file_name )
                Forecast_Tspan = out_txt[:,1]
                Analysis_Tspan = Forecast_Tspan


                # read analysis and forecast trajectories and calculate a representative analysis state..
                # initialize trajectory placeholders:
                Forecast_Trajectory = np.empty((state_size , output_indexes.size))
                Analysis_Trajectory = np.empty((state_size , output_indexes.size))
                #
                for time_ind in output_indexes:
                    #
                    if analysis_statistic.lower() in ['average','mean','mve']:
                        #
                        file_name = "Forecast_Ensemble_"+str(time_ind)+".dat"
                        out_txt = np.genfromtxt(results_directory+file_name )
                        Forecast_Trajectory[:,time_ind] = np.mean(out_txt,1)
                        #
                        file_name = "Analysis_Ensemble_"+str(time_ind)+".dat"
                        out_txt = np.genfromtxt(results_directory+file_name )
                        Analysis_Trajectory[:,time_ind] = np.mean(out_txt,1)

                    else:
                        raise ValueError("analysis statistic "+analysis_statistic+" is not implemented!")


        else:
            # use results saved in the filter object
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            # Plot results. Reference trajectory, forecast trajectory and observations.
            # this is specific to this model.
            #
            Reference_Trajectory      =  Filter_Object._referenceTrajectory
            NoAssimilation_Trajectory =  Filter_Object._NOassimilationTrajectory
            Analysis_Trajectory       =  Filter_Object._analysisMeans
            Forecast_Trajectory       =  Filter_Object._forecastTrajectory
            Observations_Trajectory   =  Filter_Object._Observations

            Reference_Tspan      =  Filter_Object._Filter_Time_Span
            NoAssimilation_Tspan =  Filter_Object._Filter_Time_Span
            Analysis_Tspan       =  Filter_Object._Filter_Time_Span
            Forecast_Tspan       =  Filter_Object._Filter_Time_Span
            Observations_Tspan   =  Filter_Object._Observation_Time_Span



        print 'Trajectories are loaded...'
        if animated_plot:
            # a single plot each of the trajectories, being animated for the time points in the assimilation time span
            if plot_type in ['line', 'lines','line_plot']:
                # animate states in a single plot
                raise NotImplementedError("Not reasonable to plot line plots of all variables animated at assimilation time!")

            elif plot_type in ['contour','contours','contour_plot']:
                # plot contours of the reference solution, the forecast the analysis and possibly observations, all animated over assimilation timepoints
                #
                if  _ndims<2:
                    raise ValueError("Contour Plots are valid only for at least two dimensional models!")

                # create grid of the state vector
                X_grid = np.array( _Grids_dict['0'])
                Y_grid = np.array( _Grids_dict['1'])
                X_grid , Y_grid = np.meshgrid(X_grid , Y_grid)
                grid_shape = X_grid.shape

                # create observational grid. This should be designed at the time observation is created...
                #X_obs_grid =
                #X_obs_grid =
                #X_obs_grid,Y_obs_grid = np.meshgrid(X_obs_grid,Y_obs_grid)
                #obs_grid_shape =

                if  _ndims==2:
                    #
                    font = {'weight' : 'bold', 'size' : Font_Size}
                    #
                    # contour plot of 2D models:
                    # construct the mesh:
                    X_grid = np.array( _Grids_dict['0'])
                    Y_grid = np.array( _Grids_dict['1'])
                    X_grid , Y_grid = np.meshgrid(X_grid , Y_grid)
                    grid_shape = X_grid.shape

                    print 'Plotting 2D model results with animation enabled'
                    # Will use add_subplot this time . Just playing :)

                    if plot_observed_states:
                        rows = 5
                        cols = _nvars
                    else:
                        rows = 4
                        cols = _nvars


                    outfig = plt.figure(11100002)
                    # rows are for the forecast, the analysis,... . columns will include the prognostic variables
                    fig , ax_arr = plt.subplots(rows, cols, sharex=True, sharey=True , figsize=(figure_height , figure_width ) )
                    #fig , ax_arr = plt.subplots(rows, cols , figsize=(figure_height , figure_width ) )
                    #fig , ax_arr = plt.subplots(rows, cols , figsize=(figure_height , figure_width ) )
                    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0)) # apply scientific text style
                    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0)) # apply scientific text style
                    #plt.ticklabel_format(style='sci') # apply scientific text style
                    plt.rc('font', **font)

                    #cax = fig.add_axes([0.9, 0.1, 0.03, 0.8])



                    plt.subplots_adjust(hspace = plt_h_spacing)
                    plt.subplots_adjust(wspace = plt_h_spacing)
                    #plt.tight_layout()
                    #
                    frs  = []


                    for var_ind in range(_nvars):
                        Reference_States      = Reference_Trajectory[var_ind:: _nvars, :]
                        NoAssimilation_States = NoAssimilation_Trajectory[var_ind:: _nvars, :]
                        Analysis_States       = Analysis_Trajectory[var_ind:: _nvars, :]
                        Forecast_States       = Forecast_Trajectory[var_ind:: _nvars, :]

                        pl_0 = ax_arr[0][var_ind].contourf(X_grid , Y_grid ,  Reference_States[:,0].reshape(grid_shape)      )
                        #ax_arr[0][var_ind].set_xlabel('$X$')
                        #ax_arr[0][var_ind].set_ylabel('$Y$')
                        ax_arr[0][var_ind].set_xticklabels([])

                        if var_ind>0:
                            ax_arr[0][var_ind].set_yticklabels([])
                            #ax_arr[0][var_ind].set_title("$U_{"+str(var_ind)+"}$")
                        elif var_ind ==0:
                            ax_arr[0][var_ind].set_ylabel('Reference')
                            #ax_arr[0][var_ind].set_title("Reference", rotation='vertical')
                        ## Make a colorbar for the ContourSet returned by the contourf call.
                        #cbar = plt.colorbar(pl_0 )

                        pl_1 = ax_arr[1][var_ind].contourf( X_grid , Y_grid ,  NoAssimilation_States[:,0].reshape(grid_shape) )
                        #ax_arr[1][var_ind].set_xlabel('$X$')
                        #ax_arr[1][var_ind].set_ylabel('$Y$')
                        ax_arr[1][var_ind].set_xticklabels([])
                        if var_ind ==0:
                            ax_arr[1][var_ind].set_ylabel('No Assimilation')
                            #ax_arr[1][var_ind].set_title("No Assimilatin", rotation='vertical' )
                        else:
                            ax_arr[1][var_ind].set_xticklabels([])
                            ax_arr[1][var_ind].set_yticklabels([])
                        ## Make a colorbar for the ContourSet returned by the contourf call.
                        #cbar = plt.colorbar(pl_1)

                        pl_2 = ax_arr[2][var_ind].contourf( X_grid , Y_grid ,  Forecast_States[:,0].reshape(grid_shape) )
                        #ax_arr[2][var_ind].set_xlabel('$X$')
                        #ax_arr[2][var_ind].set_ylabel('$Y$')
                        ax_arr[2][var_ind].set_xticklabels([])
                        if var_ind ==0:
                            ax_arr[2][var_ind].set_ylabel('Forecast')
                            #ax_arr[2][var_ind].set_title("Forecast", rotation='vertical' )
                        else:
                            ax_arr[2][var_ind].set_xticklabels([])
                            ax_arr[2][var_ind].set_yticklabels([])
                        ## Make a colorbar for the ContourSet returned by the contourf call.
                        #cbar = plt.colorbar(pl_2)

                        pl_3 = ax_arr[3][var_ind].contourf( X_grid , Y_grid ,  Analysis_States[:,0].reshape(grid_shape) )
                        ax_arr[3][var_ind].set_xlabel("$U_{"+str(var_ind)+"}$")
                        #ax_arr[3][var_ind].set_ylabel('$Y$')
                        if var_ind ==0:
                            ax_arr[3][var_ind].set_ylabel('Analysis')
                            #ax_arr[3][var_ind].set_title("Analysis", rotation='vertical' )
                        else:
                            ax_arr[3][var_ind].set_yticklabels([])

                        #divider = make_axes_locatable(ax_arr[3][var_ind])
                        #cax = divider.append_axes("bottom", pad=0.05)
                        #plt.colorbar(pl_3, cax=cax, ticks=MultipleLocator(0.2), format="%1.3e")
                        ## Make a colorbar for the ContourSet returned by the contourf call.
                        #cbar = plt.colorbar(pl_3)


                        if plot_observed_states:
                            #pl_4, = ax_arr[4][var_ind].contour( X_grid , Y_grid ,  Observations[:,time_ind].reshape(grid_shape) )
                            pass
                            #raise NotImplementedError("Design Observational grid first in the function: 'model.construct_obs_oper()'!")
                            #
                        frs = [pl_0 , pl_1 , pl_2 ,pl_3 ]



                    #for j in range(cols):
                    #    divider = make_axes_locatable(ax_arr[rows][j])
                    #    # Append axes to the right of ax3, with 20% width of ax3
                    #    cax = divider.append_axes("bottom", size="20%", pad=0.05)
                    #    if rows==4:
                    #        cbar = plt.colorbar(pl_3, cax=cax, ticks=MultipleLocator(0.2), format="%1.3e")
                    #    else:
                    #        #cbar = plt.colorbar(pl_4, cax=cax, ticks=MultipleLocator(0.2), format="%1.3e")
                    #        pass

                    # Make an axis for the colorbar on the right side
                    #fig.colorbar(pl_0 , cax=cax)

                    #run animation
                    #ani = animation.ArtistAnimation(fig,anims, interval=50,blit=False)

                    ani=animation.FuncAnimation(fig,contour_update,Forecast_Tspan.size , interval=1000 , \
                                                fargs=(ax_arr,rows,cols, X_grid , Y_grid , Reference_Trajectory,NoAssimilation_Trajectory,Analysis_Trajectory,Forecast_Trajectory,Observations_Trajectory,plot_observed_states ), \
                                                repeat=repeat_animation)


                    writer = animation.writers['ffmpeg'](fps=1) # set frames per second
                    dpi = 100

                    # save inside results directory:
                    file_name = results_directory + _utility.try_file_name(directory=results_directory,file_prefix='contour_plot',extension='mp4')

                    ani.save(file_name , writer=writer , dpi=dpi )

                    if show_fig:
                        # no need unless you want to wait for a while :)
                        plt.draw() # avoid showing. write then show in a movie player...
                        #plt.show()



                elif  _ndims==3:
                    raise NotImplementedError(" slicing of contour plots for 3D models is not implemented yet!")
                else:
                    raise NotImplementedError(" contour plots for dimension of this model is not implemeneted yet!")
                    #
            else:
                # add more animated versions of other types
                raise NotImplementedError("plot_type: "+plot_type+" is not implemented!")




        # Now start plotting... (Xonsider replacing by add_subplot(...) )
        elif plot_type in ['line', 'lines','line_plot']:
            # Start plotting RMSE results
            font = {'weight' : 'bold', 'size' : Font_Size}
            plt.rc('font', **font)

            if create_subplots:
                # create one figure with all variables to be plotted given in sub-figures
                #find good alignment of subplots:
                num_cols = int(np.sqrt(num_vars_to_plot))
                num_rows = num_vars_to_plot/num_cols
                if (num_vars_to_plot%num_cols)!=0:
                    num_rows+=1

                fig , ax_arr = plt.subplots(num_rows, num_cols, sharex=False, sharey=False )
                if ax_arr.size ==num_rows or ax_arr.size ==num_cols: # subplots can fit in a single row:
                    for sub_ind in range(num_vars_to_plot):
                        var_ind = vars_to_plot[sub_ind]
                        l_ref,  = ax_arr[sub_ind].plot(Reference_Tspan      , Reference_Trajectory[var_ind,:]      , 'k'   , linewidth=2 , label='Reference'       )
                        l_noas, = ax_arr[sub_ind].plot(NoAssimilation_Tspan , NoAssimilation_Trajectory[var_ind,:] , 'b--' , linewidth=2 , label='NO Assimilation' )
                        l_ana,  = ax_arr[sub_ind].plot(Analysis_Tspan       , Analysis_Trajectory[var_ind,:]       , 'c--' , linewidth=2 , label= filter_name      )
                        l_frcs, = ax_arr[sub_ind].plot(Forecast_Tspan       , Forecast_Trajectory[var_ind,:]       , 'r--' , linewidth=2 , label='Forecast'        )
                        #
                        if plot_observed_states:
                            if var_ind in Observed_Variables:
                                Obs_ind      = np.where(var_ind == Observed_Variables)[0][0]
                                Observations = Observations_Trajectory[Obs_ind,:]
                                l_obs,       = ax_arr[sub_ind].plot(Observations_Tspan      , Observations              , 'g^' , linewidth=2 , label='Observations'     )

                        # set lables
                        ax_arr[sub_ind].set_xlabel("Time" ,fontsize=Font_Size, fontweight='bold')
                        var_lbl = "$x_{"+str(var_ind)+"}$"
                        ax_arr[sub_ind].set_ylabel(var_lbl,fontsize=Font_Size, fontweight='bold')

                        ## Fine-tune figure; make subplots close to each other and hide x ticks for
                        ## all but bottom plot.
                        #fig.subplots_adjust(hspace=0)
                        #plt.setp([a.get_xticklabels() for a in f.axes[:-1]], visible=False)

                        if sub_ind ==num_vars_to_plot-1:
                            # turn-off axes of the remaining subplots:
                            for rem_ind in np.arange(sub_ind+1,num_vars_to_plot):
                                ax_arr[sub_ind][rem_ind].axis('off')
                else:

                    for sub_ind in range(num_vars_to_plot):
                        var_ind = vars_to_plot[sub_ind]

                        i_ind = sub_ind/num_cols
                        j_ind = (sub_ind%num_cols)
                        #ax_arr[i_ind][j_ind].subplot(num_rows, num_cols, sub_ind+1)
                        l_ref,  = ax_arr[i_ind][j_ind].plot(Reference_Tspan      , Reference_Trajectory[var_ind,:]      , 'k'   , linewidth=2 , label='Reference'       )
                        l_noas, = ax_arr[i_ind][j_ind].plot(NoAssimilation_Tspan , NoAssimilation_Trajectory[var_ind,:] , 'r--' , linewidth=2 , label='NO Assimilation' )
                        l_ana,  = ax_arr[i_ind][j_ind].plot(Analysis_Tspan       , Analysis_Trajectory[var_ind,:]       , 'c--' , linewidth=2 , label= filter_name      )
                        l_frcs, = ax_arr[i_ind][j_ind].plot(Forecast_Tspan       , Forecast_Trajectory[var_ind,:]       , 'b--' , linewidth=2 , label='Forecast'        )
                        #
                        if plot_observed_states:
                            if var_ind in Observed_Variables:
                                Obs_ind      = np.where(var_ind == Observed_Variables)[0][0]
                                Observations = Observations_Trajectory[Obs_ind,:]
                                l_obs,       = ax_arr[i_ind][j_ind].plot(Observations_Tspan      , Observations              , 'g^' , linewidth=2 , label='Observations'     )

                        # set lables
                        ax_arr[i_ind][j_ind].set_xlabel("Time" ,fontsize=Font_Size, fontweight='bold')
                        var_lbl = "$x_{"+str(var_ind)+"}$"
                        ax_arr[i_ind][j_ind].set_ylabel(var_lbl,fontsize=Font_Size, fontweight='bold')

                        ## Fine-tune figure; make subplots close to each other and hide x ticks for
                        ## all but bottom plot.
                        #fig.subplots_adjust(hspace=0)
                        #plt.setp([a.get_xticklabels() for a in f.axes[:-1]], visible=False)

                        if sub_ind ==num_vars_to_plot-1:
                            # turn-off axes of the remaining subplots:
                            for rem_ind in np.arange(j_ind+1,num_cols):
                                ax_arr[i_ind][rem_ind].axis('off')

                # Show the legend.
                try:
                    fig.legend( (l_ref, l_noas, l_ana, l_frcs , l_obs), ('Reference' , 'NO Assimilation', filter_name , 'Forecast' , 'Observations') , 'lower right' )
                except:
                    fig.legend( (l_ref, l_noas, l_ana, l_frcs ),        ('Reference' , 'NO Assimilation', filter_name , 'Forecast')                  , 'lower right' )


                plt.subplots_adjust(hspace = plt_h_spacing)
                #show the plot
                plot_file = results_directory+_utility.try_file_name(results_directory,'Trajectory','pdf')
                plt.savefig(plot_file, bbox_inches='tight')

                if show_fig :
                    plt.draw()
                    #plt.show()


            else:
                # create a new figure for each variable to be plotted
                for fig_ind in range(num_vars_to_plot):
                    var_ind = vars_to_plot[fig_ind]
                    plt.figure(fig_ind)
                    plt.plot(Reference_Tspan      , Reference_Trajectory[var_ind,:]      , 'k'   , linewidth=2 , label='Reference'       )
                    plt.plot(NoAssimilation_Tspan , NoAssimilation_Trajectory[var_ind,:] , 'b--' , linewidth=2 , label='NO Assimilation' )
                    plt.plot(Analysis_Tspan       , Analysis_Trajectory[var_ind,:]       , 'c--' , linewidth=2 , label= filter_name      )
                    plt.plot(Forecast_Tspan       , Forecast_Trajectory[var_ind,:]       , 'r--' , linewidth=2 , label='Forecast'        )
                    #
                    if plot_observed_states:
                        if var_ind in Observed_Variables:
                            Obs_ind = np.where(var_ind == Observed_Variables)[0][0]
                            Observations = Observations_Trajectory[Obs_ind,:]
                            plt.plot(Observations_Tspan      , Observations              , 'g^' , linewidth=2 , label='Observations'     )

                    # set lables
                    plt.set_xlabel("Time",fontsize=Font_Size, fontweight='bold' )
                    var_lbl = "$x_"+str(var_ind)+"$"
                    plt.set_ylabel(var_lbl,fontsize=Font_Size, fontweight='bold')

                # show the legend, show the plot
                plt.legend()
                plt.draw()
                #plt.show()

        elif plot_type in ['contour','contours','contour_plot']:
            #
            #plot contours if the model is 2d or more
            #
            if  _ndims<2:
                raise ValueError("Contour Plots are valid only for at least two dimensional models!")

            # create grid of the state vector
            X_grid = np.array( _Grids_dict['0'])
            Y_grid = np.array( _Grids_dict['1'])
            X_grid , Y_grid = np.meshgrid(X_grid , Y_grid)
            grid_shape = X_grid.shape

            # create observational grid. This should be designed at the time observation is created...
            #X_obs_grid =
            #X_obs_grid =
            #X_obs_grid,Y_obs_grid = np.meshgrid(X_obs_grid,Y_obs_grid)
            #obs_grid_shape =

            if  _ndims==2:
                # contour plot of 2D models:

                #find good alignment of subplots:
                num_cols = 5    # Reference, No assimilation , analysis, forecast , observation
                num_rows = Reference_Tspan.size
                if (not max_num_plots is None) and (num_rows>max_num_plots):
                    num_rows = max_num_plots/num_cols

                # create one figure for each variable:
                fig, ax_arr = plt.subplots(num_rows, num_cols, sharex=True, sharey=True )
                plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))

                for var_ind in range( _nvars):


                    Reference_States      = Reference_Trajectory[var_ind:: _nvars, :]
                    NoAssimilation_States = NoAssimilation_Trajectory[var_ind:: _nvars, :]
                    Analysis_States       = Analysis_Trajectory[var_ind:: _nvars, :]
                    Forecast_States       = Forecast_Trajectory[var_ind:: _nvars, :]

                    for i_ind in range(num_rows):
                        l_ref  = ax_arr[i_ind][0].contourf( X_grid , Y_grid ,  Reference_States[:,i_ind].reshape(grid_shape)      )
                        ax_arr[i_ind][0].set_xlabel("$X$" ,fontsize=Font_Size, fontweight='bold')
                        ax_arr[i_ind][0].set_ylabel("$Y$" ,fontsize=Font_Size, fontweight='bold')

                        l_noas = ax_arr[i_ind][1].contourf( X_grid , Y_grid ,  NoAssimilation_States[:,i_ind].reshape(grid_shape) )
                        ax_arr[i_ind][1].set_xlabel("$X$" ,fontsize=Font_Size, fontweight='bold')
                        ax_arr[i_ind][1].set_ylabel("$Y$" ,fontsize=Font_Size, fontweight='bold')

                        l_ana  = ax_arr[i_ind][2].contourf( X_grid , Y_grid ,  Analysis_States[:,i_ind].reshape(grid_shape)       )
                        ax_arr[i_ind][2].set_xlabel("$X$" ,fontsize=Font_Size, fontweight='bold')
                        ax_arr[i_ind][2].set_ylabel("$Y$" ,fontsize=Font_Size, fontweight='bold')

                        l_frcs = ax_arr[i_ind][3].contourf( X_grid , Y_grid ,  Forecast_States[:,i_ind].reshape(grid_shape)       )
                        ax_arr[i_ind][3].set_xlabel("$X$" ,fontsize=Font_Size, fontweight='bold')
                        ax_arr[i_ind][3].set_ylabel("$Y$" ,fontsize=Font_Size, fontweight='bold')


                    #
                    if plot_observed_states:
                        pass
                        raise NotImplementedError("Design Observational grid first in the function: 'model.construct_obs_oper()'!")
                        #if var_ind in Observed_Variables:
                        #    Obs_ind      = np.where(var_ind == Observed_Variables)[0][0]
                        #    Observations = Observations_Trajectory[Obs_ind,i_ind]
                        #    l_obs,       = ax_arr[i_ind][j_ind].plot(Observations_Tspan      , Observations              , 'g^' , linewidth=2 , label='Observations'     )
                        #    ax_arr[i_ind][4].set_xlabel("$X$" ,fontsize=Font_Size, fontweight='bold')
                        #    ax_arr[i_ind][4].set_ylabel("$Y$" ,fontsize=Font_Size, fontweight='bold')
                    else:
                        ax_arr[i_ind][4].axis('off')

                    # set titles for the first row of plots
                    ax_arr[0][0].set_title("Reference")
                    ax_arr[0][1].set_title("No Assimilation")
                    ax_arr[0][2].set_title("Analysis")
                    ax_arr[0][3].set_title("Forecast")
                    ax_arr[i_ind][4].set_title("Observation")

                    ## Fine-tune figure; make subplots close to each other and hide x ticks for
                    ## all but bottom plot.
                    #fig.subplots_adjust(hspace=0)
                    #plt.setp([a.get_xticklabels() for a in f.axes[:-1]], visible=False)

                plt.subplots_adjust(hspace = plt_h_spacing)
                #show the plot
                plt.draw()



            elif  _ndims==3:
                raise NotImplementedError(" slicing of contour plots for 3D models is not implemented yet!")
            else:
                raise NotImplementedError(" contour plots for dimension of this model is not implemeneted yet!")


        elif plot_type in ['rank_histogram','rank_histograms','rank_hist']: # probably better if moved to new function
            #
            raise NotImplementedError(" Rank histograms are not implemeneted yet!")

        else:
            # add plot of other types
            raise NotImplementedError("plot_type: "+plot_type+" is not implemented!")



    #
    def contour_update(self ,                      \
                       index,                      \
                       ax_arr,                     \
                       rows,                       \
                       cols,                       \
                       X_grid,                     \
                       Y_grid,                     \
                       Reference_Trajectory,       \
                       NoAssimilation_Trajectory,  \
                       Analysis_Trajectory,        \
                       Forecast_Trajectory,        \
                       Observations,               \
                       plot_observed_states        \
                      ):
        """
        Animation function of contour plot of assimilation trajectories
        """
        grid_shape = X_grid.shape
        # clear all subplots
        for i_row in range(rows):
            for j_col in range(cols):
                ax_arr[i_row,j_col].cla()

        _nvars = cols
        for var_ind in range(cols):
            Reference_States      = Reference_Trajectory[var_ind:: _nvars, :]
            NoAssimilation_States = NoAssimilation_Trajectory[var_ind:: _nvars, :]
            Forecast_States       = Forecast_Trajectory[var_ind:: _nvars, :]
            Analysis_States       = Analysis_Trajectory[var_ind:: _nvars, :]
            #Observations          = Forecast_Trajectory[var_ind:: _nvars, :]*0

            pl_0 = ax_arr[0][var_ind].contourf(X_grid , Y_grid ,  Reference_States[:,index].reshape(grid_shape)      )
            #ax_arr[0][var_ind].set_xlabel('$X$')
            #ax_arr[0][var_ind].set_ylabel('$Y$')
            ax_arr[0][var_ind].set_xticklabels([])
            if var_ind>0:
                #ax_arr[0][var_ind].set_title("$x_{"+str(var_ind)+"}$")
                ax_arr[0][var_ind].set_yticklabels([])
            else:
                ax_arr[0][var_ind].set_ylabel('Reference')
                #ax_arr[0][var_ind].set_title("Reference", rotation='vertical' )
            ## Make a colorbar for the ContourSet returned by the contourf call.
            #cbar = plt.colorbar(pl_0)

            pl_1 = ax_arr[1][var_ind].contourf( X_grid , Y_grid ,  NoAssimilation_States[:,index].reshape(grid_shape) )
            #ax_arr[1][var_ind].set_xlabel('$X$')
            #ax_arr[1][var_ind].set_ylabel('$Y$')
            ax_arr[1][var_ind].set_xticklabels([])
            ax_arr[1][var_ind].set_yticklabels([])
            if var_ind ==0:
                ax_arr[1][var_ind].set_ylabel('No Assimilation')
                #ax_arr[1][var_ind].set_title("No Assimilation", rotation='vertical')

            ## Make a colorbar for the ContourSet returned by the contourf call.
            #cbar = plt.colorbar(pl_1)

            pl_2 = ax_arr[2][var_ind].contourf( X_grid , Y_grid ,  Forecast_States[:,index].reshape(grid_shape) )
            #ax_arr[2][var_ind].set_xlabel('$X$')
            #ax_arr[2][var_ind].set_ylabel('$Y$')
            ax_arr[2][var_ind].set_xticklabels([])
            ax_arr[2][var_ind].set_yticklabels([])
            if var_ind ==0:
                ax_arr[2][var_ind].set_ylabel('Forecast')
                #ax_arr[2][var_ind].set_title("Forecast", rotation='vertical')

            ## Make a colorbar for the ContourSet returned by the contourf call.
            #cbar = plt.colorbar(pl_2)

            pl_3 = ax_arr[3][var_ind].contourf( X_grid , Y_grid ,  Analysis_States[:,index].reshape(grid_shape) )
            #ax_arr[3][var_ind].set_xlabel('$X$')
            #ax_arr[3][var_ind].set_ylabel('$Y$')
            if var_ind ==0:
                ax_arr[3][var_ind].set_ylabel('Analysis')
                #ax_arr[3][var_ind].set_title("Analysis", rotation='vertical' )
            ## Make a colorbar for the ContourSet returned by the contourf call.
            #cbar = plt.colorbar(pl_3)

            #pl_4 = ax_arr[4][var_ind].contourf( X_grid , Y_grid ,  Observations[:,index].reshape(grid_shape) )

            frames = [pl_0 , pl_1 , pl_2 , pl_3]
            #if plot_observed_states:
                #frames.append([pl_4])
                #pl_4, = ax_arr[4][var_ind].contour( X_grid , Y_grid ,  Observations[:,time_ind].reshape(grid_shape) )
                #raise NotImplementedError("Design Observational grid first in the function: 'model.construct_obs_oper()'!")
                #

        return frames,




    def plot_RMSE_results(self, Filter_Object=None , read_from_files=True, log_scale=True  , Font_Size=14, show_fig = False , results_directory='assimilation_results/'):
        """
        plot RMSE in a linear scale or log scale in a single plot for comparison.
        """
        if Filter_Object is None and not read_from_files:
            raise ValueError(" Filter_Object CANNOT must be a valid filter object if you don't want to read from files!")

        if not (Filter_Object is None):
            if not(Filter_Object._File_Output):
                print("WARNING: results directory exists, however filter configurations says no file output!")


        # Obtain RMSE results
        if read_from_files:
            # check the results folder named "assimilation_results" that holds all analysis results according to filter configurations


            if not(os.path.isdir(results_directory)):
                raise IOError("Cannot find results directory!")


            print("Reading model and filter information...")
            # read model and filter configurations:
            model_configs = _utility.read_original_model_configurations()
            _model_name = model_configs['model']
            #
            filter_configs = _utility.read_filter_configurations()
            _DA_Filter_Name = filter_configs['filter_name']



            # obtaind RMSE results from the saved files:
            RMSE_file_name = "RMSE.dat"
            out_txt = np.genfromtxt(results_directory+RMSE_file_name)

            # calculate the state vector size
            try:
                # first column:   index
                # second column:  times
                # third column:   NO Assimilation RMSE
                # fourth column: forecast RMSE
                # fifth column: analysis RMSE
                TimeSpan            = out_txt[:,1]
                NoAssimilation_RMSE = out_txt[:,2]
                Forecast_RMSE       = out_txt[:,3]
                Analysis_RMSE       = out_txt[:,4]
            except:
                raise IOError(" Data format in file 'RMSE.dat' is not well-recognized! Please check '_utility.plot_RMSE_results()'! ")


        else:

            # calculate the state vector size
            try:
                TimeSpan            = Filter_Object._Filter_Time_Span
                NoAssimilation_RMSE = Filter_Object._NOassimilation_RMSE
                Forecast_RMSE       = Filter_Object._forecast_RMSE
                Analysis_RMSE       = Filter_Object._analysis_RMSE
                _DA_Filter_Name = Filter_Object._DA_Filter_Name

                print("Reading model and filter information...")
                # read model and filter configurations:
                model_configs = _utility.read_original_model_configurations()
                _model_name = model_configs['model']
            except:
                raise IOError("timespan or/and RMSE results are not available in the filter object. This SHOULD NOT happen!")



        # Start plotting RMSE results
        font = {'weight' : 'bold', 'size' : Font_Size}


        outfig = plt.figure(11100001)
        plt.rc('font', **font)

        # plot results


        # use log-scale on y-access if requested (default)
        if log_scale:
            plt.semilogy(TimeSpan, NoAssimilation_RMSE , 'r--' , linewidth=2 , label='No-Assimilation')
            plt.semilogy(TimeSpan, Forecast_RMSE       , 'k'   , linewidth=2 , label='Forecast')
            plt.semilogy(TimeSpan, Analysis_RMSE       , 'b-.' , linewidth=2 , label=_DA_Filter_Name)
        else:
            plt.plot(TimeSpan, NoAssimilation_RMSE , 'r--' , linewidth=2 , label='No-Assimilation')
            plt.plot(TimeSpan, Forecast_RMSE       , 'k'   , linewidth=2 , label='Forecast')
            plt.plot(TimeSpan, Analysis_RMSE       , 'b-.' , linewidth=2 , label=_DA_Filter_Name)



        # Set lables and title
        plt.xlabel('Time',fontsize=Font_Size , fontweight='bold' )
        if log_scale:
            plt.ylabel('log-RMSE',fontsize=Font_Size, fontweight='bold' )
        else:
            plt.ylabel('RMSE',fontsize=Font_Size, fontweight='bold' )
        plt.title('RMSE results for the model: %s'%_model_name)

        # show the legend, show the plot
        plt.legend()

        results_directory = "assimilation_results/"
        if not(os.path.exists(results_directory)):
            results_directory= "./"
        plot_file = results_directory+_utility.try_file_name(results_directory,'RMSE_plot','pdf')
        plt.savefig(plot_file, bbox_inches='tight')
        if show_fig :
            plt.draw()

            #plt.show()
            


    def plot_assimilation_results(self ):
        """
        """
        raise NotImplementedError("TODO:")
