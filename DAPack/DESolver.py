import numpy as np

class DESolver:
    """
    Simple time-integration schemes. This includes:
        - FE: Forward Euler.
        - RK4: Runge-Kutta
        - RK2a: Runge-Kutta
    """
    solver_set=['FE','RK2a','RK4']

    def __init__(self,f):
        self.f = lambda u, t: np.asarray(f(u,t),float)

    #def advance(self):
    #    """Advance solution one step"""
    #    raise NotImplementedError

    def set_initial_condition(self, U0):
        if(isinstance(U0,(float,int))): # scalar ODE
            self.neq = 1
        else:
            U0=np.asarray(U0)
            self.neq = U0.size
        self.U0 = U0

    def solve(self, time_points, terminate = None, solver='RK4'):
        if solver in self.solver_set:
            self.advance=eval('self.'+solver)
        else:
            raise NotImplemented
        if terminate is None:
            terminate=lambda u, t, step_no:False

        self.t = np.asarray(time_points)
        n=self.t.size

        if(self.neq==1):
            self.u=np.zeros(n) #scalar ODE
        else:
            self.u=np.zeros((n,self.neq))

        # Assume that self t[0] xorresponds to self.U0
        self.u[0]=self.U0
        
        #Time loop
        for k in range(n-1):
            self.k=k
            self.u[k+1]=self.advance()
            if(terminate(self.u, self.t,self.k+1)):
                break #terminate loop over k
        return self.u[:k+2], self.t[:k+2]
    
    def RK4(self):
        u, f, k, t = self.u, self.f, self. k, self.t
        dt=t[k+1]-t[k]
        dt2=0.5*dt
        K1=dt*f(u[k],t[k])
        K2=dt*f(u[k]+0.5*K1,t[k]+dt2)
        K3=dt*f(u[k]+0.5*K2,t[k]+dt2)
        K4=dt*f(u[k]+K3,t[k]+dt)
        u_new=u[k]+(1./6.)*(K1+2*K2+2*K3+K4)
        return u_new
    
    def RK2a(self):
        u, f, k, t = self.u, self.f, self. k, self.t
        dt=t[k+1]-t[k]
        K1=dt*f(u[k],t[k])
        K2=dt*f(u[k]+K1,t[k]+dt)
        u_new=u[k]+(1./2.)*(K1+K2)
        return u_new
    
    def FE(self):
        u, f, k, t = self.u, self.f, self. k, self.t
        dt=t[k+1]-t[k]
        u_new=u[k]+dt*f(u[k],t[k])
        return u_new

