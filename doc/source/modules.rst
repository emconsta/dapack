DAPack
======

.. toctree::
   :maxdepth: 4

   DESolver
   HyPar_DAFiltering
   setup_dapack
