DAPack(Py-DA*)-- The Data Assimilation Testing-Suite!
=====================================================
   `Data Assimilation(DA)`_ is a rich literature with tons of open questions. Once a researcher decides to test a new idea, 
   several choices have to be made such as the model and the experimental settings. 
   **DAPack** is intended to be a *work-in-progress* package for `DA`_.
   Our vision is to provide an open-source high-level language that enables DA researchers to collaborate and to help them
   avoid reinventing the wheel. We want it to be as simple as possible to add new models and `DA`_ schemes to the package.
   This package is intended to ease the process of comparing the performance of different schemes efficiently and easly.
      
      + Modeled implemented so far:
         - Two-variables pendulum (0D), 
         - Lorenz-3 (three-variables Lorenz) model (0D),
         - Lorenz-96 (fourty-variables Lorenz) model (0D),
         - All `HyPar <http://debog.github.io/codes/hypar.html>`_ models (1D,2D,3D).
          
      + Filters implemented so far:
         - Ensemble Kalman filter (EnKF):   
            - Stochastic 
            - Deterministic (square root)
         - Partice filter (PF)
            - Vanilla PF
            - Sequential Importance Resampling (SIR)
         - `Hybrid Monte-Carlo (HMC) sampling filter <http://arxiv.org/pdf/1403.7137.pdf>`_ 
         - `No-UTurn-Sampler (NUTS) <http://www.stat.columbia.edu/~gelman/research/published/nuts.pdf>`_
         - Generalized No-UTurn-Sampler (NUTS)
          

.. _Data Assimilation(DA): https://en.wikipedia.org/wiki/Data_assimilation
.. _DA: https://en.wikipedia.org/wiki/Data_assimilation




Installing DAPack
=================
   - Navigate to the directory where you want to install DAPack.
   
   - Download DAPack from git:
       >> git clone https://bitbucket.org/emconsta/dapack.git
   
   - Install HyPar:
        Follow instructions `here <http://debog.github.io/codes/hypar.html>`_
        There you can find links to download and install `PETSc <http://www.mcs.anl.gov/petsc/index.html>`_ which is optional but recommended.
   
   - Set DAPack variables:
        Navigate to <DAPack root path>/DAPack/
        Set the variables in the file setup_dapack.py
 
            - **Hypar_bin**        : Actual path of the binary file of HyPar. <HyPar root path>/bin/HyPar
  
            - **DA_Pack_RootPATH** : Actual path of the DAPacke. This is the root directory you get by cloning the repository.
  
            - **CCompiler**        : Chose the number of C compiler on your machine from the list given in the variable **CCompilers**.


Contents
=========

.. toctree::
   :maxdepth: 2
   
   Quick Start Guide <quick_start>
   DAPack: Package Contents <DAPack_contents>
   Contact <contact>
      
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

