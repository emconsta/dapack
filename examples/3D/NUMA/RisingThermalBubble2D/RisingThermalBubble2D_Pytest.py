#!/usr/bin/env


import os
import sys

import numpy as np
from scipy import linalg
import matplotlib.pyplot as plt

import collections

sys.path.append('../../../../DAPack')
#from model_HyPar_OneD import HyPar_OneD
from model_RisingThermalBubble2D import RisingThermalBubble2D
from HyPar_DAFiltering import DAFilter

import _utility

#Hypar_bin = '../../../../../../hypar/bin/HyPar' # relative to model_tmp_dir


#
plt.close('all')
os.system('clear')
np.random.seed(2345)
driver_wd = os.getcwd()
#
divider = "------------------------------------------------------------"
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# copy model files to a local/temporary directory.
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
dest = os.getcwd()
localmodel_path = _utility.prepare_hypar_model_files(dest)
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Create an instance for the RisingThermalBubble2D model and build the experiment
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
RisingThermalBubble2D_Model = RisingThermalBubble2D()

# read filter and model configs from the da_solver.ini
daFilterConfigs = _utility.read_filter_configurations()

# read the original filter configurations
originalModelConfigs = _utility.read_original_model_configurations()
if originalModelConfigs.has_key('model'):
    model_name = originalModelConfigs['model']


n_iter       = originalModelConfigs['n_iter']
scr_op_iter  = originalModelConfigs['screen_op_iter']
file_op_iter = originalModelConfigs['file_op_iter']
# initial update of model configurations
new_fields = { 'op_file_format'   : 'text'     ,      \
               'op_overwrite'     : 'yes'      ,      \
               'ip_file_type'     : 'ascii'    ,      \
               'screen_op_iter'   :  n_iter    ,      \
               'file_op_iter'     :  n_iter           \
              }
_utility.overwrite_original_model_configurations(new_fields )
ModelConfigs = _utility.read_original_model_configurations()
# make sure binary initial condtion file exists (create if not)
#_utility.check_initial_condition(dest,overwrite = True)

# intialize the model object
print 'Initializing the model'
RisingThermalBubble2D_Model.model_setup(ModelConfigs)


#Retrieve model grids and convert initial condition to portable text file.
print 'Obtaining model grids'
RisingThermalBubble2D_Model.set_grids()

#>>>>>>>>>>>>>>>>>>>>>>>>>------------------<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
#         Test time propagation scheme and retrieve grid design.
#>>>>>>>>>>>>>>>>>>>>>>>>>------------------<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
#print 'Test time propagation scheme'
#time_bounds = np.array([0 , 10])
#out_state = RisingThermalBubble2D_Model.step_forward(time_bounds , rewrite_as_binary_ip=False )
#RisingThermalBubble2D_Model._Grid_vec = grid[:]
#
#_utility.write_output_as_binary_initial_codition(grid , out_state)
#time_bounds = np.array([100 , 1000])
#out_state = RisingThermalBubble2D_Model.step_forward(time_bounds , rewrite_as_binary_ip=True )
##>>>>>>>>>>>>>>>>>>>>>>>>>------------------<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Create an instance for the filter and set up the parameters
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Filter = DAFilter(daFilterConfigs)
Filter.set_DA_filter(RisingThermalBubble2D_Model)

# 2- use the reference initial condition to create a modeled background error covariance matrix
print "Creating background error covariance matrix..."
RisingThermalBubble2D_Model.construct_background_Error_Cov(RisingThermalBubble2D_Model._Ref_IC)

# 3- construct the observation operator
RisingThermalBubble2D_Model.construct_obs_oper( )

# 4- construct an observaiton error covariance matrix
RisingThermalBubble2D_Model.construct_observation_Error_Cov(Filter._Filter_Time_Span)

# 5- perturb initial condition  --> initial forecast
print "Generating forecast initial condition..."
frcst_initial_condition  = RisingThermalBubble2D_Model.generate_initialForecast()
#frcst_initial_condition  = ref_initial_condition + np.dot(RisingThermalBubble2D_Model._sqrtB0 , _utility.randn_Vec(RisingThermalBubble2D_Model._state_size) )

# 6- create initial ensemble
print "Generating an Initial Ensemble..."
Filter.create_Initil_Ensemble(RisingThermalBubble2D_Model , frcst_initial_condition )

# 7- Sequential data assimilation process
Filter.DA_filtering_process(RisingThermalBubble2D_Model , RisingThermalBubble2D_Model._Ref_IC )


print '\nDone...Terminating...\n----------------------------------------------',
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
_utility.clean_executables()
##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
