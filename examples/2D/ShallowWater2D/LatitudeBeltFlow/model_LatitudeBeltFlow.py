#!/usr/bin/env

import os
import sys

import numpy as np
from scipy import linalg
import scipy.sparse as sparse

import matplotlib.pyplot as plt

import collections

sys.path.append('../../../../DAPack')
from HyPar_DAFiltering import DAFilter
from _model_base_HyPar import ModelBase_HyPar
#from _model_base import ModelsBase
import _utility
from _visualization import DA_Visualization
#Hypar_bin = '../../../../../../hypar/bin/HyPar' # relative to model_tmp_dir

#================================================================================================================================#
# A class implementing the three-variables Lorenz model.
#================================================================================================================================#
class LatitudeBeltFlow(ModelBase_HyPar):
    """
       A class implementing the necessary functions of the DensitySineWave model.
       Define new functions or override funcitons in the base class "model_HyPar_OneD"
    """
    pass
    #-----------------------------------------------------------------------------------------------------------------------#
    # Constructor of the model object:
    #-----------------------------------------------------------------------------------------------------------------------#
    def __init__(self):
        #
        self._setup       = True
    #
    #-----------------------------------------------------------------------------------------------------------------------#






    #-----------------------------------------------------------------------------------------------------------------------#
    # this constructs the observation operator, and it's effect on a vector
    #-----------------------------------------------------------------------------------------------------------------------#
    #

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    # Construct H
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    def construct_obs_oper(self, varObs_jump=None):
        """
        #.. py:function:: construct_obs_oper([varObs_jump=None])

        :param int varObs_jump: This defines the observation frequency in space (among grid points).
                                1 means all prognostic variables are observed at all gridpoints.
                                2 means all prognostic variables are observed at each second gridpoints, etc.
                                If not given, a default of 1 is used.
        :return: An np.ndarray ``self._H`` is attached to the model and saved as an observation operator.
        """
        #
        if varObs_jump is None:
            varObs_jump = self._observed_Variables_Jump

        # varObs_jump: variable observed.: 1-> observe all, 2-> each second, etc...
        if self._state_size == self._nvars: # this is not relevant here, but let's leave it
            # for 0 and 1 dimensional models with a single variable
            self._no_of_obsPoints = self._state_size / varObs_jump
            self._vars_observed = np.zeros(self._no_of_obsPoints,
                                           dtype=np.int)  # observe all variables at selective grid-points
            self._vars_observed = np.arange(0, self._state_size, varObs_jump)
        else:
            self._no_of_obsPoints = self._grid_size / varObs_jump  # number of observation grid-points
            self._vars_observed = np.zeros(self._no_of_obsPoints * self._nvars,
                                           dtype=np.int)  # observe all variables at selective grid-points
            for gr_ind in xrange(self._no_of_obsPoints):
                self._vars_observed[self._nvars * gr_ind:self._nvars * (gr_ind + 1)] = [obs_ind for obs_ind in xrange(
                    self._nvars * gr_ind * varObs_jump, self._nvars * (gr_ind * varObs_jump + 1))]

        # we need to add observational grid here to plot contour of observations if requested



        # self._vars_observed contains indexes of all prognostic variables at only observed grid points
        # This can be different from the size of the observation vector in general
        #
        self._no_of_observed_vars = np.size(self._vars_observed)

        if self._observation_Operator_Type.lower() == 'linear':
            self._obs_vec_size = np.size(self._vars_observed)
            #
            # short-fat matrix (all prognostic variables are observed in this case)
            vals = np.ones(np.size(self._vars_observed))
            i_inds = np.arange(np.size(self._vars_observed))
            j_inds = self._vars_observed
            if self._Use_Sparse:
                # vals = np.ones(np.size(self._vars_observed))
                # i_inds = np.arange(np.size(self._vars_observed))
                # j_inds = self._vars_observed
                self._H = sparse.csr_matrix(( vals, (i_inds, j_inds) ),
                                            shape=(np.size(self._vars_observed), self._state_size))

            else:
                self._H = np.zeros((np.size(self._vars_observed), self._state_size))
                # for obs_ind in range(np.size(self._vars_observed)):
                #     self._H[obs_ind, self._vars_observed[obs_ind]] = 1
                self._H[i_inds,j_inds] = vals

        elif self._observation_Operator_Type.lower() in ['magnitude', 'wind_magnitude' , 'wind_magnitude_and_height' ]:

            # In this operator, the magnitude of wind velocity is evaluated, and the height is evaluated.
            # No_Obs_Points = self._no_of_obsPoints
            #
            self._H = None # this will be used to throw an exception if explicitly used.



        elif self._observation_Operator_Type.lower() == 'quadratic':
            raise NotImplementedError("Quadratic observation operator is yet to be implemented")
        else:
            raise NotImplementedError("Check the type of observation operator in the filter configurations file")

    #
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<



    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    # Evaluate the effect of the observation operator (H) on a state vector 'U' by multiplication
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    def obs_oper_prod_Vec(self, U):
        """
        #.. py:function:: obs_oper_prod_Vec(U)

        Evaluate the effect of the observation operator ``self._H`` on a state vector ``U``.
        For linear observation operator, this is a matrix-vector product.

        :param Vec U: state vector.
        :return: theoritical observation resulting as the effect of ``self._H`` on ``u``, i,e, ``self._H(U)``.
        """

        if self._observation_Operator_Type.lower() == 'linear':
            if self._Use_Sparse:
                Theo_Y = (self._H).dot(U)

            else:
                Theo_Y = np.dot(self._H, U)

        elif self._observation_Operator_Type.lower() in ['magnitude', 'wind_magnitude' , 'wind_magnitude_and_height' ]:

            # In this operator, the magnitude of wind velocity is evaluated, and the height is evaluated.

            #
            self._obs_vec_size = self._no_of_obsPoints * 2
            Theo_Y = np.zeros(self._obs_vec_size) # Here we have three prognostic variables (h, hu, hv)

            Locs_slider = self._vars_observed[::self._nvars]
            #
            # print 'obs_vec_size' , obs_vec_size
            # print 'Locs_slider' , Locs_slider
            # print 'U' , U

            h_vals = U[Locs_slider]
            u_vals = U[Locs_slider+1]/U[Locs_slider]
            v_vals = U[Locs_slider+2]/U[Locs_slider]


            Theo_Y[::2] = h_vals
            Theo_Y[1::2] = np.sqrt(u_vals**2 + v_vals**2)



        elif self._observation_Operator_Type.lower() == 'quadratic':
            raise NotImplementedError("Quadratic observation operator is yet to be implemented")

        else:
            raise NotImplementedError("Check the type of observation operator in the filter configurations file")



        return Theo_Y

    #
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    #  Evaluate the tangent linear model of the observation operator at vector 'U'
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    def obs_doper_prod_dU(self, U):
        """
        #.. py:function:: obs_doper_prod_dU(U)

        Evaluate the tangent linear (TLM) forward observation operator at a state vector ``U``.

        :param Vec U: state vector.
        :return: TLM of the observation operator evaluated at ``U``.
        """

        if self._observation_Operator_Type.lower() == 'linear':
            #same here
            Obs_Oper_Jac =  self._H


        elif self._observation_Operator_Type.lower() in ['magnitude', 'wind_magnitude' , 'wind_magnitude_and_height' ]:

            # In this operator, the magnitude of wind velocity is evaluated, and the height is evaluated.

            nobs_vars = 2

            # i_inds = np.arange(self._no_of_observed_vars)
            i_inds = np.zeros(self._no_of_obsPoints*self._nvars)
            i_inds[::self._nvars] = np.arange(0,self._obs_vec_size, nobs_vars)
            i_inds[1::self._nvars] = np.arange(1,self._obs_vec_size, nobs_vars)
            i_inds[2::self._nvars] = np.arange(1,self._obs_vec_size, nobs_vars)

            j_inds = self._vars_observed


            #
            Locs_slider = j_inds[::self._nvars]

            h_vals = U[Locs_slider]
            u_vals = U[Locs_slider+1]/h_vals
            v_vals = U[Locs_slider+2]/h_vals
            scld_wind_mag = np.sqrt( (u_vals*h_vals)**2 + (v_vals*h_vals)**2 )

            TLM_vals = np.zeros(self._no_of_observed_vars)
            TLM_vals[::self._nvars] = 1
            TLM_vals[1::self._nvars] = (u_vals*np.sign(h_vals)) / scld_wind_mag
            TLM_vals[2::self._nvars] = (v_vals*np.sign(h_vals)) / scld_wind_mag


            # Construct Linear version of the observation operator
            # short-fat matrix (all prognostic variables are observed in this case)
            if self._Use_Sparse:
                Obs_Oper_Jac = sparse.csr_matrix(( TLM_vals , (i_inds, j_inds) ),
                                            shape=(self._obs_vec_size, self._state_size))
            else:
                Obs_Oper_Jac =  np.zeros((self._obs_vec_size, self._state_size))
                Obs_Oper_Jac =  [i_inds , j_inds] = TLM_vals



        elif self._observation_Operator_Type.lower() == 'quadratic':
            raise NotImplementedError("Quadratic observation operator is yet to be implemented")
        else:
            raise NotImplementedError("Check the type of observation operator in the filter configurations file")


        return Obs_Oper_Jac

    #
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<


    #-----------------------------------------------------------------------------------------------------------------------#
    # Design the observation error covariance matrix;
    # (override base class due to existence of magnitude observation operator)
    #-----------------------------------------------------------------------------------------------------------------------#
    #
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<
    def construct_observation_Error_Cov(self, Filter_Tspan=None, Num_Time_Points=12):
        """
        .. :py:function:: construct_observation_Error_Cov([Initial_State=None], [Filter_Tspan=None], [Num_Time_Points=12] )

        Construct the full observation error covariance matrix, and square root.
        The inverse is evaluated later by solving a linear system unless it is diagonal.

        :param Vec Filter_Tspan:  states are evaluated at all time instances of this timespan. Standard deviations of observation errors
                                  are evaluated as a pre-specified percentage of this average magnitude. ``self._Ref_IC`` is used as initial condition.
        :param int Num_Time_Points: if ``Filter_Tspan=None``, used to create a timespan consisting of this number of time instances.
        :return array: ``self._B``, ``self._invB`` are attached to the model the observaiton error covariance matrix
                             and it's square root given by Cholesky decomposition.
        """

        # A timespan on which the Reference state will be forwarded in time to get the average magnitude of the state
        if Filter_Tspan is None:
            Filter_Tspan = np.asarray([i * self._dt for i in xrange(Num_Time_Points)])

        strategy = self._observation_Errors_Covariance_Method
        #
        if strategy.lower() in ['empirical', 'diagonal']:

            if self._add_Model_Errors:
                reactiv_ME = True
                self._add_Model_Errors = False
            else:
                reactiv_ME = False
            #
            # accumulate states magnitudes:
            num_steps = np.size(Filter_Tspan) - 1
            #
            out_state = self._Ref_IC[:]
            observation_mag = np.abs(self.obs_oper_prod_Vec( out_state) )
            for time_ind in range(1, num_steps + 1):
                # Get time bounds of the current time-subinterval
                t_init = Filter_Tspan[time_ind - 1]
                t_final = Filter_Tspan[time_ind]
                time_bounds = np.array([t_init, t_final])
                #print 'out_state'   , out_state
                #print 'time_bounds' , time_bounds

                out_state = self.step_forward(out_state, time_bounds)
                observation_mag += np.abs(self.obs_oper_prod_Vec( out_state))

            if num_steps > 0:
                observation_mag = observation_mag / num_steps
            #prtrb = self.obs_oper_prod_Vec(state_mag) * self._observation_Noise_Level



            if (self._observation_Operator_Type.lower() == 'linear'):
                nobs_vars = self._nvars
            elif self._observation_Operator_Type.lower() in ['magnitude', 'wind_magnitude' , 'wind_magnitude_and_height' ]:
                nobs_vars = 2 # h is taken as is, and hu, hv are augmented. 3 -> 2
            else:
                raise ValueError("Observation Operator ["+self._observation_Operator_Type+"] is undefined!")

            try:
                obs_vec_size = self._obs_vec_size
            except:
                obs_vec_size = self._no_of_obsPoints * nobs_vars
                self._obs_vec_size = obs_vec_size

            # averaging over variables...
            vars_avg = np.zeros(nobs_vars)
            for var_ind in range(nobs_vars):
                vars_avg[var_ind] = np.mean(observation_mag[var_ind::nobs_vars])



            avrg_mags = np.tile(vars_avg, self._no_of_obsPoints)
            prtrb = avrg_mags * self._observation_Noise_Level



            fac = 0.95
            prtrb = (1 - fac) + fac * prtrb

            if self._Use_Sparse:
                ind = [i for i in xrange(obs_vec_size)]

                self._R = sparse.csr_matrix((prtrb ** 2, (ind, ind)),
                                            shape=(obs_vec_size, obs_vec_size))
                self._invR = sparse.csr_matrix((1 / (prtrb ** 2), (ind, ind) ),
                                               shape=(obs_vec_size, obs_vec_size))
                self._sqrtR = sparse.csr_matrix((prtrb, (ind, ind)),
                                                shape=(obs_vec_size, obs_vec_size))

                self._detR = np.exp(np.log(
                    self._R.diagonal()).sum())  # determinant of R. Needed for Likelihood evaluation to avoid underflow issues caused by numpy
                #self._detR = np.prod(self._R.diag())

            else:
                self._R = np.diag(prtrb ** 2)
                self._invR = np.diag(1 / (prtrb ** 2))
                self._sqrtR = np.diag(prtrb)

                self._detR = np.exp((np.log(np.diag(
                    self._R))).sum())  # determinant of R. Needed for Likelihood evaluation to avoid underflow issues caused by numpy
                #self._detR = np.prod(np.diag(self._R))


            if reactiv_ME:
                # turn-on model errors again if requested:
                self._add_Model_Errors = True
                reactiv_ME = False


        else:
            raise NotImplementedError("R construction method is unidentified!")


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<






#
#================================================================================================================================#


if __name__ == "__main__":
    #
    plt.close('all')
    os.system('clear')
    np.random.seed(2345)
    driver_wd = os.getcwd()
    #
    divider = "------------------------------------------------------------"

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # 1- Copy model files to a local/temporary directory.
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    dest = os.getcwd()
    localmodel_path = _utility.prepare_hypar_model_files(dest)
    #~~~~~~~~~~~~~~~~~~~~~          ~~~~~~~~          ~~~~~~~~~~~~~~~~~~~~~
    # 1- Read filter and model configs from the da_solver.ini
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    daFilterConfigs = _utility.read_filter_configurations()
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # 2- Create an instance for the DensitySineWave model and build
    #    the experiment.
    #    Then, prepare the model: overwrite the configuration file,
    #    setup the grids, and create necessary matrices.
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    LatitudeBeltFlow_Model = LatitudeBeltFlow()
    _utility.prepare_model_for_filtering(LatitudeBeltFlow_Model)
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # 3- Create an instance for the filter and set up the parameters.
    #    Also, the uncertainty parameters are added to the model based
    #    on the filter configurations.
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Filter = DAFilter(daFilterConfigs)
    Filter.set_DA_filter(LatitudeBeltFlow_Model)
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # 4- Sequential data assimilation process
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Filter.DA_filtering_process(LatitudeBeltFlow_Model)
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # 5- Plot selective results:
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #print 'Plotting Results...\n',divider
    da_plotter=DA_Visualization()
    da_plotter.plot_RMSE_results(read_from_files=True, log_scale=True  , Font_Size=14, show_fig = False)
    da_plotter.plot_states_trajectories(animated_plot=True,plot_type='contour', read_from_files=True)

    #
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    print '\nDone...Terminating...\n----------------------------------------------',
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    _utility.clean_executables()
    ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
