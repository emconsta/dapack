#!/usr/bin/env


import os

import numpy as np
import matplotlib.pyplot as plt


import sys
sys.path.append('../../../DAPack')
from model_pendulum import ModelPendulum
from HyPar_DAFiltering import DAFilter

import _utility
from _visualization import DA_Visualization
#
plt.close('all')
os.system('clear')
np.random.seed(2345)
#
divider = "------------------------------------------------------------"
#

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# 1- Clean-up temporary model directory,
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
_utility.clean_model_dir('temp_model')
#
#~~~~~~~~~~~~~~~~~~~~~          ~~~~~~~~          ~~~~~~~~~~~~~~~~~~~~~
# prepare model configuration file to unify procedure with HyPar models
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Model_Configs = {'ndims': 1 , 'nvars': 2 , 'size': np.array([1]) , 'n_iter':100 , 'model': 'Pendulum' }
_utility.create_original_model_configurations(Model_Configs)
#~~~~~~~~~~~~~~~~~~~~~          ~~~~~~~~          ~~~~~~~~~~~~~~~~~~~~~
# 1- Read filter and model configs from the da_solver.ini
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
daFilterConfigs = _utility.read_filter_configurations()
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# 2- Create an instance for the DensitySineWave model and build
#    the experiment.
#    Then, prepare the model: overwrite the configuration file,
#    setup the grids, and create necessary matrices.
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Pendulum_Model = ModelPendulum()
_utility.prepare_model_for_filtering(Pendulum_Model)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# 3- Create an instance for the filter and set up the parameters.
#    Also, the uncertainty parameters are added to the model based
#    on the filter configurations.
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Filter = DAFilter(daFilterConfigs)
Filter.set_DA_filter(Pendulum_Model)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# 4- Sequential data assimilation process
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Filter.DA_filtering_process(Pendulum_Model , keep_all_in_memory=True)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# 5- Plot selective results:
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
print 'Plotting Results...\n',divider
da_plotter = DA_Visualization()
da_plotter.plot_RMSE_results(Filter_Object=Filter , read_from_files=True, log_scale=True  , show_fig = True )
da_plotter.plot_states_trajectories(show_fig=True, plot_observed_states=True)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Plot results. Reference trajectory, forecast trajectory and observations.
# This is specific to this model.
#
referenceTraject  =  Filter._referenceTrajectory
noAssimTraject    =  Filter._NOassimilationTrajectory
forecastTraject   =  Filter._forecastTrajectory
analysisMeans     =  Filter._analysisMeans
Observations      =  Filter._Observations
Fl_Timespan       =  Filter._Filter_Time_Span
Obs_Timespan      =  Filter._Observation_Time_Span


#
print 'Plotting Results...\n',divider
#
outfig = plt.figure(1)
FS = 14
font = {'weight' : 'bold', 'size' : FS}
plt.rc('font', **font)
plt.plot(Fl_Timespan, referenceTraject[0,:] , 'k' , linewidth=2 , label='Reference')
plt.plot(Fl_Timespan, referenceTraject[1,:] , 'k' , linewidth=2)
plt.plot(Fl_Timespan, noAssimTraject[0,:] , 'b' , linewidth=2 , label='NO Assimilation')
plt.plot(Fl_Timespan, noAssimTraject[1,:] , 'b' , linewidth=2)
plt.plot(Fl_Timespan, forecastTraject[0,:] , 'r--' , linewidth=2 , label='Forecast')
plt.plot(Fl_Timespan, forecastTraject[1,:] , 'r--' , linewidth=2)
plt.plot(Fl_Timespan, analysisMeans[0,:] , 'c--' , linewidth=2 , label='EnKF')
plt.plot(Fl_Timespan, analysisMeans[1,:] , 'c--' , linewidth=2)

plt.plot(Obs_Timespan, Observations[0,:] ,'g^' , label='Observations')
plt.plot(Obs_Timespan, Observations[1,:] ,'g^')

plt.xlabel('Time',fontsize=FS , fontweight='bold' )
plt.ylabel('$x_i$',fontsize=FS+10, fontweight='bold' )
plt.title('Pendulum model experiment')
plt.legend()
plt.draw()

plt.show()


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
print '\nDone...Terminating...\n',divider
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# clean up executables for next run.
filelist = [ f for f in os.listdir(".") if f.endswith(".pyc") ]
for fl in filelist:
    os.remove(fl)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
