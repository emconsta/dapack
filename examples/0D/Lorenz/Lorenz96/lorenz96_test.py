#!/usr/bin/env

import os
import sys

import numpy as np
from scipy import linalg
import matplotlib.pyplot as plt

sys.path.append('../../../../DAPack')
from DESolver import DESolver
from model_Lorenz96 import ModelLorenz96
from HyPar_DAFiltering import DAFilter

import _utility
from _visualization import DA_Visualization

#
plt.close('all')
os.system('clear')
np.random.seed(2345)
driver_wd = os.getcwd()
#
divider = "------------------------------------------------------------"

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# 1- Clean-up temporary model directory,
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
_utility.clean_model_dir('temp_model')
#
#~~~~~~~~~~~~~~~~~~~~~          ~~~~~~~~          ~~~~~~~~~~~~~~~~~~~~~
# prepare model configurationf file to unify procedure with HyPar models
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Model_Configs = {'ndims': 1 , 'nvars': 40 , 'size': np.array([1]) , 'dt':0.005 , 'n_iter':100 , 'model': 'Lorenz-96'}
_utility.create_original_model_configurations(Model_Configs)
#~~~~~~~~~~~~~~~~~~~~~          ~~~~~~~~          ~~~~~~~~~~~~~~~~~~~~~
# 1- Read filter and model configs from the da_solver.ini
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
daFilterConfigs = _utility.read_filter_configurations()
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# 2- Create an instance for the DensitySineWave model and build
#    the experiment.
#    Then, prepare the model: overwrite the configuration file,
#    setup the grids, and create necessary matrices.
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Lorenz96_Model = ModelLorenz96()
# burn-in a state to create initial condition... (this must come after setting dt for the model)
#Lorenz96_Model.create_initial_condition()
_utility.prepare_model_for_filtering(Lorenz96_Model)
Lorenz96_Model.create_initial_condition()
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# 3- Create an instance for the filter and set up the parameters.
#    Also, the uncertainty parameters are added to the model based
#    on the filter configurations.
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Filter = DAFilter(daFilterConfigs)
Filter.set_DA_filter(Lorenz96_Model)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# 4- Sequential data assimilation process
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Filter.DA_filtering_process(Lorenz96_Model , keep_all_in_memory=True)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# 5- Plot selective results:
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
print 'Plotting Results...\n',divider
da_plotter = DA_Visualization()
da_plotter.plot_RMSE_results( Filter_Object=Filter, read_from_files=False, log_scale=False)
da_plotter.plot_states_trajectories(Filter_Object=Filter , read_from_files=True , plot_observed_states=True, Font_Size=7 , vars_to_plot=[i for i in range(9)])
#
plt.show()
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
print '\nDone...Terminating...\n',divider
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# clean up executables for next run.
filelist = [ f for f in os.listdir(".") if f.endswith(".pyc") ]
for fl in filelist:
    os.remove(fl)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
