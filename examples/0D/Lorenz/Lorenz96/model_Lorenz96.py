#!/opt/local/bin/python

import os
import sys

import numpy as np
from scipy import linalg
import matplotlib.pyplot as plt

sys.path.append('../../../../DAPack')
from DESolver import DESolver
from _model_base_HyPar import ModelBase_HyPar
from HyPar_DAFiltering import DAFilter

import _utility
from _visualization import DA_Visualization

#================================================================================================================================#
# A class implementing the three-variables Lorenz model.
#================================================================================================================================#
class ModelLorenz96(ModelBase_HyPar):
    """
    Class implementing Lorenz-96 (40 variables) model.
    """


    #-----------------------------------------------------------------------------------------------------------------------#
    # Constructor of the model object:
    #-----------------------------------------------------------------------------------------------------------------------#
    def __init__(self, create_Ref_IC=False):
        #
        self._name       = "Lorenz-96 model"
        self._setup      = False
        self._state_size = 40
        self._Forcing    = 8

        self._dt         = 0.005
        self._nvars      = 40

        if create_Ref_IC:
            self._dt     = 0.005
            self._Ref_IC = self.create_initial_condition()
    #
    #-----------------------------------------------------------------------------------------------------------------------#


    #-----------------------------------------------------------------------------------------------------------------------#
    # This should be called immediately after initialization. May be from inside __init__ or in the main program...
    #-----------------------------------------------------------------------------------------------------------------------#
    def create_initial_condition(self , x_init=np.linspace(-2,2,40) , burnin_time_bounds=np.array([0,20]) ):
        #
        # turn-off model errors temporarily if it is on:
        try:
            if self._add_Model_Errors:
                reactiv_ME = True
                self._add_Model_Errors = False
            else:
                reactiv_ME = False

            #
            self._Ref_IC = self.step_forward(x_init , burnin_time_bounds)
            #
            if reactiv_ME:
                #turn-on model errors again if requested:
                self._add_Model_Errors = True
                reactiv_ME = False
        except:
            self._add_Model_Errors = False
            self._Ref_IC = self.step_forward(x_init , burnin_time_bounds)
    #
    #-----------------------------------------------------------------------------------------------------------------------#






    #-----------------------------------------------------------------------------------------------------------------------#
    # Set the spatial grids' parameters:
    #-----------------------------------------------------------------------------------------------------------------------#
    def set_grids(self):
        #
        # set spatial grid: (Only To unify work with HyPar design)
        #------------------------
        self._grids = np.array([0])
        self._Grids_dict = {'0': self._grids}
        self._spacings = np.array([1]) # used here to use index for decorrelation array

        self._nvars = 40


    #
    #-----------------------------------------------------------------------------------------------------------------------#




    #-----------------------------------------------------------------------------------------------------------------------#
    # Model forward function; Here the right-hand side function.
    #-----------------------------------------------------------------------------------------------------------------------#
    def step_forward_function(self, x , t ):
        """
        Function describing the dynamical model: $\dot{x}=f(x)$
               + param x: the state of the model.
               +    t   : time
             > return fx: function of the state.
        """
        F = self._Forcing

        fx    = np.empty(shape=x.shape)
        nvar  = x.size
        fx[0] = ( x[nvar-1] * ( x[1] - x[nvar-2] )   )   - x[0]      + F
        fx[1] = ( x[0]      * ( x[2] - x[nvar-1] )   )   - x[1]      + F
        #fx[2:nvar-2] = ( x[1:nvar-3] *(x[3:nvar-1]-x[0:nvar-4]) ) - x[2:nvar-2] + F
        ind = 2
        while (ind <=nvar-2):
            fx[ind]    = ( x[ind-1]  * ( x[ind+1] - x[ind-2]) )   - x[ind]    + F
            ind+=1

        fx[nvar-1] = ( x[nvar-2] * ( x[0]-x[nvar-3] ) )  - x[nvar-1] + F

        return fx

    #
    #-----------------------------------------------------------------------------------------------------------------------#




    #-----------------------------------------------------------------------------------------------------------------------#
    # Jacobian of the model (Jacobian of right-hand side). Not needed now!
    #-----------------------------------------------------------------------------------------------------------------------#
    def step_dforward_dx_function(self,x ,t ):
        """
        Jacobian of the model. Not needed now!
        """
        nvar = x.size
        dx   = np.empty((nvar,nvar))

        return dx

        # Just some time for coding... (not needed for current tests)

    #
    #-----------------------------------------------------------------------------------------------------------------------#




    #-----------------------------------------------------------------------------------------------------------------------#
    #-----------------------------------------------------------------------------------------------------------------------#
    def step_forward(self, state , time_bounds ):
        """
        Steps the model forward

        :param state: the state of the model.
        :type state: list.
        :param params: parameters of the model.
        :type params: list.
        :return: list of states from the model advanced by one step.
        """
        # calculate the number of iterations based on dt in the configurations of the model
        dt = self._dt
        tspan_length = time_bounds[1] - time_bounds[0]
        n_iter = int(np.ceil(tspan_length / dt))

        #
        #
        if not(self._add_Model_Errors) or                          \
               np.isinf(self._modelError_Steps_per_Model_Steps) or \
               self._modelError_Steps_per_Model_Steps > n_iter     \
               :
            #
            # propagate directly without model errors
            Tspan = _utility.linspace(time_bounds[0], time_bounds[1], dt)
            #print Tspan


            odeSolver = DESolver(self.step_forward_function)
            for iter_stp in range(n_iter-1):
                # create a DESolver object and passing the step_forward_funciton it's constructor
                odeSolver.set_initial_condition(state)
                y , t     = odeSolver.solve(time_points=Tspan[iter_stp:iter_stp+2] , solver='RK2a')
                #y , t     = odeSolver.solve(time_points=np.asarray(time_bounds) , solver='RK2a')
                state = y[1,:]

            out_state = state


        else:
            #step forward with intermediate model errors added.
            #print 'time_bounds',time_bounds
            #print 'dt',dt
            #print 'n_iter ', n_iter
            #print 'self._modelError_Steps_per_Model_Steps', self._modelError_Steps_per_Model_Steps

            add_Q_iters = n_iter/self._modelError_Steps_per_Model_Steps
            #print 'add_Q_iters ',add_Q_iters
            more_iters  = n_iter%self._modelError_Steps_per_Model_Steps
            #print 'more_iters ',more_iters

            TSpan_W_ME = [itr*dt*self._modelError_Steps_per_Model_Steps for itr in xrange(add_Q_iters)]
            #print 'TSpan_W_ME',TSpan_W_ME

            for iter_ME in range(len(TSpan_W_ME)-1):
                #
                #print TSpan_W_ME[iter_ME]
                #print TSpan_W_ME[iter_ME+1]
                #print dt
                Tspan = _utility.linspace(TSpan_W_ME[iter_ME], TSpan_W_ME[iter_ME+1], dt , endpoint=True )
                #print Tspan
                odeSolver = DESolver(self.step_forward_function)
                for iter_stp in range(self._modelError_Steps_per_Model_Steps-1):
                    # create a DESolver object and passing the step_forward_funciton it's constructor
                    odeSolver.set_initial_condition(state)
                    #print '>>', Tspan[iter_stp:iter_stp+2]
                    y , t     = odeSolver.solve(time_points=Tspan[iter_stp:iter_stp+2] , solver='RK2a')
                    state = y[1,:]

                    # add model errors
                    model_errors_Vec = self.generate_modelNoise_Vector()
                    state = state + model_errors_Vec

                # add model errors:
                model_errors_Vec = self.generate_modelNoise_Vector()
                state = state + model_errors_Vec

            out_state = state
            #print 'out_state' , out_state



            if more_iters>0:
                # more iterations without model errors
                state = out_state
                tspan_length = time_bounds[-1] - TSpan_W_ME[-1]
                n_iter = int(np.ceil(tspan_length / dt))
                #print 'TSpan_W_ME[-1] ',TSpan_W_ME[-1]
                #print 'time_bounds[-1] ',time_bounds[-1]
                Tspan = _utility.linspace(TSpan_W_ME[-1], time_bounds[-1], dt)
                odeSolver = DESolver(self.step_forward_function)

                for iter_stp in range(n_iter-1):
                    # create a DESolver object and passing the step_forward_funciton it's constructor
                    odeSolver.set_initial_condition(state)
                    y , t     = odeSolver.solve(time_points=Tspan[iter_stp:iter_stp+2] , solver='RK2a')
                    #y , t     = odeSolver.solve(time_points=np.asarray(time_bounds) , solver='RK2a')
                    state = y[1,:]

                out_state = state


        x_out = out_state

        return x_out
    #
    #-----------------------------------------------------------------------------------------------------------------------#



    #
    #-----------------------------------------------------------------------------------------------------------------------#



#
#================================================================================================================================#


if __name__ == "__main__":
    #
    plt.close('all')
    os.system('clear')
    np.random.seed(2345)
    driver_wd = os.getcwd()
    #
    divider = "------------------------------------------------------------"

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # 1- Clean-up temporary model directory,
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #
    _utility.clean_model_dir('temp_model')
    #
    #~~~~~~~~~~~~~~~~~~~~~          ~~~~~~~~          ~~~~~~~~~~~~~~~~~~~~~
    # prepare model configurationf file to unify procedure with HyPar models
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Model_Configs = {'ndims': 1 , 'nvars': 40 , 'size': np.array([1]) , 'dt':0.005 , 'n_iter':100 , 'model': 'Lorenz-96'}
    _utility.create_original_model_configurations(Model_Configs)
    #~~~~~~~~~~~~~~~~~~~~~          ~~~~~~~~          ~~~~~~~~~~~~~~~~~~~~~
    # 1- Read filter and model configs from the da_solver.ini
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    daFilterConfigs = _utility.read_filter_configurations()
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # 2- Create an instance for the DensitySineWave model and build
    #    the experiment.
    #    Then, prepare the model: overwrite the configuration file,
    #    setup the grids, and create necessary matrices.
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Lorenz96_Model = ModelLorenz96()
    # burn-in a state to create initial condition... (this must come after setting dt for the model)
    #Lorenz96_Model.create_initial_condition()
    _utility.prepare_model_for_filtering(Lorenz96_Model)
    Lorenz96_Model.create_initial_condition()
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # 3- Create an instance for the filter and set up the parameters.
    #    Also, the uncertainty parameters are added to the model based
    #    on the filter configurations.
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Filter = DAFilter(daFilterConfigs)
    Filter.set_DA_filter(Lorenz96_Model)
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # 4- Sequential data assimilation process
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Filter.DA_filtering_process(Lorenz96_Model)
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # 5- Plot selective results:
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #
    print 'Plotting Results...\n',divider
    da_plotter = DA_Visualization()
    da_plotter.plot_RMSE_results(read_from_files=True, log_scale=False)
    # da_plotter.plot_states_trajectories(Filter_Object=Filter, read_from_files=False,show_fig=True, plot_observed_states=True)
    da_plotter.plot_states_trajectories(show_fig=True,plot_observed_states=True)
    plt.show()
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    print '\nDone...Terminating...\n',divider
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    _utility.clean_executables()
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
