#!/opt/local/bin/python

import os
import sys

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

sys.path.append('../../../../DAPack')
from DESolver import DESolver
from _model_base_HyPar import ModelBase_HyPar
from HyPar_DAFiltering import DAFilter

import _utility
from _visualization import DA_Visualization

#================================================================================================================================#
# A class implementing the three-variables Lorenz model.
#================================================================================================================================#
class ModelLorenz3(ModelBase_HyPar):
    """
    Class implementing Lorenz-3 model.
    """


    #-----------------------------------------------------------------------------------------------------------------------#
    # Constructor of the model object:
    #-----------------------------------------------------------------------------------------------------------------------#
    def __init__(self , x_init=np.array([1.5, 1.5 , 1]) ):
        #
        self._name       = "Lorenz-3 model"
        self._setup      = False
        self._state_size = 3
        self._sigma      = 10
        self._rho        = 28
        self._beta       = 8./3

        self._dt         = 0.001
        self._nvars      = 3

        self._Ref_IC = x_init
    #
    #-----------------------------------------------------------------------------------------------------------------------#





    #-----------------------------------------------------------------------------------------------------------------------#
    # Set the spatial grids' parameters:
    #-----------------------------------------------------------------------------------------------------------------------#
    def set_grids(self):
        #
        # set spatial grid: (Only To unify work with HyPar design)
        #------------------------
        self._grids = np.array([0])
        self._Grids_dict = {'0': self._grids}
        self._spacings = np.array([1]) # used here to use index for decorrelation array
        self._nvars  = 3


    #
    #-----------------------------------------------------------------------------------------------------------------------#




    #-----------------------------------------------------------------------------------------------------------------------#
    # Model forward function; Here the right-hand side function.
    #-----------------------------------------------------------------------------------------------------------------------#
    def step_forward_function(self, x , t ):
        """
        Function describing the dynamical model: $\dot{x}=f(x)$
               + param x: the state of the model.
               +    t   : time
             > return fx: function of the state.
        """
        sig = self._sigma
        ro  = self._rho
        bet = self._beta

        fx = np.empty(shape=x.shape)
        fx[0] = sig*(x[1]-x[0])
        fx[1] = x[0]*(ro - x[2]) - x[1]
        fx[2] = x[0]*x[1] - (bet * x[2])

        return fx
    #
    #-----------------------------------------------------------------------------------------------------------------------#




    #-----------------------------------------------------------------------------------------------------------------------#
    # Jacobian of the model (Jacobian of right-hand side). Not needed now!
    #-----------------------------------------------------------------------------------------------------------------------#
    def step_dforward_dx_function(self,x ,t ):
        """
        Jacobian of the model. Not needed now!
        """
        sig = self._sigma
        ro  = self._rho
        bet = self._beta

        nvar = x.size
        dx   = np.empty((nvar,nvar))

        nvar[0,:] = np.array([ -sig      ,    sig   ,   0      ])
        nvar[1,:] = np.array([  ro-x[2]  ,    -1    ,  -x[0]   ])
        nvar[2,:] = np.array([  x[1]     ,    x[2]  ,  -bet   ])

        return dx

    #
    #-----------------------------------------------------------------------------------------------------------------------#




    #-----------------------------------------------------------------------------------------------------------------------#
    #-----------------------------------------------------------------------------------------------------------------------#
    def step_forward(self, state , time_bounds ):
        """
        Steps the model forward

        :param state: the state of the model.
        :type state: list.
        :param params: parameters of the model.
        :type params: list.
        :return: list of states from the model advanced by one step.
        """
        # calculate the number of iterations based on dt in the configurations of the model
        dt = self._dt
        tspan_length = time_bounds[1] - time_bounds[0]
        n_iter = int(np.ceil(tspan_length / dt))

        #
        #
        if not(self._add_Model_Errors) or                          \
               np.isinf(self._modelError_Steps_per_Model_Steps) or \
               self._modelError_Steps_per_Model_Steps > n_iter     \
               :
            #
            # propagate directly without model errors
            Tspan = _utility.linspace(time_bounds[0], time_bounds[1], dt)
            #print Tspan


            odeSolver = DESolver(self.step_forward_function)
            for iter_stp in range(n_iter-1):
                # create a DESolver object and passing the step_forward_funciton it's constructor
                odeSolver.set_initial_condition(state)
                y , t     = odeSolver.solve(time_points=Tspan[iter_stp:iter_stp+2] , solver='RK2a')
                #y , t     = odeSolver.solve(time_points=np.asarray(time_bounds) , solver='RK2a')
                state = y[1,:]

            out_state = state


        else:
            #step forward with intermediate model errors added.
            #print 'n_iter ', n_iter
            #print 'self._modelError_Steps_per_Model_Steps', self._modelError_Steps_per_Model_Steps

            add_Q_iters = n_iter/self._modelError_Steps_per_Model_Steps
            #print 'add_Q_iters ',add_Q_iters
            more_iters  = n_iter%self._modelError_Steps_per_Model_Steps
            #print 'more_iters ',more_iters

            TSpan_W_ME = [itr*dt*self._modelError_Steps_per_Model_Steps for itr in xrange(add_Q_iters)]
            #print 'TSpan_W_ME',TSpan_W_ME

            for iter_ME in range(len(TSpan_W_ME)-1):
                #
                #print TSpan_W_ME[iter_ME]
                #print TSpan_W_ME[iter_ME+1]
                #print dt
                Tspan = _utility.linspace(TSpan_W_ME[iter_ME], TSpan_W_ME[iter_ME+1], dt , endpoint=True )
                #print Tspan
                odeSolver = DESolver(self.step_forward_function)
                for iter_stp in range(self._modelError_Steps_per_Model_Steps-1):
                    # create a DESolver object and passing the step_forward_funciton it's constructor
                    odeSolver.set_initial_condition(state)
                    #print '>>', Tspan[iter_stp:iter_stp+2]
                    y , t     = odeSolver.solve(time_points=Tspan[iter_stp:iter_stp+2] , solver='RK2a')
                    state = y[1,:]

                    # add model errors
                    model_errors_Vec = self.generate_modelNoise_Vector()
                    state = state + model_errors_Vec

                # add model errors:
                model_errors_Vec = self.generate_modelNoise_Vector()
                state = state + model_errors_Vec

            out_state = state
            #print 'out_state' , out_state



            if more_iters>0:
                # more iterations without model errors
                state = out_state
                tspan_length = time_bounds[-1] - TSpan_W_ME[-1]
                n_iter = int(np.ceil(tspan_length / dt))
                Tspan = _utility.linspace(TSpan_W_ME[-1], time_bounds[-1], dt)
                odeSolver = DESolver(self.step_forward_function)

                for iter_stp in range(n_iter-1):
                    # create a DESolver object and passing the step_forward_funciton it's constructor
                    odeSolver.set_initial_condition(state)
                    y , t     = odeSolver.solve(time_points=Tspan[iter_stp:iter_stp+2] , solver='RK2a')
                    #y , t     = odeSolver.solve(time_points=np.asarray(time_bounds) , solver='RK2a')
                    state = y[1,:]

                out_state = state


        x_out = out_state

        return x_out
    #
    #-----------------------------------------------------------------------------------------------------------------------#




    #
    #-----------------------------------------------------------------------------------------------------------------------#

#
#================================================================================================================================#





#
#================================================================================================================================#


#
if __name__ == "__main__":
    #
    plt.close('all')
    os.system('clear')
    np.random.seed(2345)
    #
    divider = "------------------------------------------------------------"
    #

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # 1- Clean-up temporary model directory,
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #
    _utility.clean_model_dir('temp_model')
    #
    #~~~~~~~~~~~~~~~~~~~~~          ~~~~~~~~          ~~~~~~~~~~~~~~~~~~~~~
    # prepare model configurationf file to unify procedure with HyPar models
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Model_Configs = {'ndims': 1 , 'nvars': 3 , 'size': np.array([1]) , 'n_iter':100 , 'model': 'Lorenz-3' }
    _utility.create_original_model_configurations(Model_Configs)
    #~~~~~~~~~~~~~~~~~~~~~          ~~~~~~~~          ~~~~~~~~~~~~~~~~~~~~~
    # 1- Read filter and model configs from the da_solver.ini
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    daFilterConfigs = _utility.read_filter_configurations()
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # 2- Create an instance for the DensitySineWave model and build
    #    the experiment.
    #    Then, prepare the model: overwrite the configuration file,
    #    setup the grids, and create necessary matrices.
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Lorenz3_Model = ModelLorenz3()
    _utility.prepare_model_for_filtering(Lorenz3_Model)
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # 3- Create an instance for the filter and set up the parameters.
    #    Also, the uncertainty parameters are added to the model based
    #    on the filter configurations.
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Filter = DAFilter(daFilterConfigs)
    Filter.set_DA_filter(Lorenz3_Model)
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # 4- Sequential data assimilation process
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Filter.DA_filtering_process(Lorenz3_Model,keep_all_in_memory=True)
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # 5- Plot selective results:
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #
    print 'Plotting Results...\n',divider
    da_plotter = DA_Visualization()
    da_plotter.plot_RMSE_results( Filter_Object=Filter, read_from_files=False, log_scale=True)
    da_plotter.plot_states_trajectories(show_fig=True, read_from_files=True , plot_observed_states=True)
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~





    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Plot results. Reference trajectory, forecast trajectory and observations.
    # this is specific to this model.
    #
    referenceTraject  =  Filter._referenceTrajectory
    noAssimTraject    =  Filter._NOassimilationTrajectory
    forecastTraject   =  Filter._forecastTrajectory
    analysisMeans     =  Filter._analysisMeans
    Observations      =  Filter._Observations
    #Timespan          =  Filter._Filter_Time_Span

    #
    print 'Plotting Results...\n',divider
    #
    outfig = plt.figure(1)
    ax = outfig.gca(projection='3d')
    FS = 14
    font = {'weight' : 'bold', 'size' : FS}
    plt.rc('font', **font)

    ax.plot(referenceTraject[0,:] , referenceTraject[1,:] ,referenceTraject[2,:] , 'k' , linewidth=2 , label='Reference')

    ax.plot(noAssimTraject[0,:]   , noAssimTraject[1,:]   ,noAssimTraject[2,:]   , 'b' , linewidth=2 , label='NO Assimilation')

    ax.plot(forecastTraject[0,:]  , forecastTraject[1,:] ,forecastTraject[2,:] , 'r--' , linewidth=2 , label='Forecast')

    plt.plot( analysisMeans[0,:], analysisMeans[1,:] , analysisMeans[2,:], 'c--' , linewidth=4 , label='EnKF')

    plt.plot(Observations[0,:], Observations[1,:], Observations[2,:] ,'g^' , label='Observations')

    ax.set_xlabel('$x_1$',fontsize=FS+10, fontweight='bold' )
    ax.set_ylabel('$x_2$',fontsize=FS+10, fontweight='bold' )
    ax.set_zlabel('$x_3$',fontsize=FS+10, fontweight='bold' )
    plt.title('Lorenz-3 model experiment')
    plt.legend()
    plt.draw()
    
    
    plt.show()

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    print '\nDone...Terminating...\n',divider
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    _utility.clean_executables()
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
