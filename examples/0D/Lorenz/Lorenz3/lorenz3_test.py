#!/usr/bin/env

import os
import sys

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

sys.path.append('../../../../DAPack')
from model_Lorenz3 import ModelLorenz3
from HyPar_DAFiltering import DAFilter

import _utility
from _visualization import DA_Visualization

#
plt.close('all')
os.system('clear')
np.random.seed(2345)
#
divider = "------------------------------------------------------------"
#

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# 1- Clean-up temporary model directory,
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
_utility.clean_model_dir('temp_model')
#
#~~~~~~~~~~~~~~~~~~~~~          ~~~~~~~~          ~~~~~~~~~~~~~~~~~~~~~
# prepare model configurationf file to unify procedure with HyPar models
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Model_Configs = {'ndims': 1 , 'nvars': 3 , 'size': np.array([1]) , 'dt':0.01 , 'n_iter':100 , 'model': 'Lorenz-3' }
_utility.create_original_model_configurations(Model_Configs)
#~~~~~~~~~~~~~~~~~~~~~          ~~~~~~~~          ~~~~~~~~~~~~~~~~~~~~~
# 1- Read filter and model configs from the da_solver.ini
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
daFilterConfigs = _utility.read_filter_configurations()
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# 2- Create an instance for the DensitySineWave model and build
#    the experiment.
#    Then, prepare the model: overwrite the configuration file,
#    setup the grids, and create necessary matrices.
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Lorenz3_Model = ModelLorenz3()
_utility.prepare_model_for_filtering(Lorenz3_Model)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# 3- Create an instance for the filter and set up the parameters.
#    Also, the uncertainty parameters are added to the model based
#    on the filter configurations.
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Filter = DAFilter(daFilterConfigs)
Filter.set_DA_filter(Lorenz3_Model)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# 4- Sequential data assimilation process
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Filter.DA_filtering_process(Lorenz3_Model,keep_all_in_memory=True)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# 5- Plot selective results:
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
print 'Plotting Results...\n',divider
da_plotter = DA_Visualization()
da_plotter.plot_RMSE_results(Filter_Object=Filter, show_fig=True , read_from_files=False, log_scale=True)
da_plotter.plot_states_trajectories(show_fig=True, read_from_files=True, plot_observed_states=True)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Plot results. Reference trajectory, forecast trajectory and observations.
# this is specific to this model.
#
referenceTraject  =  Filter._referenceTrajectory
noAssimTraject    =  Filter._NOassimilationTrajectory
forecastTraject   =  Filter._forecastTrajectory
analysisMeans     =  Filter._analysisMeans
Observations      =  Filter._Observations
#Timespan          =  Filter._Filter_Time_Span

#
print 'Plotting Results...\n',divider
#
outfig = plt.figure(1)
ax = outfig.gca(projection='3d')
FS = 14
font = {'weight' : 'bold', 'size' : FS}
plt.rc('font', **font)

ax.plot(referenceTraject[0,:] , referenceTraject[1,:] ,referenceTraject[2,:] , 'k' , linewidth=2 , label='Reference')

ax.plot(noAssimTraject[0,:]   , noAssimTraject[1,:]   ,noAssimTraject[2,:]   , 'b' , linewidth=2 , label='NO Assimilation')

ax.plot(forecastTraject[0,:]  , forecastTraject[1,:] ,forecastTraject[2,:] , 'r--' , linewidth=2 , label='Forecast')

plt.plot( analysisMeans[0,:], analysisMeans[1,:] , analysisMeans[2,:], 'c--' , linewidth=4 , label='EnKF')

plt.plot(Observations[0,:], Observations[1,:], Observations[2,:] ,'g^' , label='Observations')

ax.set_xlabel('$x_1$',fontsize=FS+10, fontweight='bold' )
ax.set_ylabel('$x_2$',fontsize=FS+10, fontweight='bold' )
ax.set_zlabel('$x_3$',fontsize=FS+10, fontweight='bold' )
plt.title('Lorenz-3 model experiment')
plt.legend()
plt.draw()

plt.show()


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
print '\nDone...Terminating...\n',divider
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
_utility.clean_executables()
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
