#!/usr/bin/env

import os
import sys

import numpy as np
from scipy import linalg
import matplotlib.pyplot as plt

import collections

sys.path.append('../../../../DAPack')
#from model_HyPar_OneD import HyPar_OneD
from model_SmallPerturbation import SmallPerturbation
from HyPar_DAFiltering import DAFilter

import _utility
from _visualization import DA_Visualization

#
plt.close('all')
os.system('clear')
np.random.seed(2345)
driver_wd = os.getcwd()
#
divider = "------------------------------------------------------------"

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# 1- Copy model files to a local/temporary directory.
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
dest = os.getcwd()
localmodel_path = _utility.prepare_hypar_model_files(dest)
#~~~~~~~~~~~~~~~~~~~~~          ~~~~~~~~          ~~~~~~~~~~~~~~~~~~~~~
# 1- Read filter and model configs from the da_solver.ini
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
daFilterConfigs = _utility.read_filter_configurations()
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# 2- Create an instance for the DensitySineWave model and build
#    the experiment.
#    Then, prepare the model: overwrite the configuration file,#    setup the grids, and create necessary matrices.
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SmallPerturbation_Model_Model = SmallPerturbation()
_utility.prepare_model_for_filtering(SmallPerturbation_Model_Model)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# 3- Create an instance for the filter and set up the parameters.
#    Also, the uncertainty parameters are added to the model based
#    on the filter configurations.
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Filter = DAFilter(daFilterConfigs)
Filter.set_DA_filter(SmallPerturbation_Model_Model)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# 4- Sequential data assimilation process
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Filter.DA_filtering_process(SmallPerturbation_Model_Model)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# 5- Plot selective results:
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
print 'Plotting Results...\n',divider
#
da_plotter = DA_Visualization()
da_plotter.plot_RMSE_results(Filter_Object=Filter , read_from_files=False, log_scale=False)
da_plotter.plot_states_trajectories(Filter_Object=Filter , vars_to_plot=[i for i in range(12)], read_from_files=True,      \
                                        plot_observed_states=True, show_fig = True )
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

print '\nDone...Terminating...\n----------------------------------------------',
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
_utility.clean_executables()
##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
