#!/usr/bin/env


import os
import sys

import numpy as np
from scipy import linalg
import matplotlib.pyplot as plt

import collections

sys.path.append('../../../../DAPack')
#from model_HyPar_OneD import HyPar_OneD
from model_LaxShockTube import LaxShockTube
from HyPar_DAFiltering import DAFilter

import _utility

#Hypar_bin = '../../../../../../hypar/bin/HyPar' # relative to model_tmp_dir


#
plt.close('all')
os.system('clear')
np.random.seed(2345)
driver_wd = os.getcwd()
#
divider = "------------------------------------------------------------"
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# copy model files to a local/temporary directory.
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
dest = os.getcwd()
localmodel_path = _utility.prepare_hypar_model_files(dest)
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Create an instance for the LaxShockTube model and build the experiment
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
LaxShockTube_Model = LaxShockTube()

# read filter and model configs from the da_solver.ini
daFilterConfigs = _utility.read_filter_configurations()

# read the original filter configurations
originalModelConfigs = _utility.read_original_model_configurations()
if originalModelConfigs.has_key('model'):
    model_name = originalModelConfigs['model']


n_iter       = originalModelConfigs['n_iter']
scr_op_iter  = originalModelConfigs['screen_op_iter']
file_op_iter = originalModelConfigs['file_op_iter']
# initial update of model configurations
new_fields = { 'op_file_format'   : 'text'     ,      \
               'op_overwrite'     : 'yes'      ,      \
               'ip_file_type'     : 'ascii'    ,      \
               'screen_op_iter'   :  n_iter    ,      \
               'file_op_iter'     :  n_iter           \
              }
_utility.overwrite_original_model_configurations(new_fields )
ModelConfigs = _utility.read_original_model_configurations()
# make sure binary initial condtion file exists (create if not)
#_utility.check_initial_condition(dest,overwrite = True)

# intialize the model object
print 'Initializing the model'
LaxShockTube_Model.model_setup(ModelConfigs)


#Retrieve model grids and convert initial condition to portable text file.
print 'Obtaining model grids'
LaxShockTube_Model.set_grids()

#>>>>>>>>>>>>>>>>>>>>>>>>>------------------<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
#         Test time propagation scheme and retrieve grid design.
#>>>>>>>>>>>>>>>>>>>>>>>>>------------------<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
#print 'Test time propagation scheme'
#time_bounds = np.array([0 , 10])
#out_state = LaxShockTube_Model.step_forward(time_bounds , rewrite_as_binary_ip=False )
#LaxShockTube_Model._Grid_vec = grid[:]
#
#_utility.write_output_as_binary_initial_codition(grid , out_state)
#time_bounds = np.array([100 , 1000])
#out_state = LaxShockTube_Model.step_forward(time_bounds , rewrite_as_binary_ip=True )
##>>>>>>>>>>>>>>>>>>>>>>>>>------------------<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Create an instance for the filter and set up the parameters
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Filter = DAFilter(daFilterConfigs)
Filter.set_DA_filter(LaxShockTube_Model)


# 2- use the reference initial condition to create a modeled background error covariance matrix
print "Creating background error covariance matrix..."
LaxShockTube_Model.construct_background_Error_Cov(LaxShockTube_Model._Ref_IC)

# 3- construct the observation operator
LaxShockTube_Model.construct_obs_oper( )

# 4- construct an observaiton error covariance matrix
LaxShockTube_Model.construct_observation_Error_Cov(Filter._Filter_Time_Span)

# 5- perturb initial condition  --> initial forecast
print "Generating forecast initial condition..."
frcst_initial_condition  = LaxShockTube_Model.generate_initialForecast()
#frcst_initial_condition  = ref_initial_condition + np.dot(LaxShockTube_Model._sqrtB0 , _utility.randn_Vec(LaxShockTube_Model._state_size) )

# 6- create initial ensemble
print "Generating an Initial Ensemble..."
Filter.create_Initil_Ensemble(LaxShockTube_Model , LaxShockTube_Model._Ref_IC)


# 7- Sequential data assimilation process
Filter.DA_filtering_process(LaxShockTube_Model , ref_initial_condition )


print '\nDone...Terminating...\n----------------------------------------------',
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
_utility.clean_executables()
##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
