#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

const double pi = 4.0*atan(1.0);

int main(){
  
	int NI,ndims;
        FILE *out;
        char ip_file_type[50];
        strcpy(ip_file_type , "ascii"); // default format.
  std::ifstream in;
  std::cout << "Reading file \"solver.inp\"...\n";
  in.open("solver.inp");
  if (!in) {
    std::cout << "Error: Input file \"solver.inp\" not found. Default values will be used.\n";
  } else {
    char word[500];
    in >> word;
    if (!strcmp(word, "begin")){
      while (strcmp(word, "end")){
        in >> word;
        if (!strcmp(word, "ndims"))     in >> ndims;
        else if (!strcmp(word, "size")) in >> NI;
      }
    }else{ 
      std::cout << "Error: Illegal format in solver.inp. Crash and burn!\n";
    }
  }
  in.close();
  if (ndims != 1) {
    std::cout << "ndims is not 1 in solver.inp. this code is to generate 1D initial conditions\n";
    return(0);
  }
	std::cout << "Grid:\t\t\t" << NI << "\n";

	int i;
	double dx = 10.0 / ((double)(NI-1));

	double *x, *rho,*rhou,*e;
	x    = (double*) calloc (NI, sizeof(double));
	rho  = (double*) calloc (NI, sizeof(double));
	rhou = (double*) calloc (NI, sizeof(double));
	e    = (double*) calloc (NI, sizeof(double));

	for (i = 0; i < NI; i++){
		x[i] = -5.0 + i*dx;
    double RHO,U,P;
    if (x[i] < -4.0) {
      RHO = 27.0/7.0;
      U   = 4.0*sqrt(35.0)/9.0;
      P   = 31.0/3.0;
    } else {
      RHO = 1.0+0.2*sin(5*x[i]);
      U   = 0;
      P   = 1;
    }
    rho[i]  = RHO;
    rhou[i] = RHO*U;
    e[i]    = P/0.4 + 0.5*RHO*U*U;
	}


 
  if (!strcmp(ip_file_type,"ascii")){
	out = fopen("initial.inp","w");
  for (i = 0; i < NI; i++)  fprintf(out,"%lf ",x[i]);
  fprintf(out,"\n");
	for (i = 0; i < NI; i++)	fprintf(out,"%lf ",rho[i]);						
  fprintf(out,"\n");
	for (i = 0; i < NI; i++)	fprintf(out,"%lf ",rhou[i]);						
  fprintf(out,"\n");
	for (i = 0; i < NI; i++)	fprintf(out,"%lf ",e[i]);						
  fprintf(out,"\n");
	fclose(out);

  }else if ((!strcmp(ip_file_type,"binary")) || (!strcmp(ip_file_type,"bin"))) {
           double *Vec = (double*) calloc(3*NI , sizeof(double));
           for (i = 0 ; i < NI ; i++){
               int p = 3*i;
               Vec[p]   =  rho[p];
               Vec[p+1] = rhou[p];
               Vec[p+2] =    e[p];               
           }
           out = fopen("initial.inp","wb");
           fwrite(x,sizeof(double),NI,out);
           fwrite(Vec,sizeof(double),3*NI,out);
           free(Vec);
           fclose(out);
        }else{
           printf("Error: Illegal ip_file_type in solver.inp!\n");
        }





	free(x);
	free(rho);
	free(rhou);
	free(e);

	return(0);
}
