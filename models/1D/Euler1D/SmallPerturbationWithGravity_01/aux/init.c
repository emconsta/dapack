#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

int main(){

  double eta = 0.0001; // has to be set here.
  //printf("Enter the perturbation constant (eta): ");
  //scanf("%lf",&eta);
  
	int NI,ndims;
        FILE *in, *out;
        char ip_file_type[50];
        strcpy(ip_file_type , "ascii"); //default ip_file_type

  
  printf("Reading file \"solver.inp\"...\n");
  in = fopen("solver.inp","r");
  if (!in) printf("Error: Input file \"solver.inp\" not found. Default values will be used.\n");
  else {
    char word[500];
    fscanf(in,"%s",word);
    if (!strcmp(word, "begin")) {
      while (strcmp(word, "end")) {
        fscanf(in,"%s",word);
        if (!strcmp(word, "ndims"))     fscanf(in,"%d",&ndims);
        else if (!strcmp(word, "size")) fscanf(in,"%d",&NI);
        else if (!strcmp(word, "ip_file_type")) fscanf(in,"%s",ip_file_type);
      }
    } else printf("Error: Illegal format in solver.inp. Crash and burn!\n");
  }
  fclose(in);
  if (ndims != 1) {
    printf("ndims is not 1 in solver.inp. this code is to generate 1D initial conditions\n");
    return(0);
  }
	printf("Grid:\t\t\t%d\n",NI);

	int i;
	double dx = 1.0 / ((double)(NI-1));

	double *x, *rho,*rhou,*e;
	x    = (double*) calloc (NI, sizeof(double));
	rho  = (double*) calloc (NI, sizeof(double));
	rhou = (double*) calloc (NI, sizeof(double));
	e    = (double*) calloc (NI, sizeof(double));

	for (i = 0; i < NI; i++){
		x[i] = i*dx;
    double RHO = exp(-x[i]);
    double U   = 0.0;
    double P   = exp(-x[i]) + eta * exp (-100*(x[i]-0.5)*(x[i]-0.5));
    rho[i]  = RHO;
    rhou[i] = RHO*U;
    e[i]    = P/0.4 + 0.5*RHO*U*U;
	}


  if (!strcmp(ip_file_type,"ascii")){    
     printf("Writing ASCII initial condition\n");
     out = fopen("initial.inp","w");
     for (i = 0; i < NI; i++)  fprintf(out,"%1.16E ",x[i]);        fprintf(out,"\n");
     for (i = 0; i < NI; i++)	fprintf(out,"%1.16E ",rho[i]);	  fprintf(out,"\n");
     for (i = 0; i < NI; i++)	fprintf(out,"%1.16E ",rhou[i]);	  fprintf(out,"\n");
     for (i = 0; i < NI; i++)	fprintf(out,"%1.16E ",e[i]);	  fprintf(out,"\n");
  	    
     } else if((!strcmp(ip_file_type,"binary")) || (!strcmp(ip_file_type,"bin")) ){
     double *Vec = (double*) calloc(3*NI , sizeof(double));
     for (i = 0 ; i < NI ; i++){
               int p = 3*i;
               Vec[p]   =  rho[p];
               Vec[p+1] = rhou[p];
               Vec[p+2] =    e[p];               
           }
     printf("Writing binary initial condition\n");
     out = fopen("initial.inp","wb");           
     fwrite(x,sizeof(double),NI,out);           
     fwrite(Vec,sizeof(double),3*NI,out);           
     free(Vec);
     fclose(out);
  }else{
     printf("Error: Illegal ip_file_type in solver.inp!\n");
  }

        

	free(x);
	free(rho);
	free(rhou);
	free(e);

	return(0);
}
