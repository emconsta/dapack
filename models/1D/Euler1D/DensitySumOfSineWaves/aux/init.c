#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>


double raiseto(double x,double a) 
{
  return(exp(a*log(x)));
}

int main() 
{
	int NI,ndims,niter;
  double dt, pi = 4.0*atan(1.0), gamma = 1.4;
  FILE *in, *out;
  char ip_file_type[50];
  strcpy(ip_file_type , "ascii"); //default ip_file_type

  double rho_inf = 1.0,
         u_inf   = 1.0,
         p_inf   = 1.0/gamma;

  /* default values */
  ndims = 1;
  NI    = 64;
  niter = 1000;
  dt    = 0.001;

  printf("Reading file \"solver.inp\"...\n");
  in = fopen("solver.inp","r");
  if (!in) printf("Error: Input file \"solver.inp\" not found. Default values will be used.\n");
  else {
    char word[500];
    fscanf(in,"%s",word);
    if (!strcmp(word, "begin")) {
      while (strcmp(word, "end")) {
        fscanf(in,"%s",word);
        if      (!strcmp(word, "ndims"))  fscanf(in,"%d",&ndims);
        else if (!strcmp(word, "size"))   fscanf(in,"%d",&NI);
        else if (!strcmp(word, "n_iter")) fscanf(in,"%d",&niter);
        else if (!strcmp(word, "dt"))     fscanf(in,"%lf",&dt);
        else if (!strcmp(word, "ip_file_type")) fscanf(in,"%s",ip_file_type);
      }
    } else printf("Error: Illegal format in solver.inp. Crash and burn!\n");
  }
  fclose(in);

  if (ndims != 1) {
    printf("ndims is not 1 in solver.inp. Make sure the correct solver.inp file is present.\n");
    return(0);
  }
	printf("Grid: %d\n",NI);
  //printf("Input maximum wavenumber (typically NI/2): ");
  int limit; //scanf("%d",&limit);
  limit=NI/2;
  printf("Max wavenumber: %d\n",limit);

	int i,k;
	double dx = 1.0 / ((double)NI);
  double tf = ((double)niter) * dt;
  printf("Final Time: %lf\n",tf);

  double factor = 0.01;

  srand(time(NULL));
  double *phi = (double*) calloc (limit+1,sizeof(double));
  for (k=1; k<=limit; k++) {
    phi[k] = -pi + 2*pi*(((double) rand()) / ((double) RAND_MAX));
//    phi[k] = pi/2.0;;
  }

    if (0){
	double *x, *rho,*rhou,*e;
	x    = (double*) calloc (NI, sizeof(double));
	rho  = (double*) calloc (NI, sizeof(double));
	rhou = (double*) calloc (NI, sizeof(double));
	e    = (double*) calloc (NI, sizeof(double));

	for (i = 0; i < NI; i++){
		x[i] = -0.5 + i*dx;
    double RHO,U,P,drho=0;
    for (k=1; k<=limit; k++) {
      double Ak = factor * raiseto(((double)k),-5.0/6.0);
      drho += (Ak * cos(2*pi*((double)k)*(x[i]-u_inf*tf)+phi[k]));
    }
    RHO = rho_inf + drho;
    U   = u_inf;
    P   = p_inf;
    rho[i]  = RHO;
    rhou[i] = RHO*U;
    e[i]    = P/0.4 + 0.5*RHO*U*U;
    printf("%d: %f\n",i,rho[i]);
	}
	out = fopen("exact.inp","w");
  for (i = 0; i < NI; i++)  fprintf(out,"%1.16E ",x[i]);
  fprintf(out,"\n");
	for (i = 0; i < NI; i++)	fprintf(out,"%1.16E ",rho[i]);						
  fprintf(out,"\n");
	for (i = 0; i < NI; i++)	fprintf(out,"%1.16E ",rhou[i]);						
  fprintf(out,"\n");
	for (i = 0; i < NI; i++)	fprintf(out,"%1.16E ",e[i]);						
  fprintf(out,"\n");
	fclose(out);
	free(x);
	free(rho);
	free(rhou);
	free(e);
    }

    
  double *x, *rho,*rhou,*e;
  x    = (double*) calloc (NI, sizeof(double));
  rho  = (double*) calloc (NI, sizeof(double));
  rhou = (double*) calloc (NI, sizeof(double));
  e    = (double*) calloc (NI, sizeof(double));
  for (i = 0; i < NI; i++){
        x[i] = -0.5 + i*dx;
        double RHO,U,P,drho=0;
        for (k=1; k<=limit; k++) {
             double Ak = factor * raiseto(((double)k),-5.0/6.0);
             drho += (Ak * cos(2*pi*((double)k)*(x[i])+phi[k]));
        }
        RHO = rho_inf + drho;
        U   = u_inf;
        P   = p_inf;
        rho[i]  = RHO;
        rhou[i] = RHO*U;
        e[i]    = P/0.4 + 0.5*RHO*U*U;
  }
   	  

  if (!strcmp(ip_file_type,"ascii")){    
     printf("Writing ASCII initial condition\n");
     out = fopen("initial.inp","w");
     for (i = 0; i < NI; i++)  fprintf(out,"%1.16E ",x[i]);        fprintf(out,"\n");
     for (i = 0; i < NI; i++)	fprintf(out,"%1.16E ",rho[i]);	  fprintf(out,"\n");
     for (i = 0; i < NI; i++)	fprintf(out,"%1.16E ",rhou[i]);	  fprintf(out,"\n");
     for (i = 0; i < NI; i++)	fprintf(out,"%1.16E ",e[i]);	  fprintf(out,"\n");
  	    
     } else if((!strcmp(ip_file_type,"binary")) || (!strcmp(ip_file_type,"bin")) ){
     double *Vec = (double*) calloc(3*NI , sizeof(double));
     for (i = 0 ; i < NI ; i++){
               int p = 3*i;
               Vec[p]   =  rho[p];
               Vec[p+1] = rhou[p];
               Vec[p+2] =    e[p];               
           }
     printf("Writing binary initial condition\n");
     out = fopen("initial.inp","wb");           
     fwrite(x,sizeof(double),NI,out);           
     fwrite(Vec,sizeof(double),3*NI,out);           
     free(Vec);
     fclose(out);
  }else{
     printf("Error: Illegal ip_file_type in solver.inp!\n");
  }

        
  free(x);
  free(rho);
  free(rhou);
  free(e);
  free(phi);

return(0);
}
